package com.eduspire.starsavers;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.TimeUtils;

public class PizzaScreen extends AbstractScreen {
	String Prefs = "StarCharacters";
	Preferences pref;

	Set<Integer> set;

	boolean quest = false;
	private Skin skin;

	boolean mushroomret = false, capsicumret = false, hamret = false,
			cheeseret = false, olivesret = false;
	boolean setblank1 = false, setblank2 = false, setblank3 = false,
			setblank4 = false, setblank5 = false, setblank6 = false;

	Image cheese, mushroombase, olivesbase, rocketbase, salamibase,
	pineapplebase, hambase, olives, capsicumbase, pineapple, pizzabase,
	salami, mushroom, cheeseblock, capsicum, ham, rocket, olivestext,
	capsicumtext, hamtext, cheesetext, pineappletext, mushroomtext,
	rockettext, salamitext, iconface, questionface, counterBoxImage,
	blackImage, escapeImage, popupImage, nextButtonImage, pizzaInst0,
	pizzaInst1, pizzaInst2, pizzaInst3, goText, skip;

	double first = 0, second = 0, third = 0, fourth = 0, left = 0, right = 0;

	String op = "", op2 = "";
	private int state = 0;
	long prev_coin, timediff = 0, timex = 0;
	int count = 0;
	int correctpizzas = 0;
	boolean pizzadone = true, stop = false, restart = false,
			debouncepine = true, debouncecheese = true,
			debouncemushroom = true, debouncecapsicum = true,
			debounceolives = true, debounceham = true, debouncesalami = true,
			debouncerocket = true;
	private Image backgroundImage, imagebar1, imagebar2;
	// private SpriteBatch batch;
	int screenWidth, screenHeight;
	int timeCount = 0;
	private Stage stage;
	ArrayList<Integer> numList;
	TextField moneyEarned;
	Group introGroup = new Group();
	BitmapFont sketchFont2;
	BitmapFont arial60;
	Group group, draggroup, ingredients, basegroup;
	Music gameTheme;
	Sound pizzaDrop, correctBell, incorrectBuzz;

	public PizzaScreen(StarSavers game) {
		super(game);

	}

	public PizzaScreen(StarSavers game, boolean questmode) {
		super(game);
		quest = questmode;

	}

	@Override
	public void show() {
		pref = Gdx.app.getPreferences(Prefs);

		Integer[] list = { 1, 2, 3, 4, 5, 6, 7, 8 };
		numList = new ArrayList<Integer>(Arrays.asList(list));
		Texture.setEnforcePotImages(false);
		group = new Group();
		draggroup = new Group();
		ingredients = new Group();
		basegroup = new Group();
		// jarSprite = new Sprite(scalesb);
		// bankSprite = new Sprite(bank);

		pizzaDrop = Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/pizzaDrop.mp3"));
		correctBell = Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/correctBell.mp3"));
		incorrectBuzz = Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/incorrectBuzz.mp3"));

		// g batch = new SpriteBatch();
		boolean maintainAspectRatio = true;
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		stage = new Stage(screenWidth, screenHeight, maintainAspectRatio);
		Gdx.input.setInputProcessor(stage);
		arial60 = new BitmapFont(Gdx.files.internal("data/Fonts/arial64.fnt"),
				Gdx.files.internal("data/Fonts/arial64.png"), false);
		TextFieldStyle moneyEarnedStyle = new TextFieldStyle();
		moneyEarnedStyle.fontColor = Color.BLACK;
		moneyEarnedStyle.font = arial60;
		moneyEarnedStyle.font.setScale(screenWidth / 1200f);
		moneyEarned = new TextField("0", moneyEarnedStyle);
		moneyEarned.setWidth(screenWidth);
		moneyEarned.setX(83f * screenWidth / 100f);
		moneyEarned.setY(80f * screenHeight / 100f);
		moneyEarned.setDisabled(true);

		cheese = new Image(game.pizzaAtlas.findRegion("cheese base"));
		mushroombase = new Image(game.pizzaAtlas.findRegion("mushroom base"));
		capsicumbase = new Image(game.pizzaAtlas.findRegion("cap base"));
		pineapplebase = new Image(game.pizzaAtlas.findRegion("pineapplebase"));
		rocketbase = new Image(game.pizzaAtlas.findRegion("rocket base"));
		salamibase = new Image(game.pizzaAtlas.findRegion("salami base"));
		hambase = new Image(game.pizzaAtlas.findRegion("ham base"));
		olivesbase = new Image(game.pizzaAtlas.findRegion("olivesbase"));
		pizzabase = new Image(game.pizzaAtlas.findRegion("pizza base"));
		
		imagebar1 = new Image(game.generalTextureAtlas.findRegion("bar"));
		imagebar2 = new Image(game.generalTextureAtlas.findRegion("grayBar"));
		imagebar1.setWidth(0);
		imagebar2.setWidth(screenWidth);

		backgroundImage = new Image(new Texture(
				"data/Backgrounds/pizzaScreenBackground.png"));
		backgroundImage.setFillParent(true);
		pref.putInteger("Savings_new", pref.getInteger("Savings", 0));

		gameTheme = Gdx.audio.newMusic(Gdx.files
				.internal("data/Sound/gameTheme.mp3"));
		gameTheme.setLooping(true);
		skip = new Image(game.generalTextureAtlas.findRegion("skip"));
		skip.setX(80f * screenWidth / 100f);
		skip.setY(5f * screenHeight / 100f);
		skip.setScale(screenWidth / 1280f);
		skip.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				timeCount = 238;
			}
		});

		popupImage = new Image(game.generalTextureAtlas.findRegion("popup"));
		popupImage.setX(10f * screenWidth / 100f);
		popupImage.setY(10f * screenHeight / 100f);
		popupImage.setSize(80f * screenWidth / 100f, 80f * screenHeight / 100f);

		pizzaInst0 = new Image(new Texture("data/Instructions/pizzaInst0.png"));
		pizzaInst0.setSize(screenWidth, screenHeight);
		introGroup.addActor(pizzaInst0);
		// introGroup.addActor(skip);

		pizzaInst1 = new Image(new Texture("data/Instructions/pizzaInst1.png"));
		pizzaInst1.setSize(screenWidth, screenHeight);

		pizzaInst2 = new Image(new Texture("data/Instructions/pizzaInst2.png"));
		pizzaInst2.setSize(screenWidth, screenHeight);

		pizzaInst3 = new Image(new Texture("data/Instructions/pizzaInst3.png"));
		pizzaInst3.setSize(screenWidth, screenHeight);

		goText = new Image(game.generalTextureAtlas.findRegion("goText"));
		goText.setX(30f * screenWidth / 100f);
		goText.setY(30f * screenHeight / 100f);

		counterBoxImage = new Image(game.generalTextureAtlas.findRegion("counterSavings"));
		counterBoxImage.setScale(screenWidth / 2280f);
		counterBoxImage.setX(77f * screenWidth / 100f);
		counterBoxImage.setY(78f * screenHeight / 100f);

		nextButtonImage = new Image(game.generalTextureAtlas.findRegion("nextButton"));
		nextButtonImage.setScale(screenWidth / 1280f);
		nextButtonImage.setX(74f * screenWidth / 100f);
		nextButtonImage.setY(10f * screenHeight / 100f);
		nextButtonImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				stage.addActor(blackImage);
				blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
					@Override
					public boolean act(float delta) {
						gameTheme.stop();
						dispose();
						game.setScreen(new BankScreen(game));
						return false;
					}
				}));
			}
		});

		stage.addActor(backgroundImage);
		stage.addActor(imagebar2);
		stage.addActor(imagebar1);
		stage.addActor(counterBoxImage);
		stage.addActor(moneyEarned);

		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("x " + x + "y " + y + "" + screenHeight);
			}
		});

		pizzabase.setX(screenWidth / 3.6f);
		pizzabase.setY(0);//-screenHeight / 15f);
		pizzabase.setScale(screenHeight / 900f);
		cheeseblock = new Image(game.pizzaAtlas.findRegion("cheeseblock"));
		cheeseblock.setScale(screenHeight / 1000f);
		cheeseblock.setX(screenWidth / 1.605f);
		cheeseblock.setY(screenHeight / 24f);
		cheeseblock.setName("1");
		mushroom = new Image(game.pizzaAtlas.findRegion("mushroom"));
		mushroom.setX(screenWidth / 1.180f);
		mushroom.setY(screenHeight / 36F);
		mushroom.setScale(screenHeight / 1250F);
		mushroom.setName("3");
		capsicum = new Image(game.pizzaAtlas.findRegion("cap green"));
		capsicum.setX(screenWidth / 1.625f);
		capsicum.setScale(screenHeight / 1100f);
		capsicum.setY(screenHeight / 4);
		capsicum.setName("2");
		salami = new Image(game.pizzaAtlas.findRegion("salamiwhole"));
		salami.setX(screenWidth / 102.85f);
		salami.setScale(screenHeight / 1100f);
		salami.setY(screenHeight / 20f);
		salami.setName("4");
		ham = new Image(game.pizzaAtlas.findRegion("ham"));
		ham.setX(screenWidth / 7.85f);
		ham.setScale(screenHeight / 1050f);
		ham.setY(screenHeight / 20f);
		ham.setName("5");
		rocket = new Image(game.pizzaAtlas.findRegion("rocket"));
		rocket.setX(screenWidth / 1.185f);
		rocket.setScale(screenHeight / 1000f);
		rocket.setY(screenHeight / 4f);
		rocket.setName("6");
		pineapple = new Image(game.pizzaAtlas.findRegion("pineapple"));
		pineapple.setX(screenWidth / 1.325f);
		pineapple.setScale(screenHeight / 900f);
		pineapple.setY(screenHeight / 45.5f);
		pineapple.setName("7");
		olives = new Image(game.pizzaAtlas.findRegion("olives"));
		olives.setX(screenWidth / 1.325f);
		olives.setScale(screenHeight / 1100f);
		olives.setY(screenHeight / 2.8f);
		olives.setName("8");

		iconface = new Image(game.pizzaAtlas.findRegion("iconface"));
		questionface = new Image(game.pizzaAtlas.findRegion("questionface"));

		escapeImage = new Image(game.generalTextureAtlas.findRegion("HomeButton"));
		escapeImage.setX(screenWidth - screenWidth / 10f);
		escapeImage.setY(60f * screenHeight / 100f);
		escapeImage.setScale(0.70f);
		escapeImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				gameTheme.stop();
				dispose();
				game.setScreen(new BankScreen(game));
			}
		});
		stage.addActor(escapeImage);

		group.addActor(pizzabase);
		group.addActor(cheeseblock);
		group.addActor(mushroom);
		group.addActor(capsicum);
		group.addActor(olives);
		group.addActor(ham);
		group.addActor(rocket);
		group.addActor(salami);
		group.addActor(pineapple);

		pineapple.addListener(new DragListener() {
			public void touchDragged(InputEvent event, float x, float y,
					int pointer) {
				float dx = x - pineapple.getWidth() * 0.5f;
				float dy = y - pineapple.getHeight() * 0.5f;
				pineapple.setPosition(pineapple.getX() + dx, pineapple.getY()
						+ dy);
				group.removeActor(pineapple);
				draggroup.addActor(pineapple);
			}
		});

		pineapple.addListener(new DragListener() {
			public void dragStop(InputEvent event, float x, float y, int pointer) {

				snapto(pineapple, x, y);
				pizzaDrop.play();
			}
		});

		salami.addListener(new DragListener() {
			public void touchDragged(InputEvent event, float x, float y,
					int pointer) {

				float dx = x - salami.getWidth() * 0.5f;
				float dy = y - salami.getHeight() * 0.5f;
				salami.setPosition(salami.getX() + dx, salami.getY() + dy);
				group.removeActor(salami);
				draggroup.addActor(salami);
			}
		});

		salami.addListener(new DragListener() {
			public void dragStop(InputEvent event, float x, float y, int pointer) {

				snapto(salami, x, y);
				pizzaDrop.play();

			}
		});
		ham.addListener(new DragListener() {
			public void touchDragged(InputEvent event, float x, float y,
					int pointer) {
				System.out.println("Ham picked");
				float dx = x - ham.getWidth() * 0.5f;
				float dy = y - ham.getHeight() * 0.5f;
				ham.setPosition(ham.getX() + dx, ham.getY() + dy);

				// group.removeActor(ham);
				// draggroup.addActor(ham);
			}
		});

		ham.addListener(new DragListener() {
			public void dragStop(InputEvent event, float x, float y, int pointer) {
				System.out.println("dragstop ham");
				snapto(ham, x, y);
				pizzaDrop.play();

			}
		});

		rocket.addListener(new DragListener() {
			public void touchDragged(InputEvent event, float x, float y,
					int pointer) {
				float dx = x - rocket.getWidth() * 0.5f;
				float dy = y - rocket.getHeight() * 0.5f;
				rocket.setPosition(rocket.getX() + dx, rocket.getY() + dy);
				group.removeActor(rocket);
				draggroup.addActor(rocket);
			}
		});

		rocket.addListener(new DragListener() {
			public void dragStop(InputEvent event, float x, float y, int pointer) {

				snapto(rocket, x, y);
				pizzaDrop.play();

			}
		});

		cheeseblock.addListener(new DragListener() {
			public void touchDragged(InputEvent event, float x, float y,
					int pointer) {
				float dx = x - cheeseblock.getWidth() * 0.5f;
				float dy = y - cheeseblock.getHeight() * 0.5f;
				cheeseblock.setPosition(cheeseblock.getX() + dx,
						cheeseblock.getY() + dy);
				group.removeActor(cheeseblock);
				draggroup.addActor(cheeseblock);

			}
		});

		cheeseblock.addListener(new DragListener() {
			public void dragStop(InputEvent event, float x, float y, int pointer) {

				snapto(cheeseblock, x, y);
				pizzaDrop.play();

			}
		});

		olives.addListener(new DragListener() {
			public void touchDragged(InputEvent event, float x, float y,
					int pointer) {
				float dx = x - olives.getWidth() * 0.5f;
				float dy = y - olives.getHeight() * 0.5f;
				olives.setPosition(olives.getX() + dx, olives.getY() + dy);
				group.removeActor(olives);
				draggroup.addActor(olives);

			}
		});

		olives.addListener(new DragListener() {
			public void dragStop(InputEvent event, float x, float y, int pointer) {

				snapto(olives, x, y);
				pizzaDrop.play();

			}
		});
		mushroom.addListener(new DragListener() {
			public void touchDragged(InputEvent event, float x, float y,
					int pointer) {

				float dx = x - mushroom.getWidth() * 0.5f;
				float dy = y - mushroom.getHeight() * 0.5f;
				mushroom.setPosition(mushroom.getX() + dx, mushroom.getY() + dy);

				// evaluate(capsicum);
				group.removeActor(mushroom);
				draggroup.addActor(mushroom);

			}
		});

		mushroom.addListener(new DragListener() {
			public void dragStop(InputEvent event, float x, float y, int pointer) {
				System.out.print("mushroom stopped");
				snapto(mushroom, x, y);
				pizzaDrop.play();

			}
		});

		capsicum.addListener(new DragListener() {
			public void touchDragged(InputEvent event, float x, float y,
					int pointer) {

				float dx = x - capsicum.getWidth() * 0.5f;
				float dy = y - capsicum.getHeight() * 0.5f;
				capsicum.setPosition(capsicum.getX() + dx, capsicum.getY() + dy);

				// evaluate(capsicum);
				group.removeActor(capsicum);
				draggroup.addActor(capsicum);

			}
		});
		capsicum.addListener(new DragListener() {
			public void dragStop(InputEvent event, float x, float y, int pointer) {

				snapto(capsicum, x, y);
				pizzaDrop.play();

			}
		});

		stage.addActor(group);
		stage.addActor(ingredients);
		stage.addActor(draggroup);
		stage.addActor(basegroup);
		// stage.addActor(blackImage);

		blackImage = new Image(game.generalTextureAtlas.findRegion("black"));
		blackImage.setFillParent(true);
		stage.addActor(blackImage);
		blackImage.addAction(sequence(delay(0.75f), fadeOut(0.1f),
				Actions.removeActor()));

		gameTheme.setVolume(0.4f);
		gameTheme.play();
	}

	@Override
	public void render(float delta) {
		if ((pizzadone && !stop)) {

			restart = false;
			pizzadone = false;
			Random rand = new Random(System.currentTimeMillis());
			int x = rand.nextInt(6 - 2) + 2;
			System.out.println(x);
			System.gc();
			getIngredients(x);
		} else {
			if (ingredients.hasChildren() == false) {

				iconface.setX(screenWidth / 3.7f);
				iconface.setY(-screenHeight / 150f);
				iconface.setScale(screenHeight / 900f);

				timex = TimeUtils.nanoTime();
				pizzadone = true;
				basegroup.clear();
				correctpizzas++;
				moneyEarned.setText("" + correctpizzas * 5);

				System.out.println(pref.getInteger("Savings_new", 0));
				System.out.println(pref.getInteger("Savings", 0));
				group.addActor(iconface);
				correctBell.play();
				debouncepine = true;
				debouncecheese = true;
				debouncemushroom = true;
				debouncecapsicum = true;
				debounceham = true;
				debouncesalami = true;
				debouncerocket = true;
				debounceolives = true;
				basegroup.clear();
			}
			if (restart) {

				questionface.setX(screenWidth / 3.7f);
				questionface.setY(-screenHeight / 150f);
				timex = TimeUtils.nanoTime();
				group.addActor(questionface);
				incorrectBuzz.play();
				basegroup.clear();

				// pizzabase.setName("1");
				questionface.setScale(screenHeight / 900f);
				if (quest) {
					moneyEarned.setText("" + --correctpizzas * 5);
				}
				long time = TimeUtils.nanoTime();

				ingredients.clear();

				pizzadone = true;
				debouncepine = true;
				debouncecheese = true;
				debouncemushroom = true;
				debouncecapsicum = true;
				debounceham = true;
				debouncesalami = true;
				debouncerocket = true;
				debounceolives = true;
				basegroup.clear();
			}
		}

		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();

		batch.begin();
		// batch.draw(bank, 100, 150);

		batch.end();

		imagebar1.setX(0);
		// imagebar1.setScaleY(0.02f);
		imagebar2.setX(0);
		// imagebar2.setScaleY(0.02f);
		imagebar1.setY(screenHeight - screenHeight / 20);
		imagebar2.setY(screenHeight - screenHeight / 20);
		// stage.draw();

		if (TimeUtils.nanoTime() - timex > 600000000) {
			group.removeActor(questionface);
			group.removeActor(iconface);
		}

		if (TimeUtils.nanoTime() - timediff > 39500000) {
			timediff = TimeUtils.nanoTime();
			timeCount++;
			if (timeCount == 60) {
				introGroup.clear();
				introGroup.addActor(pizzaInst1);
				introGroup.addActor(skip);
			} else if (timeCount == 120) {
				introGroup.clear();
				introGroup.addActor(pizzaInst2);
				introGroup.addActor(skip);
			} else if (timeCount == 180) {
				introGroup.clear();
				introGroup.addActor(pizzaInst3);
				introGroup.addActor(skip);
			} else if (timeCount == 240) {
				introGroup.clear();
				introGroup.addActor(goText);
			} else if (timeCount == 260) {
				introGroup.clear();
				stop = false;
			}

			if (timeCount > 300) {
				imagebar1.setWidth(screenWidth / 500 * count++);
				// System.out.println(count+","+imagebar1.getWidth());
				if (imagebar1.getWidth() >= screenWidth && !stop) {

					stop = true;

					stage.addActor(popupImage);
					stage.addActor(nextButtonImage);


					TextFieldStyle finishedMessageStyle = new TextFieldStyle();
					finishedMessageStyle.fontColor = Color.BLACK;
					finishedMessageStyle.font = game.font;
					finishedMessageStyle.font.setScale(screenWidth / 1200f);
					TextField finishedMessage1, finishedMessage2;
					if (quest) {
						if (correctpizzas > 5) {
							finishedMessage1 = new TextField("Quest",
									finishedMessageStyle);
							finishedMessage2 = new TextField("Completed",
									finishedMessageStyle);
							finishedMessage1.setWidth(screenWidth);
							finishedMessage1.setX(22.5f * screenWidth / 100f);
							finishedMessage1.setY(50f * screenHeight / 100f);
							stage.addActor(finishedMessage1);
							finishedMessage1.setDisabled(true);
							finishedMessage2.setWidth(screenWidth);
							finishedMessage2.setX(30f * screenWidth / 100f);

							finishedMessage2.setY(35f * screenHeight / 100f);
							stage.addActor(finishedMessage2);
							pref.putInteger("fromAdventureScreen", 0);
							pref.putInteger("Savings_new",
									pref.getInteger("Savings", 0) + 5
									* correctpizzas + 30);
							pref.flush();
							return;

						} else {
							finishedMessage1 = new TextField("Quest",
									finishedMessageStyle);
							finishedMessage2 = new TextField("Failed",
									finishedMessageStyle);
						}
					} else {
						finishedMessage1 = new TextField("You made",
								finishedMessageStyle);
						finishedMessage2 = new TextField("" + correctpizzas
								+ " pizzas", finishedMessageStyle);
					}

					finishedMessage1.setWidth(screenWidth);
					finishedMessage1.setX(22.5f * screenWidth / 100f);
					finishedMessage1.setY(50f * screenHeight / 100f);
					stage.addActor(finishedMessage1);
					finishedMessage1.setDisabled(true);
					finishedMessage2.setWidth(screenWidth);
					if (correctpizzas > 9) {
						finishedMessage2.setX(30f * screenWidth / 100f);
					} else {
						finishedMessage2.setX(35f * screenWidth / 100f);
					}
					finishedMessage2.setY(35f * screenHeight / 100f);
					stage.addActor(finishedMessage2);
					finishedMessage2.setDisabled(true);

					stage.addActor(escapeImage);

					pref.putInteger("Savings_new",
							pref.getInteger("Savings", 0) + correctpizzas);

					pref.putInteger("fromAdventureScreen", 0);
					pref.flush();

					// ///////////////////////////////////////////////////////////////////////

					pref.putInteger("fromAdventureScreen", 0);

					if (quest) {
						if (correctpizzas > 8) {
							pref.putInteger("Savings_new",
									pref.getInteger("Savings", 0) + 5
									* correctpizzas + 30);
						} else {

							if(correctpizzas>0)
							{
								pref.putInteger("Savings_new",
										pref.getInteger("Savings", 0) + 5
										* correctpizzas);
							}
							else
							{
								pref.putInteger("Savings_new",
										pref.getInteger("Savings", 0) -10);
							}

						}
					} else {
						pref.putInteger("Savings_new",
								pref.getInteger("Savings", 0) + 5
								* correctpizzas);
					}
					pref.flush();

				}
			}
		}
		stage.addActor(introGroup);
	}

	@Override
	public void resize(int width, int height) {

	}

	public void snapto(Image image, float x, float y) {

		System.out.println("Entered0");
		if (((image.getX() + x) > screenWidth / 4.4f)
				&& ((image.getX() + x) < screenWidth / 1.3f))

		{
			System.out.println("Entered1");
			if (((image.getY() + y > -30))
					&& (image.getY() + y < screenHeight / 2.0f)) {
				System.out.println("Entered12");
				if (image.getName().equals("1")) {
					System.out.print("cheese");
					cheese.setX(screenWidth / 3.2f);
					cheese.setY(screenHeight/22f);
					cheese.setScale(screenHeight / 980f);
					basegroup.addActor(cheese);
					image.setY(30);
					image.setX(screenWidth / 1.605f);
					System.out.println("Drop It");
					try {
						if (debouncecheese) {
							boolean value = ingredients.removeActor(cheesetext);
							System.out.println(value);
							if (!value) {
								System.out.println("Oops wrong ingredient!");
								restart = true;
							} else {
								debouncecheese = false;
							}
						}
					} catch (Exception e) {

					}

				}
				if (image.getName().equals("2")) {
					System.out.print("capscium");
					capsicumbase.setX(screenWidth / 3.2f);
					capsicumbase.setY(screenHeight/15f);//-screenHeight / 20f);
					capsicumbase.setScale(screenHeight / 900f);
					basegroup.addActor(capsicumbase);
					image.setY(screenHeight / 4);
					image.setX(screenWidth / 1.505f);
					try {
						if (debouncecapsicum) {
							boolean value = ingredients
									.removeActor(capsicumtext);
							System.out.println(value);
							if (!value) {
								System.out.println("Oops wrong ingredient!");
								restart = true;

							} else {
								debouncecapsicum = false;
							}
						}
					} catch (Exception e) {

					}
					System.out.println("Drop It");

				}
				if (image.getName().equals("3")) {
					System.out.print("mushroom");

					mushroombase.setX(screenWidth / 3.2f);
					mushroombase.setY(screenHeight / 24f);
					mushroombase.setScale(screenHeight / 900f);
					System.out.println("mushroom added");
					basegroup.addActor(mushroombase);
					image.setY(20);
					image.setX(screenWidth / 1.18f);
					System.out.println("Drop It");
					try {
						if (debouncemushroom) {
							boolean value = ingredients
									.removeActor(mushroomtext);
							System.out.println(value);
							if (!value) {
								System.out.println("Oops wrong ingredient!");
								restart = true;
							} else {
								debouncemushroom = false;
							}
						}
					} catch (Exception e) {

					}

				}
				if (image.getName().equals("4")) {
					System.out.print("salami");
					salamibase.setX(screenWidth / 3.2f);
					salamibase.setY(screenHeight / 18f);
					salamibase.setScale(screenHeight / 900f);
					basegroup.addActor(salamibase);
					image.setY(screenHeight / 20f);
					image.setX(screenWidth / 102.85f);
					System.out.println("Drop It");
					try {
						if (debouncesalami) {
							boolean value = ingredients.removeActor(salamitext);
							System.out.println(value);
							if (!value) {
								System.out.println("Oops wrong ingredient!");
								restart = true;
							} else {
								debouncesalami = false;
							}
						}
					} catch (Exception e) {

					}
				}
				if (image.getName().equals("5")) {
					System.out.print("ham");
					hambase.setX(screenWidth / 3.2f);
					hambase.setY(screenHeight / 18f);
					hambase.setScale(screenHeight / 900f);
					basegroup.addActor(hambase);
					image.setY(screenHeight / 20f);
					image.setX(screenWidth / 7.85f);
					System.out.println("Drop It");
					try {
						if (debounceham) {
							boolean value = ingredients.removeActor(hamtext);
							System.out.println(value);
							if (!value) {
								System.out.println("Oops wrong ingredient!");
								restart = true;
							} else {
								debounceham = false;
							}
						}
					} catch (Exception e) {
						System.out.println("Oops wrong ingredient!");
					}
				}

				if (image.getName().equals("7")) {
					System.out.print("pineapple");
					pineapplebase.setX(screenWidth / 3.2f);
					pineapplebase.setY(screenHeight / 18f);
					pineapplebase.setScale(screenHeight / 900f);
					basegroup.addActor(pineapplebase);
					image.setY(screenHeight / 45.5f);
					image.setX(screenWidth / 1.325f);
					System.out.println("Drop It");
					try {
						if (debouncepine) {
							boolean value = ingredients
									.removeActor(pineappletext);
							System.out.println(value);
							if (!value) {
								System.out.println("Oops wrong ingredient!");
								restart = true;
							} else {
								debouncepine = false;
							}
						}
					} catch (Exception e) {
						System.out.println("Oops wrong ingredient!");
					}
				}
				if (image.getName().equals("6")) {
					System.out.print("rocket");
					rocketbase.setX(screenWidth / 3.2f);
					rocketbase.setY(screenHeight / 18f);
					rocketbase.setScale(screenHeight / 900f);
					basegroup.addActor(rocketbase);
					image.setY(screenHeight / 3.8f);
					image.setX(screenWidth / 1.185f);
					System.out.println("Drop It");
					try {
						if (debouncerocket) {
							boolean value = ingredients.removeActor(rockettext);
							System.out.println(value);
							if (!value) {
								System.out.println("Oops wrong ingredient!");
								restart = true;
							} else {
								debouncerocket = false;
							}
						}
					} catch (Exception e) {
						System.out.println("Oops wrong ingredient!");
					}
				}
				if (image.getName().equals("8")) {
					System.out.print("olives");
					olivesbase.setX(screenWidth / 3.2f);
					olivesbase.setY(screenHeight / 18f);
					olivesbase.setScale(screenHeight / 900f);
					basegroup.addActor(olivesbase);
					image.setY(screenHeight / 2.8f);
					image.setX(screenWidth / 1.325f);
					System.out.println("Drop It");
					try {
						if (debounceolives) {
							boolean value = ingredients.removeActor(olivestext);
							System.out.println(value);
							if (!value) {
								System.out.println("Oops wrong ingredient!");
								restart = true;
							} else {
								debounceolives = false;
							}
						}
					} catch (Exception e) {
						System.out.println("Oops wrong ingredient!");
					}
				}

			}

		}

	}

	public void getIngredients(int num) {
		basegroup.clear();
		capsicumtext = new Image(game.pizzaAtlas.findRegion("capsicumtext"));
		hamtext = new Image(game.pizzaAtlas.findRegion("hamtext"));
		cheesetext = new Image(game.pizzaAtlas.findRegion("cheesetext"));
		pineappletext = new Image(game.pizzaAtlas.findRegion("pineappletext"));
		mushroomtext = new Image(game.pizzaAtlas.findRegion("mushroomtext"));
		rockettext = new Image(game.pizzaAtlas.findRegion("rockettext"));
		salamitext = new Image(game.pizzaAtlas.findRegion("salamitext"));
		olivestext = new Image(game.pizzaAtlas.findRegion("olivestext"));
		set = new HashSet<Integer>();
		long seed = System.nanoTime();
		Collections.shuffle(numList, new Random(seed));
		System.out.println("***begin**");

		int counter = 0;
		while (counter < num) {
			System.out.println(counter);
			float Y = 0, X = 0;
			int i = index(counter);
			while (i < 0) {
				// System.out.println("stuck");
				i = index(counter);
			}

			switch (counter) {
			case 0:
				X = screenWidth / 3.81f;
				Y = (screenHeight / 1.18f);
				break;
			case 1:
				X = screenWidth / 6.41f;
				Y = (screenHeight / 1.33f);
				break;
			case 2:
				X = screenWidth / 3.81f;
				Y = (screenHeight / 1.55f);
				break;
			case 3:
				X = screenWidth / 6.41f;
				Y = (screenHeight / 1.83f);
				break;
			case 4:
				X = screenWidth / 3.81f;
				Y = (screenHeight / 2.17f);
				break;
			default:
				System.out.println("cerror" + counter);

			}
			switch (i) {

			case 1:
				System.out.println("pineapple");
				pineappletext.setScale(screenHeight / 1250f);
				pineappletext.setX(X);
				pineappletext.setY(Y);
				ingredients.addActor(pineappletext);
				counter++;
				break;

			case 2:
				System.out.println("capsicum");
				capsicumtext.setScale(screenHeight / 1250f);
				capsicumtext.setX(X);
				capsicumtext.setY(Y);
				ingredients.addActor(capsicumtext);
				counter++;
				break;

			case 3:
				System.out.println("mushroom");
				mushroomtext.setScale(screenHeight / 1250f);
				mushroomtext.setX(X);
				mushroomtext.setY(Y);
				ingredients.addActor(mushroomtext);
				counter++;
				break;

			case 4:
				System.out.println("salami");
				salamitext.setScale(screenHeight / 1250f);
				salamitext.setX(X);
				salamitext.setY(Y);
				ingredients.addActor(salamitext);
				counter++;
				break;

			case 5:
				System.out.println("ham");
				hamtext.setScale(screenHeight / 1250f);
				hamtext.setX(X);
				hamtext.setY(Y);
				ingredients.addActor(hamtext);
				counter++;
				break;

			case 6:
				System.out.println("rocket");
				rockettext.setScale(screenHeight / 1250f);
				rockettext.setX(X);
				rockettext.setY(Y);
				ingredients.addActor(rockettext);
				counter++;
				break;

			case 8:
				System.out.println("cheese");
				cheesetext.setScale(screenHeight / 1200f);
				cheesetext.setX(X);
				cheesetext.setY(Y);
				ingredients.addActor(cheesetext);
				counter++;
				break;

			case 7:
				System.out.println("olives");
				olivestext.setScale(screenHeight / 1250f);
				olivestext.setX(X);
				olivestext.setY(Y);
				ingredients.addActor(olivestext);
				counter++;
				break;

			default:
				System.out.println("error");
				break;

				// System.out.println("i : " + i+","+counter);
			}
		}
		System.out.println("**end***");

		pizzadone = false;

	}

	public int index(int counter) {

		// System.out.println(counter);

		int r = numList.get(counter);
		if (set.add(r)) {
			// System.out.println("r : " + r);

			return r;
		} else
			return -1;

	}

	@Override
	public void dispose() {
		font.dispose();
		batch.dispose();
		stage.dispose();
		
		gameTheme.dispose();
	}

}
