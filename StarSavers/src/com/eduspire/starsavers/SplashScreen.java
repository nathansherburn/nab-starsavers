package com.eduspire.starsavers;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import com.eduspire.starsavers.StarSavers;

// Shows a splash image and moves on to the next screen.
public class SplashScreen extends AbstractScreen {
	private Image splashImage;
	String Prefs = "StarCharacters";
	Preferences pref;

	public SplashScreen(StarSavers game) {
		super(game);
	}

	@Override
	public void show() {
		super.show();
		pref = Gdx.app.getPreferences(Prefs);
		Texture.setEnforcePotImages(false);

		Texture splashTexture = new Texture("data/Backgrounds/splashscreen.png");
		splashTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		// Here we create the splash image actor
		splashImage = new Image(splashTexture);
		splashImage.setFillParent(true);

		// Make the image completely transparent
		splashImage.getColor().a = 0f;

		// Configure the fade-in/out effect on the splash image
		splashImage.addAction(sequence(fadeIn(0.75f), delay(1.75f),
				fadeOut(0.75f), new Action() {
					@Override
					public boolean act(float delta) {
						// Get the next screen
						if (pref.getBoolean("setupdone", false) == false) {
							game.setScreen(new SetupScreen2(game));
						} else {
							game.setScreen(new AdventureScreen(game));
						}
						return true;
					}
				}));

		// Add the actor to the stage
		stage.addActor(splashImage);
	}

	@Override
	public void dispose() {
		super.dispose();
	}
}