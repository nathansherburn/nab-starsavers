package com.eduspire.starsavers;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.utils.TimeUtils;

public class SchoolScreen extends AbstractScreen {

	public Texture background, avatarTexture, bedTexture, adventureTexture;
	private Stage stage;
	private Image backgroundImage;
	private SpriteBatch batch;
	Set<Integer> set;
	boolean quest = false;
	int screenWidth, screenHeight;
	String Prefs = "StarCharacters";
	Preferences pref;
	boolean nextword = true;
	List<String> phrases;
	ListIterator listit;
	List<String> sentences;
	ListIterator listit2;
	Image[] images;
	private BitmapFont arial64;
	Group group;
	Group draggroup;
	Group starGroup = new Group();
	Group introGroup = new Group();

	String catchphrase;
	String sentencedisplayed = "";

	Image imagebar1, imagebar2, escapeImage, popupImage, nextButtonImage,
			blackImage, correctImage, incorrectImage, instruction1,
			instruction2, instruction3, instruction4, instruction5,
			instruction6, skip, counterEducation;
	TextField finishedMessage1, finishedMessage2, eduCount;
	Label jumble1;
	int timeCount = 0;
	long timediff = 0;
	int count = 0;
	int educationPointsThisRound = 0;
	int faceCounter = 0;

	Music schoolTheme;
	Sound correctBell, incorrectBuzz;

	boolean stop = false;

	public SchoolScreen(StarSavers game) {
		super(game);
	}

	public SchoolScreen(StarSavers game, boolean questmode) {
		super(game);
		quest = questmode;
	}

	void placeImage(Image imageName, Float scale, Float x, Float y) {
		imageName.setScale(scale);
		imageName.setX(x);
		imageName.setY(y);
	}

	@Override
	public void show() {

		Texture.setEnforcePotImages(false);
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		batch = new SpriteBatch();
		boolean maintainAspectRatio = true;
		stage = new Stage(screenWidth, screenHeight, maintainAspectRatio);
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);
		pref = Gdx.app.getPreferences(Prefs);
		

		backgroundImage = new Image(new Texture(
				"data/Backgrounds/SchoolScreenBackground.png"));
		backgroundImage.setFillParent(true);
		stage.addActor(backgroundImage);

		schoolTheme = Gdx.audio.newMusic(Gdx.files
				.internal("data/Sound/schoolTheme.mp3"));
		schoolTheme.setLooping(true);

		stage.addActor(starGroup);

		arial64 = new BitmapFont(Gdx.files.internal("data/Fonts/arial64.fnt"),
				Gdx.files.internal("data/Fonts/arial64.png"), false);
		arial64.getRegion().getTexture()
				.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		LabelStyle style = new LabelStyle();
		style.fontColor = Color.BLACK;

		popupImage = new Image(game.generalTextureAtlas.findRegion("popup"));
		popupImage.setX(10f * screenWidth / 100f);
		popupImage.setY(10f * screenHeight / 100f);
		popupImage.setSize(80f * screenWidth / 100f, 80f * screenHeight / 100f);

		counterEducation = new Image(
				game.generalTextureAtlas.findRegion("counterEducation"));
		counterEducation.setScale(screenWidth / 1400f);
		counterEducation.setX(70f * screenWidth / 100f);
		counterEducation.setY(77f * screenHeight / 100f);
		stage.addActor(counterEducation);

		BitmapFont arial64 = new BitmapFont(
				Gdx.files.internal("data/Fonts/arial64.fnt"),
				Gdx.files.internal("data/Fonts/arial64.png"), false);

		TextFieldStyle eduCountStyle = new TextFieldStyle();
		eduCountStyle.fontColor = Color.BLACK;
		eduCountStyle.font = arial64;
		eduCountStyle.font.setScale(screenWidth / 1280f);
		eduCount = new TextField("" + educationPointsThisRound, eduCountStyle);
		eduCount.setX(78.5f * screenWidth / 100f);
		eduCount.setY(80f * screenHeight / 100f);
		eduCount.setDisabled(true);
		stage.addActor(eduCount);

		TextFieldStyle finishedMessageStyle = new TextFieldStyle();
		finishedMessageStyle.fontColor = Color.BLACK;
		finishedMessageStyle.font = game.font;
		finishedMessageStyle.font.setScale(screenWidth / 1200f);
		finishedMessage1 = new TextField("You earned", finishedMessageStyle);
		finishedMessage2 = new TextField("" + educationPointsThisRound
				+ " education points", finishedMessageStyle);
		finishedMessage1.setWidth(screenWidth);
		finishedMessage1.setX(19.5f * screenWidth / 100f);
		finishedMessage1.setY(50f * screenHeight / 100f);
		finishedMessage1.setDisabled(true);
		finishedMessage2.setWidth(screenWidth);
		if (pref.getInteger("Education", 0) > 9) {
			finishedMessage2.setX(13f * screenWidth / 100f);
		} else {
			finishedMessage2.setX(11f * screenWidth / 100f);
		}
		finishedMessage2.setY(35f * screenHeight / 100f);
		finishedMessage2.setDisabled(true);

		correctBell = Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/correctBell.mp3"));
		incorrectBuzz = Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/incorrectBuzz.mp3"));

		nextButtonImage = new Image(
				game.generalTextureAtlas.findRegion("nextButton"));
		nextButtonImage.setScale(screenWidth / 1280f);
		nextButtonImage.setX(74f * screenWidth / 100f);
		nextButtonImage.setY(10f * screenHeight / 100f);
		nextButtonImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				pref.putInteger("fromAdventureScreen", 0);
				stage.addActor(blackImage);
				blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
					@Override
					public boolean act(float delta) {
						schoolTheme.stop();
						dispose();
						game.setScreen(new BankScreen(game));
						return false;
					}
				}));
			}
		});

		phrases = new Vector<String>();
		sentences = new Vector<String>();

		if (Math.random() * 10 < 5) {
			phrases.add("JOB");
			phrases.add("earn");
			phrases.add("save");
			phrases.add("give");
			phrases.add("coins");
			phrases.add("bank");
			phrases.add("budget");
			phrases.add("interest");
			phrases.add("goods");
			phrases.add("borrow");
			phrases.add("happy");

			sentences
					.add("Earning money usually \nmeans to do a JOB \nand get money in return");
			sentences
					.add("You can EARN money by \ndoing different types of \njobs");
			sentences.add("We SAVE money to use \nlater in life");
			sentences
					.add("Sharing means to GIVE \nsome of what you have \nto someone else as a gift");
			sentences
					.add("To spend money, we need \nCOINS and notes that \nare special to that part \nof the World!");
			sentences
					.add("You can save money in a \ncoin jar or a BANK account.");
			sentences
					.add("A BUDGET is used to help \nyou spend money carefully");
			sentences
					.add("Saving in a bank account \nearns you extra money \ncalled INTEREST");
			sentences.add("	We spend money to pay for \nGOODS and services.");
			sentences
					.add("When you BORROW money, \nyou are taking money \nfrom someone else to pay \nback later");
			sentences.add("Sharing helps to make you \nand other people HAPPY");

		} else {

			phrases.add("happy");
			phrases.add("borrow");
			phrases.add("goods");
			phrases.add("interest");
			phrases.add("budget");
			phrases.add("bank");
			phrases.add("coins");
			phrases.add("give");
			phrases.add("save");
			phrases.add("earn");
			phrases.add("JOB");

			sentences.add("Sharing helps to make you \nand other people HAPPY");
			sentences
					.add("When you BORROW money, \nyou are taking money \nfrom someone else to pay \nback later");
			sentences.add("	We spend money to pay for \nGOODS and services.");
			sentences
					.add("Saving in a bank account \nearns you extra money \ncalled INTEREST");
			sentences
					.add("A BUDGET is used to help \nyou spend money carefully");
			sentences
					.add("You can save money in a \ncoin jar or a BANK account.");
			sentences
					.add("To spend money, we need \nCOINS and notes that \nare special to that part \nof the World!");
			sentences
					.add("Sharing means to GIVE \nsome of what you have \nto someone else as a gift");
			sentences.add("We SAVE money to use \nlater in life");
			sentences
					.add("You can EARN money by \ndoing different types of \njobs");
			sentences
					.add("Earning money usually \nmeans to do a JOB \nand get money in return");

		}
		listit = phrases.listIterator();
		listit2 = sentences.listIterator();

		group = new Group();
		draggroup = new Group();

		style.font = arial64;
		style.font.setScale(0.7f);

		jumble1 = new Label("", style);
		// jumble1.setScale(0.25f);
		jumble1.setWidth(screenWidth);
		jumble1.setX(18f * screenWidth / 100f);
		jumble1.setY(80f * screenHeight / 100f);
		stage.addActor(jumble1);

		imagebar1 = new Image(game.generalTextureAtlas.findRegion("bar"));
		imagebar2 = new Image(game.generalTextureAtlas.findRegion("grayBar"));

		imagebar1.setWidth(0);
		imagebar2.setWidth(screenWidth);

		imagebar1.setX(0);
		// imagebar1.setScaleY(0.02f);
		imagebar2.setX(0);
		// imagebar2.setScaleY(0.02f);
		imagebar1.setY(screenHeight - screenHeight / 20);
		imagebar2.setY(screenHeight - screenHeight / 20);

		skip = new Image(game.generalTextureAtlas.findRegion("skip"));
		skip.setX(80f * screenWidth / 100f);
		skip.setY(5f * screenHeight / 100f);
		skip.setScale(screenWidth / 1280f);
		skip.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				timeCount = 298;
			}
		});

		stage.addActor(imagebar2);
		stage.addActor(imagebar1);

		escapeImage = new Image(
				game.generalTextureAtlas.findRegion("HomeButton"));
		escapeImage.setX(screenWidth - screenWidth / 10f);
		escapeImage.setY(60f * screenHeight / 100f);
		escapeImage.setScale(0.70f);
		escapeImage.addListener(new ClickListener() {

			public void clicked(InputEvent event, float x, float y) {
				// the last action will move to the next screen
				schoolTheme.stop();
				dispose();
				game.setScreen(new BankScreen(game));
			}
		});

		stage.addActor(escapeImage);

		stage.addActor(group);
		stage.addActor(draggroup);

		Image checkImage = new Image(
				game.generalTextureAtlas.findRegion("tickButton"));
		checkImage.setX(80f * screenWidth / 100f);
		checkImage.setY(1f * screenWidth / 100f);
		// checkImage.setScale(0.25f);
		checkImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {

				String checkphrase = "";
				checkphrase = Sort();

				if (checkphrase.equalsIgnoreCase(catchphrase)) {

					// save.setText("Good job! :)");

					correctImage = new Image(game.schoolAtlas.findRegion("schoolCorrect"));
					correctImage.setX(0.8f * screenWidth);
					correctImage.setY(0.3f * screenHeight);
					starGroup.addActor(correctImage);
					correctBell.play();
					faceCounter = 50;

					nextword = true;
					educationPointsThisRound++;
					eduCount.setText("" + educationPointsThisRound * 10);

					if (quest) {
						if (educationPointsThisRound >= 5) {
							finishedMessage1.setText("");
							finishedMessage2.setText("Quest Completed");
							pref.putInteger("Education_new",
									pref.getInteger("Education", 0)
											+ educationPointsThisRound * 10
											+ 30);
							pref.flush();
						} else {
							finishedMessage1.setText("");
							finishedMessage2.setText("Quest Failed");
							pref.putInteger("Education_new",
									pref.getInteger("Education", 0)
											+ educationPointsThisRound * 2);
							pref.flush();
						}
					} else {
						finishedMessage2.setText("" + educationPointsThisRound
								* 10 + " education points");
						System.out.println("education new is "
								+ pref.getInteger("Education_new", 0));
						pref.putInteger("Education_new",
								pref.getInteger("Education", 0)
										+ educationPointsThisRound * 10);
						pref.flush();
					}
				}

				else {
					// save.setText("Whoops! Try again");
					incorrectImage = new Image(game.schoolAtlas
							.findRegion("schoolIncorrect"));
					starGroup.addActor(incorrectImage);
					incorrectBuzz.play();
					incorrectImage.setX(0.8f * screenWidth);
					incorrectImage.setY(0.3f * screenHeight);
					faceCounter = 50;

				}
				// stage.addActor(save);
				// the last action will move to the next screen

			}
		});

		stage.addActor(checkImage);
		Image instruction = new Image(
				new Texture("data/Instructions/word1.png"));
		introGroup.addActor(instruction);
		stage.addActor(introGroup);

		blackImage = new Image(game.generalTextureAtlas.findRegion("black"));
		blackImage.setFillParent(true);
		stage.addActor(blackImage);
		blackImage.addAction(sequence(delay(0.75f), fadeOut(0.1f),
				Actions.removeActor()));

		schoolTheme.play();
	}

	@Override
	public void render(float delta) {

		if (faceCounter > 0) {
			faceCounter--;
		} else {
			starGroup.clear();
		}

		if (nextword && !stop) {
			if (listit.hasNext()) {
				catchphrase = (String) listit.next();
				sentencedisplayed = (String) listit2.next();
			} else{
				listit = phrases.listIterator();
				stop = true;
				stage.addActor(popupImage);
				stage.addActor(finishedMessage1);
				stage.addActor(finishedMessage2);
				stage.addActor(nextButtonImage);
			}
			System.gc();
			genLetters(catchphrase);
			intializedrag();
		}
		if (TimeUtils.nanoTime() - timediff > 60000000) {
			timediff = TimeUtils.nanoTime();
			timeCount++;

			if (timeCount == 60) {
				introGroup.clear();
				Image instruction = new Image(new Texture(
						"data/Instructions/word2.png"));
				introGroup.addActor(instruction);
				introGroup.addActor(skip);
			} else if (timeCount == 140) {
				introGroup.clear();
				Image instruction = new Image(new Texture(
						"data/Instructions/word3.png"));
				introGroup.addActor(instruction);
				introGroup.addActor(skip);
			} else if (timeCount == 200) {
				introGroup.clear();
				Image instruction = new Image(new Texture(
						"data/Instructions/word4.png"));
				introGroup.addActor(instruction);
				introGroup.addActor(skip);
			} else if (timeCount == 240) {
				introGroup.clear();
				Image instruction = new Image(new Texture(
						"data/Instructions/word5.png"));
				introGroup.addActor(instruction);
				introGroup.addActor(skip);
			} else if (timeCount == 300) {
				introGroup.clear();
				Image instruction = new Image(
						game.generalTextureAtlas.findRegion("goText"));
				instruction.setX(30f * screenWidth / 100f);
				instruction.setY(30f * screenHeight / 100f);
				introGroup.addActor(instruction);
			} else if (timeCount == 320) {
				introGroup.clear();
				stop = false;
			}

			if (timeCount > 360 && !stop) {
				imagebar1.setWidth(screenWidth / 500 * count++);
				System.out.println(count + "," + imagebar1.getWidth());
				if (imagebar1.getWidth() >= screenWidth) {

					stop = true;

					stage.addActor(popupImage);
					stage.addActor(finishedMessage1);
					stage.addActor(finishedMessage2);
					stage.addActor(nextButtonImage);

				}
			}
		}
		jumble1.setText(sentencedisplayed);
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void dispose() {
		schoolTheme.dispose();
		stage.dispose();
		arial64.dispose();
	}

	public void genLetters(String phrase) {

		set = new HashSet<Integer>();
		group.clear();
		draggroup.clear();
		nextword = false;
		System.out.println("Here");

		images = new Image[phrase.length()];

		for (int i = 0; i < phrase.length(); i++) {
			String word = String.valueOf(phrase.charAt(i)).toUpperCase();
			images[i] = new Image(
					game.schoolAtlas.findRegion("letter" + word));
			images[i].setName(word);
			images[i].setScale(0.7f);
			group.addActor(images[i]);
		}

		for (int j = 0; j < phrase.length(); j++) {

			int index = index(phrase.length());
			while (index == -1) {
				index = index(phrase.length());
			}
			images[index].setX((j + 2) * (8f * screenWidth / 100f));
			images[index].setY(36 * screenHeight / 100);

		}

	}

	public int index(int length) {
		final Random rnd = new Random();

		for (;;) {

			final int r = Math.abs(rnd.nextInt()) % length;

			if (set.add(r)) {
				System.out.println(r);
				return r;
			} else
				return -1;
		}

	}

	public String Sort() {
		Image[] copyImage = images.clone();

		int j;
		boolean flag = true; // set flag to true to begin first pass
		Image temp = new Image(); // holding variable

		while (flag) {
			flag = false; // set flag to false awaiting a possible swap
			for (j = 0; j < copyImage.length - 1; j++) {
				if (copyImage[j].getX() > copyImage[j + 1].getX()) // change to
				// > for
				// ascending
				// sort
				{
					temp = copyImage[j]; // swap elements
					copyImage[j] = copyImage[j + 1];
					copyImage[j + 1] = temp;
					flag = true; // shows a swap occurred
				}
			}
		}
		String phrase = "";
		for (int i = 0; i < copyImage.length; i++) {

			phrase += copyImage[i].getName();

		}
		return phrase;
	}

	public void intializedrag() {

		for (int i = 0; i < images.length; i++) {
			final int j = i;
			images[j].addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					images[j].setPosition(
							images[j].getX() - images[j].getWidth() / 2 + x,
							(images[j].getY() - images[j].getHeight() / 2 + y));
					// snapto(oneImage,x,y);
					// evaluate(oneImage);
					group.removeActor(images[j]);
					draggroup.addActor(images[j]);
					//
				}
			});
		}

	}

}
