package com.eduspire.starsavers;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class ShopScreen extends AbstractScreen {

	String Prefs = "StarCharacters";
	Preferences pref;

	public Texture background, avatarTexture, bedTexture, adventureTexture;
	private Stage stage;
	private Image backgroundImage, item1, item2, item3, item4, item5, item6,
			item7, item8;
	private Image shirtitem1, shirtitem2, shirtitem3, shirtitem4, shirtitem5,
			shirtitem6, shirtitem7, shirtitem8;
	private Image shadesitem1, shadesitem2, shadesitem3, shadesitem4,
			shadesitem5, shadesitem6, shadesitem7, shadesitem8;
	private Image hatitem1, hatitem2, hatitem3, hatitem4, hatitem5, hatitem6,
			hatitem7, hatitem8;
	private Image pantsitem1, pantsitem2, pantsitem3, pantsitem4, pantsitem5,
			pantsitem6, pantsitem7, pantsitem8, blackImage;
	Image setblankI, popupImage, nextButtonImage;

	Music shoppingTheme;
	Sound cashRegister;

	boolean setblank = false;

	TextField finishedMessage1, finishedMessage2;
	int[] shirtsArray = new int[8];
	int[] pantsArray = new int[8];
	int[] shadesArray = new int[8];
	int[] hatsArray = new int[8];
	
	boolean[] shirtsFlag = new boolean[8];
	boolean[] shadesFlag = new boolean[8];
	boolean[] hatsFlag = new boolean[8];
	boolean[] pantsFlag = new boolean[8];

	private SpriteBatch batch;
	int screenWidth, screenHeight;

	boolean pants = false, shirts = false, shades = false, hats = true;

	int spent = 0;
	// 1121 ; 19.5f
	// item6.setX(971);
	// item6.setY(175);
	Image demo;
	float shirt1x, shirt2x, shirt3x, shirt4x, shirt5x, shirt6x, shirt7x,
			shirt8x, shirt1y, shirt2y, shirt3y, shirt4y, shirt5y, shirt6y,
			shirt7y, shirt8y;
	float shirtLocationX, shirtLocationY;
	float pantsLocationX, pantsLocationY;
	float shirtTempX, shirtTempY;
	float shirtShopX, shirtShopY;
	float shadesShopX;
	Group group;
	boolean switched = false;
	TextField costText;

	Group popupGroup = new Group();

	public ShopScreen(StarSavers game) {
		super(game);
	}

	void placeImage(Image imageName, Float scale, Float x, Float y) {
		imageName.setScale(scale);
		imageName.setX(x);
		imageName.setY(y);
	}

	@Override
	public void show() {

		pref = Gdx.app.getPreferences(Prefs);

		shoppingTheme = Gdx.audio.newMusic(Gdx.files
				.internal("data/Sound/shoppingTheme.mp3"));
		shoppingTheme.setLooping(true);
		
		cashRegister = Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/cashRegister.mp3"));

		System.out.println("shopscreen:" + pref.getInteger("Savings", 0));
		System.out.println("shopscreen:" + pref.getInteger("Savings_new", 0));

		group = new Group();
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		Texture.setEnforcePotImages(false);

		
		shirtitem1 = new Image(game.shopAtlas.findRegion("tshirt1"));
		shirtitem2 = new Image(game.shopAtlas.findRegion("tshirt2"));
		shirtitem3 = new Image(game.shopAtlas.findRegion("tshirt3"));
		shirtitem4 = new Image(game.shopAtlas.findRegion("tshirt4"));
		shirtitem5 = new Image(game.shopAtlas.findRegion("tshirt5"));
		shirtitem6 = new Image(game.shopAtlas.findRegion("tshirt6"));
		shirtitem7 = new Image(game.shopAtlas.findRegion("tshirt7"));
		shirtitem8 = new Image(game.shopAtlas.findRegion("tshirt8"));

		pantsitem1 = new Image(game.shopAtlas.findRegion("pants1"));
		pantsitem2 = new Image(game.shopAtlas.findRegion("pants2"));
		pantsitem3 = new Image(game.shopAtlas.findRegion("pants3"));
		pantsitem4 = new Image(game.shopAtlas.findRegion("pants4"));
		pantsitem5 = new Image(game.shopAtlas.findRegion("pants5"));
		pantsitem6 = new Image(game.shopAtlas.findRegion("pants6"));
		pantsitem7 = new Image(game.shopAtlas.findRegion("pants7"));
		pantsitem8 = new Image(game.shopAtlas.findRegion("pants8"));

		shadesitem1 = new Image(game.shopAtlas.findRegion("shades1"));
		shadesitem2 = new Image(game.shopAtlas.findRegion("shades2"));
		shadesitem3 = new Image(game.shopAtlas.findRegion("shades3"));
		shadesitem4 = new Image(game.shopAtlas.findRegion("shades4"));
		shadesitem5 = new Image(game.shopAtlas.findRegion("shades5"));
		shadesitem6 = new Image(game.shopAtlas.findRegion("shades6"));
		shadesitem7 = new Image(game.shopAtlas.findRegion("shades7"));
		shadesitem8 = new Image(game.shopAtlas.findRegion("shades8"));

		hatitem1 = new Image(game.shopAtlas.findRegion("cap1"));
		hatitem2 = new Image(game.shopAtlas.findRegion("cap2"));
		hatitem3 = new Image(game.shopAtlas.findRegion("cap3"));
		hatitem4 = new Image(game.shopAtlas.findRegion("cap4"));
		hatitem5 = new Image(game.shopAtlas.findRegion("cap5"));
		hatitem6 = new Image(game.shopAtlas.findRegion("cap6"));
		hatitem7 = new Image(game.shopAtlas.findRegion("cap7"));
		hatitem8 = new Image(game.shopAtlas.findRegion("cap8"));

		// shirt1x =
		shirt2x = screenWidth / 1.6202f;
		shirt3x = screenWidth / 1.4545f;
		shirt4x = screenWidth / 1.5802f;
		shirt5x = screenWidth / 4.4137f;
		shirt6x = screenWidth / 3.6571f;
		shirt7x = screenWidth / 2.16949f;
		shirt8x = screenWidth / 3.6571f;

		shirt1y = screenHeight / 1.25217f;
		shirt2y = screenHeight / 1.411764f;
		shirt3y = screenHeight / 2.32258f;
		shirt4y = screenHeight / 6F;
		shirt6y = screenHeight / 6.54545f;
		shirt7y = screenHeight / 18f;
		// shirt8y = screenWidth/shirt2;

		shirtLocationX = screenWidth / 1.1459f;
		shirtLocationY = screenHeight / 9.47368f;
		shirtTempX = 0.3515f * screenWidth;
		shirtTempY = 0.4305f * screenHeight;
		shirtShopX = 0.546875f * screenWidth;
		shadesShopX = 0.5f * screenWidth;
		pantsLocationX = 0.8757f * screenWidth;
		pantsLocationY = 0.04697f * screenHeight;

		batch = new SpriteBatch();
		boolean maintainAspectRatio = true;
		stage = new Stage(screenWidth, screenHeight, maintainAspectRatio);
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);
		pref = Gdx.app.getPreferences(Prefs);

		BitmapFont sketchFont1 = new BitmapFont(
				Gdx.files.internal("data/Fonts/arial64.fnt"),
				Gdx.files.internal("data/Fonts/arial64.png"), false);
		TextFieldStyle costStyle = new TextFieldStyle();
		costStyle.fontColor = Color.BLACK;
		costStyle.font = sketchFont1;
		costStyle.font.setScale(screenWidth / 900f);
		costText = new TextField("", costStyle);
		costText.setX(59f * screenWidth / 100f);
		costText.setY(44f * screenHeight / 100f);
		costText.setDisabled(true);

		Texture backgroundTexture = new Texture("data/Backgrounds/shopScreenBackground.png");
		backgroundTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		Drawable splashDrawable = new TextureRegionDrawable(new TextureRegion(
				backgroundTexture, 0, 0, 1280, 720));
		backgroundImage = new Image(splashDrawable);
		backgroundImage.setFillParent(true);
		stage.addActor(backgroundImage);
		stage.addActor(group);

		pref.putInteger("Savings_new", pref.getInteger("Savings", 0));

		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("x " + x + "y " + y + "" + screenHeight);

				if ((x > (0.029687f * screenWidth))
						&& (x < (0.11094f * screenWidth))) {

					if ((y > 0.25694f * screenHeight)
							&& (y < 0.39305f * screenHeight)) {
						pants = true;
						switched = false;
						shirts = false;
						shades = false;
						hats = false;
						System.out.print("pantson");
					}
				}
				if ((x > (0.029687f * screenWidth))
						&& (x < (0.11094f * screenWidth))) {

					if ((y > 0.7764f * screenHeight)
							&& (y < 0.9042f * screenHeight)) {
						pants = false;
						switched = false;
						shirts = true;
						shades = false;
						hats = false;
						System.out.print("shirtson");
					}
				}
				if ((x > (0.029687f * screenWidth))
						&& (x < (0.11094f * screenWidth))) {

					if ((y > 0.4264f * screenHeight)
							&& (y < 0.5722f * screenHeight)) {
						pants = false;
						switched = false;
						shirts = false;
						shades = false;
						hats = true;
						System.out.print("caps");
					}
				}
				if ((x > (0.029687f * screenWidth))
						&& (x < (0.11094f * screenWidth))) {

					if ((y > 0.5972f * screenHeight)
							&& (y < 0.7361f * screenHeight)) {
						pants = false;
						switched = false;
						shirts = false;
						shades = true;
						hats = false;
						System.out.print("shades");
					}
				}

			}
		});

		stage.addActor(costText);

		popupImage = new Image(game.generalTextureAtlas.findRegion("popup"));
		popupImage.setX(10f * screenWidth / 100f);
		popupImage.setY(10f * screenHeight / 100f);
		popupImage.setSize(80f * screenWidth / 100f, 80f * screenHeight / 100f);

		
		TextFieldStyle finishedMessageStyle = new TextFieldStyle();
		finishedMessageStyle.fontColor = Color.BLACK;
		finishedMessageStyle.font = game.font;
		finishedMessageStyle.font.setScale(screenWidth / 1200f);
		finishedMessage1 = new TextField("You need to", finishedMessageStyle);
		finishedMessage1.setWidth(screenWidth);
		finishedMessage1.setX(19.5f * screenWidth / 100f);
		finishedMessage1.setY(50f * screenHeight / 100f);
		finishedMessage2 = new TextField("keep saving", finishedMessageStyle);
		finishedMessage1.setDisabled(true);
		finishedMessage2.setWidth(screenWidth);
		finishedMessage2.setX(16f * screenWidth / 100f);
		finishedMessage2.setY(35f * screenHeight / 100f);
		finishedMessage2.setDisabled(true);

		nextButtonImage = new Image(game.generalTextureAtlas.findRegion("nextButton"));
		nextButtonImage.setScale(screenWidth / 2010f);
		nextButtonImage.setX(74f * screenWidth / 100f);
		nextButtonImage.setY(10f * screenHeight / 100f);
		nextButtonImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				popupGroup.clear();
			}
		});

		stage.addActor(popupGroup);

		// THESE ARE FOR THE POPUP
		// popupGroup.addActor(popupImage);
		// popupGroup.addActor(finishedMessage1);
		// popupGroup.addActor(finishedMessage2);
		// popupGroup.addActor(nextButtonImage);

		Image adventureImage = new Image(game.generalTextureAtlas.findRegion("nextButton"));
		adventureImage.setScale(screenWidth / 2010f);
		adventureImage.setX(88f * screenWidth / 100f);
		adventureImage.setY(80f * screenHeight / 100f);
		adventureImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {

				for (int i : shirtsArray)
					spent += i;
				for (int i : pantsArray)
					spent += i;
				for (int i : shadesArray)
					spent += i;
				for (int i : hatsArray)
					spent += i;

				pref.putInteger("fromAdventureScreen", 0);
				pref.flush();

				// ///////////////////////////////////////////////////////////////////////

				// pref.putInteger("Savings_new", pref.getInteger("Savings", 0)
				// + spent);
				System.out.println("shopscreen::::"
						+ pref.getInteger("Savings", 0));
				pref.flush();
				System.out.println("shopscreen::new::"
						+ pref.getInteger("Savings_new", 0));
				stage.addActor(blackImage);

				blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
					@Override
					public boolean act(float delta) {
						shoppingTheme.stop();
						dispose();
						game.setScreen(new BankScreen(game));
						return false;
					}
				}));
			}
		});
		stage.addActor(adventureImage);

		blackImage = new Image(game.generalTextureAtlas.findRegion("black"));
		blackImage.setFillParent(true);
		stage.addActor(blackImage);
		blackImage.addAction(sequence(delay(0.1f), fadeOut(0.1f),
				Actions.removeActor()));

		shoppingTheme.play();

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();

		for (int i : shirtsArray)
			spent += i;
		for (int i : pantsArray)
			spent += i;
		for (int i : shadesArray)
			spent += i;
		for (int i : hatsArray)
			spent += i;

		if (shirts) {

			if (!switched) {
				switched = true;

				group.removeActor(item1);
				group.removeActor(item2);
				group.removeActor(item3);
				group.removeActor(item4);
				group.removeActor(item5);
				group.removeActor(item6);
				group.removeActor(item7);
				group.removeActor(item8);

				item1 = shirtitem1;
				item2 = shirtitem2;
				item3 = shirtitem3;
				item4 = shirtitem4;
				item5 = shirtitem5;
				item6 = shirtitem6;
				item7 = shirtitem7;
				item8 = shirtitem8;

				item1.setX(shirt7x);
				item1.setY(shirt1y);
				item1.setName("5");

				item2.setX(shirt2x);
				item2.setY(shirt2y);
				item2.setName("10");

				item3.setX(shirt3x);
				item3.setY(shirt3y);
				item3.setName("15");

				item4.setX(shirt4x);
				item4.setY(shirt4y);
				item4.setName("20");

				item5.setX(shirt5x);
				item5.setY(shirt3y);
				item5.setName("25");

				item6.setX(shirt6x);
				item6.setY(shirt6y);
				item6.setName("30");

				item7.setX(shirt7x);
				item7.setY(shirt7y);
				item7.setName("35");

				item8.setX(shirt8x);
				item8.setY(shirt2y);
				item8.setName("40");
			}

			item1.addListener(new ClickListener() {

				public void clicked(InputEvent event, float x, float y) {

					if (item1.getX() == shirt7x) {
						display(item1);
						demo = new Image(game.shopAtlas.findRegion("tshirt1"));
						demo.setX(shirtLocationX);
						demo.setY(shirtLocationY);
						group.addActor(demo);
						costText.setText(item1.getName());
					}
				}
			});

			item2.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					if (item2.getX() == shirt2x) {
						display(item2);
						demo = new Image(game.shopAtlas.findRegion("tshirt2"));
						demo.setX(shirtLocationX);
						demo.setY(shirtLocationY);
						stage.addActor(demo);
						costText.setText(item2.getName());
					}
				}
			});
			item3.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					if (item3.getX() == shirt3x) {
						display(item3);
						demo = new Image(game.shopAtlas.findRegion("tshirt3"));
						demo.setX(shirtLocationX);
						demo.setY(shirtLocationY);
						stage.addActor(demo);
						costText.setText(item3.getName());
					}
				}
			});
			item4.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					if (item4.getX() == shirt4x) {
						display(item4);
						demo = new Image(game.shopAtlas.findRegion("tshirt4"));
						demo.setX(shirtLocationX);
						demo.setY(shirtLocationY);
						stage.addActor(demo);
						costText.setText(item4.getName());
					}
				}
			});

			item5.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					if (item5.getX() == shirt5x) {
						display(item5);
						demo = new Image(game.shopAtlas.findRegion("tshirt5"));
						demo.setX(shirtLocationX);
						demo.setY(shirtLocationY);
						stage.addActor(demo);
						costText.setText(item5.getName());
					}
				}
			});

			item6.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					if (item6.getX() == shirt6x) {
						display(item6);
						demo = new Image(game.shopAtlas.findRegion("tshirt6"));
						demo.setX(shirtLocationX);
						demo.setY(shirtLocationY);
						stage.addActor(demo);
						costText.setText(item6.getName());
					}
				}
			});

			item7.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					if (item7.getX() == shirt7x) {
						display(item7);
						demo = new Image(game.shopAtlas.findRegion("tshirt7"));
						demo.setX(shirtLocationX);
						demo.setY(shirtLocationY);
						stage.addActor(demo);
						costText.setText(item7.getName());
					}
				}
			});

			item8.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					if (item8.getX() == shirt8x) {
						display(item8);
						demo = new Image(game.shopAtlas.findRegion("tshirt8"));
						demo.setX(shirtLocationX);
						demo.setY(shirtLocationY);
						stage.addActor(demo);
						costText.setText(item8.getName());
					}
				}
			});

			item1.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item1.getY() == shirtTempY) {
						float dx = x - item1.getWidth() * 0.5f;
						if (dx > 0) {
							item1.setX(item1.getX() + dx);
							if (item1.getX() > 0.3984f * screenWidth) {
								item1.setX(shirtShopX);
								if (shirtsFlag[0] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item1
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shirtsFlag[0] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item1
															.getName()));
									pref.putInteger("shirtStyle", 1);
									pref.flush();
									cashRegister.play();
									shirtsFlag[0] = true;
								}
							}
						}
					}
				}
			});

			item2.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item2.getY() == shirtTempY) {
						float dx = x - item2.getWidth() * 0.5f;
						if (dx > 0) {
							item2.setX(item2.getX() + dx);
							if (item2.getX() > 0.3984f * screenWidth) {
								item2.setX(shirtShopX);
								if (shirtsFlag[1] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item2
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shirtsFlag[1] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item2
															.getName()));
									pref.putInteger("shirtStyle", 2);
									cashRegister.play();
									pref.flush();
									shirtsFlag[1] = true;
								}
							}
						}
					}
				}
			});

			item3.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item3.getY() == shirtTempY) {
						float dx = x - item3.getWidth() * 0.5f;
						if (dx > 0) {
							item3.setX(item3.getX() + dx);
							if (item3.getX() > 0.3984f * screenWidth) {
								item3.setX(shirtShopX);
								if (shirtsFlag[2] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item3
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shirtsFlag[2] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item3
															.getName()));
									pref.putInteger("shirtStyle", 3);
									cashRegister.play();
									pref.flush();
									shirtsFlag[2] = true;
								}
							}
						}
					}
				}
			});

			item4.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item4.getY() == shirtTempY) {
						float dx = x - item4.getWidth() * 0.5f;
						if (dx > 0) {
							item4.setX(item4.getX() + dx);
							if (item4.getX() > 0.3984f * screenWidth) {
								item4.setX(shirtShopX);
								if (shirtsFlag[3] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item4
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shirtsFlag[3] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item4
															.getName()));
									pref.putInteger("shirtStyle", 4);
									cashRegister.play();
									pref.flush();
									shirtsFlag[3] = true;
								}
							}
						}
					}
				}
			});

			item5.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item5.getY() == shirtTempY) {
						float dx = x - item5.getWidth() * 0.5f;
						if (dx > 0) {
							item5.setX(item5.getX() + dx);
							if (item5.getX() > 0.3984f * screenWidth) {
								item5.setX(shirtShopX);
								if (shirtsFlag[4] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item5
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shirtsFlag[4] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item5
															.getName()));
									pref.putInteger("shirtStyle", 5);
									cashRegister.play();
									pref.flush();
									shirtsFlag[4] = true;
								}
							}
						}
					}
				}
			});

			item6.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item6.getY() == shirtTempY) {
						float dx = x - item6.getWidth() * 0.5f;
						if (dx > 0) {
							item6.setX(item6.getX() + dx);
							if (item6.getX() > 0.3984f * screenWidth) {
								item6.setX(shirtShopX);
								if (shirtsFlag[5] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item6
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shirtsFlag[5] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item6
															.getName()));
									pref.putInteger("shirtStyle", 6);
									cashRegister.play();
									pref.flush();
									shirtsFlag[5] = true;
								}
							}
						}
					}
				}
			});

			item7.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item7.getY() == shirtTempY) {
						float dx = x - item7.getWidth() * 0.5f;
						if (dx > 0) {
							item7.setX(item7.getX() + dx);
							if (item7.getX() > 0.3984f * screenWidth) {
								item7.setX(shirtShopX);
								if (shirtsFlag[6] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item7
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shirtsFlag[6] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item7
															.getName()));
									pref.putInteger("shirtStyle", 7);
									cashRegister.play();
									pref.flush();
									shirtsFlag[6] = true;
								}
							}
						}
					}
				}
			});

			item8.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item8.getY() == shirtTempY) {
						float dx = x - item8.getWidth() * 0.5f;
						if (dx > 0) {
							item8.setX(item8.getX() + dx);
							if (item8.getX() > 0.3984f * screenWidth) {
								item8.setX(shirtShopX);
								if (shirtsFlag[7] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item8
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shirtsFlag[7] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item8
															.getName()));
									pref.putInteger("shirtStyle", 8);
									cashRegister.play();
									pref.flush();
									shirtsFlag[7] = true;
								}
							}
						}
					}
				}
			});
		}

		if (pants) {
			if (!switched) {

				switched = true;

				group.removeActor(item1);
				group.removeActor(item2);
				group.removeActor(item3);
				group.removeActor(item4);
				group.removeActor(item5);
				group.removeActor(item6);
				group.removeActor(item7);
				group.removeActor(item8);

				item1 = pantsitem1;
				item2 = pantsitem2;
				item3 = pantsitem3;
				item4 = pantsitem4;
				item5 = pantsitem5;
				item6 = pantsitem6;
				item7 = pantsitem7;
				item8 = pantsitem8;

				item1.setX(shirt7x);
				item1.setY(shirt1y);
				item1.setName("6");
				// item1.setScaleX(0.538f);
				// item1.setScaleY(0.538f);

				item2.setX(shirt2x);
				item2.setY(shirt2y);
				item2.setName("12");
				// stage.addActor(item2);

				item3.setX(shirt3x);
				item3.setY(shirt3y);
				item3.setName("18");
				// stage.addActor(item3);

				item4.setX(shirt4x);
				item4.setY(shirt4y);
				item4.setName("24");
				// stage.addActor(item4);

				item5.setX(shirt5x);
				item5.setY(shirt3y);
				item5.setName("29");
				// stage.addActor(item5);

				item6.setX(shirt6x);
				item6.setY(shirt6y);
				// item6.setScale(0.6f);
				item6.setName("32");
				// stage.addActor(item6);

				item7.setX(shirt7x);
				item7.setY(shirt7y);
				item7.setName("36");
				// stage.addActor(item7);

				item8.setX(shirt8x);
				item8.setY(shirt2y);
				item8.setName("39");

			}

			item1.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					display(item1);
					demo = new Image(game.shopAtlas.findRegion("pants1"));
					demo.setX(pantsLocationX);
					demo.setY(pantsLocationY);
					stage.addActor(demo);
					costText.setText(item1.getName());
				}
			});
			item2.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					display(item2);
					demo = new Image(game.shopAtlas.findRegion("pants2"));
					demo.setX(pantsLocationX);
					demo.setY(pantsLocationY);
					stage.addActor(demo);
					costText.setText(item2.getName());
				}
			});
			item3.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					display(item3);
					demo = new Image(game.shopAtlas.findRegion("pants3"));
					demo.setX(pantsLocationX);
					demo.setY(pantsLocationY);
					stage.addActor(demo);
					costText.setText(item3.getName());
				}
			});
			item4.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					display(item4);
					demo = new Image(game.shopAtlas.findRegion("pants4"));
					demo.setX(pantsLocationX);
					demo.setY(pantsLocationY);
					stage.addActor(demo);
					costText.setText(item4.getName());
				}
			});
			item5.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					display(item5);
					demo = new Image(game.shopAtlas.findRegion("pants5"));
					demo.setX(pantsLocationX);
					demo.setY(pantsLocationY);
					stage.addActor(demo);
					costText.setText(item5.getName());
				}
			});
			item6.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					display(item6);
					demo = new Image(game.shopAtlas.findRegion("pants6"));
					demo.setX(pantsLocationX);
					demo.setY(pantsLocationY);
					stage.addActor(demo);
					costText.setText(item6.getName());
				}
			});
			item7.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					display(item7);
					demo = new Image(game.shopAtlas.findRegion("pants7"));
					demo.setX(pantsLocationX);
					demo.setY(pantsLocationY);
					stage.addActor(demo);
					costText.setText(item7.getName());
				}
			});
			item8.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					display(item8);
					demo = new Image(game.shopAtlas.findRegion("pants8"));
					demo.setX(pantsLocationX);
					demo.setY(pantsLocationY);
					stage.addActor(demo);
					costText.setText(item8.getName());
				}
			});

			item1.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item1.getY() == shirtTempY) {
						float dx = x - item1.getWidth() * 0.5f;
						if (dx > 0) {
							item1.setX(item1.getX() + dx);
							if (item1.getX() > 0.3984f * screenWidth) {
								item1.setX(shirtShopX);
								if (pantsFlag[0] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item1
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (pantsFlag[0] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item1
															.getName()));
									pref.putInteger("pantsStyle", 1);
									cashRegister.play();
									pref.flush();
									pantsFlag[0] = true;
								}
							}
						}
					}
				}
			});

			item2.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item2.getY() == shirtTempY) {
						float dx = x - item2.getWidth() * 0.5f;
						if (dx > 0) {
							item2.setX(item2.getX() + dx);
							if (item2.getX() > 0.3984f * screenWidth) {
								item2.setX(shirtShopX);
								if (pantsFlag[1] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item2
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (pantsFlag[1] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item2
															.getName()));
									pref.putInteger("pantsStyle", 2);
									cashRegister.play();
									pref.flush();
									pantsFlag[1] = true;
								}
							}
						}
					}
				}
			});

			item3.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item3.getY() == shirtTempY) {
						float dx = x - item3.getWidth() * 0.5f;
						if (dx > 0) {
							item3.setX(item3.getX() + dx);
							if (item3.getX() > 0.3984f * screenWidth) {
								item3.setX(shirtShopX);
								if (pantsFlag[2] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item3
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (pantsFlag[2] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item3
															.getName()));
									pref.putInteger("pantsStyle", 3);
									cashRegister.play();
									pref.flush();
									pantsFlag[2] = true;
								}
							}
						}
					}
				}
			});

			item4.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item4.getY() == shirtTempY) {
						float dx = x - item4.getWidth() * 0.5f;
						if (dx > 0) {
							item4.setX(item4.getX() + dx);
							if (item4.getX() > 0.3984f * screenWidth) {
								item4.setX(shirtShopX);
								if (pantsFlag[3] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item4
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (pantsFlag[3] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item4
															.getName()));
									pref.putInteger("pantsStyle", 4);
									cashRegister.play();
									pref.flush();
									pantsFlag[3] = true;
								}
							}
						}
					}
				}
			});

			item5.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item5.getY() == shirtTempY) {
						float dx = x - item5.getWidth() * 0.5f;
						if (dx > 0) {
							item5.setX(item5.getX() + dx);
							if (item5.getX() > 0.3984f * screenWidth) {
								item5.setX(shirtShopX);
								if (pantsFlag[4] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item5
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (pantsFlag[4] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item5
															.getName()));
									pref.putInteger("pantsStyle", 5);
									cashRegister.play();
									pref.flush();
									pantsFlag[4] = true;
								}
							}
						}
					}
				}
			});

			item6.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item6.getY() == shirtTempY) {
						float dx = x - item6.getWidth() * 0.5f;
						if (dx > 0) {
							item6.setX(item6.getX() + dx);
							if (item6.getX() > 0.3984f * screenWidth) {
								item6.setX(shirtShopX);
								if (pantsFlag[5] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item6
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (pantsFlag[5] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item6
															.getName()));
									pref.putInteger("pantsStyle", 6);
									cashRegister.play();
									pref.flush();
									pantsFlag[5] = true;
								}
							}
						}
					}
				}
			});

			item7.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item7.getY() == shirtTempY) {
						float dx = x - item7.getWidth() * 0.5f;
						if (dx > 0) {
							item7.setX(item7.getX() + dx);
							if (item7.getX() > 0.3984f * screenWidth) {
								item7.setX(shirtShopX);
								if (pantsFlag[6] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item7
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (pantsFlag[6] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item7
															.getName()));
									pref.putInteger("pantsStyle", 7);
									cashRegister.play();
									pref.flush();
									pantsFlag[6] = true;
								}
							}
						}
					}
				}
			});

			item8.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item8.getY() == shirtTempY) {
						float dx = x - item8.getWidth() * 0.5f;
						if (dx > 0) {
							item8.setX(item8.getX() + dx);
							if (item8.getX() > 0.3984f * screenWidth) {
								item8.setX(shirtShopX);
								if (pantsFlag[7] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item8
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (pantsFlag[7] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item8
															.getName()));
									pref.putInteger("pantsStyle", 8);
									cashRegister.play();
									pref.flush();
									pantsFlag[7] = true;
								}
							}
						}
					}
				}
			});
		}

		if (shades) {

			if (!switched) {
				switched = true;

				group.removeActor(item1);
				group.removeActor(item2);
				group.removeActor(item3);
				group.removeActor(item4);
				group.removeActor(item5);
				group.removeActor(item6);
				group.removeActor(item7);
				group.removeActor(item8);

				item1 = shadesitem1;
				item2 = shadesitem2;
				item3 = shadesitem3;
				item4 = shadesitem4;
				item5 = shadesitem5;
				item6 = shadesitem6;
				item7 = shadesitem7;
				item8 = shadesitem8;

				item1.setX(shirt7x - 0.0390625f*screenWidth);
				item1.setY(shirt1y);
				item1.setName("7");

				item2.setX(shirt2x - 0.0390625f*screenWidth);
				item2.setY(shirt2y);
				item2.setName("14");

				item3.setX(shirt3x - 0.0390625f*screenWidth);
				item3.setY(shirt3y);
				item3.setName("21");

				item4.setX(shirt4x - 0.0390625f*screenWidth);
				item4.setY(shirt4y);
				item4.setName("28");

				item5.setX(shirt5x - 0.0390625f*screenWidth);
				item5.setY(shirt3y);
				item5.setName("34");

				item6.setX(shirt6x - 0.0390625f*screenWidth);
				item6.setY(shirt6y);
				item6.setName("42");

				item7.setX(shirt7x - 0.0390625f*screenWidth);
				item7.setY(shirt7y);
				item7.setName("49");

				item8.setX(shirt8x - 0.0390625f*screenWidth);
				item8.setY(shirt2y);
				item8.setName("55");

				item1.addListener(new ClickListener() {

					public void clicked(InputEvent event, float x, float y) {

						if (item1.getX() == shirt7x - 0.0390625f*screenWidth) {

							group.removeActor(demo);
							display(item1);

							demo = new Image(game.shopAtlas.findRegion("shades1"));
							demo.setX(0.7578f*screenWidth);
							demo.setY(0.32638f*screenHeight);

				
							group.addActor(demo);
							costText.setText(item1.getName());
						}
					}
				});

				item2.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item2.getX() == shirt2x - 0.0390625f*screenWidth) {
							group.removeActor(demo);
							display(item2);

							demo = new Image(game.shopAtlas.findRegion("shades2"));
							demo.setX(0.7578f*screenWidth);
							demo.setY(0.32638f*screenHeight);

							group.addActor(demo);
							costText.setText(item2.getName());
						}
					}
				});
				item3.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item3.getX() == shirt3x - 0.0390625f*screenWidth) {
							group.removeActor(demo);
							display(item3);

							demo = new Image(game.shopAtlas.findRegion("shades3"));
							demo.setX(0.7578f*screenWidth);
							demo.setY(0.32638f*screenHeight);

							costText.setText(item3.getName());
						}

					}
				});
				item4.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item4.getX() == shirt4x - 0.0390625f*screenWidth) {
							group.removeActor(demo);
							display(item4);

							demo = new Image(game.shopAtlas.findRegion("shades4"));
							demo.setX(0.7578f*screenWidth);
							demo.setY(0.32638f*screenHeight);

							group.addActor(demo);
							costText.setText(item4.getName());
						}

					}
				});
				item5.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item5.getX() == shirt5x - 0.0390625f*screenWidth) {
							group.removeActor(demo);
							display(item5);

							demo = new Image(game.shopAtlas.findRegion("shades5"));
							demo.setX(0.7578f*screenWidth);
							demo.setY(0.32638f*screenHeight);

							group.addActor(demo);
							costText.setText(item5.getName());
						}

					}
				});
				item6.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item6.getX() == shirt6x - 0.0390625f*screenWidth) {
							group.removeActor(demo);
							display(item6);

							demo = new Image(game.shopAtlas.findRegion("shades6"));
							demo.setX(0.7578f*screenWidth);
							demo.setY(0.32638f*screenHeight);

							group.addActor(demo);
							costText.setText(item6.getName());
						}

					}
				});
				item7.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item7.getX() == shirt7x - 0.0390625f*screenWidth) {
							group.removeActor(demo);
							display(item7);

							demo = new Image(game.shopAtlas.findRegion("shades7"));
							demo.setX(0.7578f*screenWidth);
							demo.setY(0.32638f*screenHeight);

							group.addActor(demo);
							costText.setText(item7.getName());
						}

					}
				});
				item8.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item8.getX() == shirt8x - 0.0390625f*screenWidth) {
							group.removeActor(demo);
							display(item8);

							demo = new Image(game.shopAtlas.findRegion("shades8"));
							demo.setX(0.7578f*screenWidth);
							demo.setY(0.32638f*screenHeight);

							group.addActor(demo);
							costText.setText(item8.getName());
						}

					}
				});
			}
			
			item1.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item1.getY() == shirtTempY - 0.048611f*screenHeight) {
						float dx = x - item1.getWidth() * 0.5f;
						if (dx > 0) {
							item1.setX(item1.getX() + dx);
							if (item1.getX() > 0.3984f * screenWidth) {
								item1.setX(shirtShopX);
								if (shadesFlag[0] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item1
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shadesFlag[0] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item1
															.getName()));
									pref.putInteger("shadesStyle", 1);
									pref.flush();
									cashRegister.play();
									shadesFlag[0] = true;
								}
							}
						}
					}
				}
			});

			item2.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item2.getY() == shirtTempY - 0.048611f*screenHeight) {
						float dx = x - item2.getWidth() * 0.5f;
						if (dx > 0) {
							item2.setX(item2.getX() + dx);
							if (item2.getX() > 0.3984f * screenWidth) {
								item2.setX(shirtShopX);
								if (shadesFlag[1] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item2
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shadesFlag[1] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item2
															.getName()));
									pref.putInteger("shadesStyle", 2);
									cashRegister.play();
									pref.flush();
									shadesFlag[1] = true;
								}
							}
						}
					}
				}
			});

			item3.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item3.getY() == shirtTempY - 0.048611f*screenHeight) {
						float dx = x - item3.getWidth() * 0.5f;
						if (dx > 0) {
							item3.setX(item3.getX() + dx);
							if (item3.getX() > 0.3984f * screenWidth) {
								item3.setX(shirtShopX);
								if (shadesFlag[2] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item3
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shadesFlag[2] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item3
															.getName()));
									pref.putInteger("shadesStyle", 3);
									cashRegister.play();
									pref.flush();
									shadesFlag[2] = true;
								}
							}
						}
					}
				}
			});

			item4.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item4.getY() == shirtTempY - 0.048611f*screenHeight) {
						float dx = x - item4.getWidth() * 0.5f;
						if (dx > 0) {
							item4.setX(item4.getX() + dx);
							if (item4.getX() > 0.3984f * screenWidth) {
								item4.setX(shirtShopX);
								if (shadesFlag[3] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item4
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shadesFlag[3] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item4
															.getName()));
									pref.putInteger("shadesStyle", 4);
									cashRegister.play();
									pref.flush();
									shadesFlag[3] = true;
								}
							}
						}
					}
				}
			});

			item5.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item5.getY() == shirtTempY - 0.048611f*screenHeight) {
						float dx = x - item5.getWidth() * 0.5f;
						if (dx > 0) {
							item5.setX(item5.getX() + dx);
							if (item5.getX() > 0.3984f * screenWidth) {
								item5.setX(shirtShopX);
								if (shadesFlag[4] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item5
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shadesFlag[4] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item5
															.getName()));
									pref.putInteger("shadesStyle", 5);
									cashRegister.play();
									pref.flush();
									shadesFlag[4] = true;
								}
							}
						}
					}
				}
			});

			item6.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item6.getY() == shirtTempY - 0.048611f*screenHeight) {
						float dx = x - item6.getWidth() * 0.5f;
						if (dx > 0) {
							item6.setX(item6.getX() + dx);
							if (item6.getX() > 0.3984f * screenWidth) {
								item6.setX(shirtShopX);
								if (shadesFlag[5] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item6
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shadesFlag[5] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item6
															.getName()));
									pref.putInteger("shadesStyle", 6);
									cashRegister.play();
									pref.flush();
									shadesFlag[5] = true;
								}
							}
						}
					}
				}
			});

			item7.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item7.getY() == shirtTempY - 0.048611f*screenHeight) {
						float dx = x - item7.getWidth() * 0.5f;
						if (dx > 0) {
							item7.setX(item7.getX() + dx);
							if (item7.getX() > 0.3984f * screenWidth) {
								item7.setX(shirtShopX);
								if (shadesFlag[6] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item7
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shadesFlag[6] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item7
															.getName()));
									pref.putInteger("shadesStyle", 7);
									cashRegister.play();
									pref.flush();
									shadesFlag[6] = true;
								}
							}
						}
					}
				}
			});

			item8.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item8.getY() == shirtTempY - 0.048611f*screenHeight) {
						float dx = x - item8.getWidth() * 0.5f;
						if (dx > 0) {
							item8.setX(item8.getX() + dx);
							if (item8.getX() > 0.3984f * screenWidth) {
								item8.setX(shirtShopX);
								if (shadesFlag[7] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item8
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (shadesFlag[7] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item8
															.getName()));
									pref.putInteger("shadesStyle", 8);
									cashRegister.play();
									pref.flush();
									shadesFlag[7] = true;
								}
							}
						}
					}
				}
			});

		}

		if (hats) {

			if (!switched) {
				switched = true;

				group.removeActor(item1);
				group.removeActor(item2);
				group.removeActor(item3);
				group.removeActor(item4);
				group.removeActor(item5);
				group.removeActor(item6);
				group.removeActor(item7);
				group.removeActor(item8);

				item1 = hatitem1;
				item2 = hatitem2;
				item3 = hatitem3;
				item4 = hatitem4;
				item5 = hatitem5;
				item6 = hatitem6;
				item7 = hatitem7;
				item8 = hatitem8;

				item1.setX(shirt7x - 25);
				item1.setY(shirt1y);
				item1.setName("2");
				item1.setScale(0.5f);

				item2.setX(shirt2x);
				item2.setY(shirt2y);
				item2.setName("3");
				item2.setScale(0.5f);

				item3.setX(shirt3x);
				item3.setY(shirt3y);
				item3.setName("4");
				item3.setScale(0.5f);

				item4.setX(shirt4x);
				item4.setY(shirt4y);
				item4.setName("8");
				item4.setScale(0.5f);

				item5.setX(shirt5x - 0.0390625f*screenWidth);
				item5.setY(shirt3y);
				item5.setScale(0.5f);
				item5.setName("9");

				item6.setX(shirt6x);
				item6.setY(shirt6y);
				item6.setScale(0.5f);
				item6.setName("11");

				item7.setX(shirt7x);
				item7.setY(shirt7y);
				item7.setName("16");
				item7.setScale(0.5f);

				item8.setX(shirt8x);
				item8.setY(shirt2y);
				item8.setName("19");
				item8.setScale(0.5f);

				item1.addListener(new ClickListener() {

					public void clicked(InputEvent event, float x, float y) {

						if (item1.getX() == shirt7x - 25) {

							group.removeActor(demo);
							display(item1);

							demo = new Image(game.shopAtlas.findRegion("cap1"));
							demo.setX(0.7890625f*screenWidth);

							// demo.setScale(0.6F);
							demo.setY(0.5f*screenHeight);
							group.addActor(demo);
							costText.setText(item1.getName());
						}
					}
				});

				item2.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item2.getX() == shirt2x) {
							group.removeActor(demo);
							display(item2);

							demo = new Image(game.shopAtlas.findRegion("cap2"));
							demo.setX(0.7890625f*screenWidth);

							// demo.setScale(0.6F);
							demo.setY(0.48611f*screenHeight);
							group.addActor(demo);
							costText.setText(item2.getName());
						}
					}
				});
				item3.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item3.getX() == shirt3x) {
							group.removeActor(demo);
							display(item3);

							demo = new Image(game.shopAtlas.findRegion("cap3"));
							demo.setX(0.7890625f*screenWidth);

							// demo.setScale(0.6F);
							demo.setY(0.48611f*screenHeight);
							group.addActor(demo);
							costText.setText(item3.getName());
						}
					}
				});
				item4.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item4.getX() == shirt4x) {
							group.removeActor(demo);
							display(item4);

							demo = new Image(game.shopAtlas.findRegion("cap4"));
							demo.setX(0.7890625f*screenWidth);


							demo.setY(0.48611f*screenHeight);
							group.addActor(demo);
							costText.setText(item4.getName());
						}
					}
				});
				item5.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item5.getX() == shirt5x - 0.0390625f*screenWidth) {
							group.removeActor(demo);
							display(item5);

							demo = new Image(game.shopAtlas.findRegion("cap5"));
							demo.setX(0.7890625f*screenWidth);

							// demo.setScale(0.6F);
							demo.setY(0.48611f*screenHeight);
							group.addActor(demo);
							costText.setText(item5.getName());
						}
					}
				});
				item6.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item6.getX() == shirt6x) {
							group.removeActor(demo);
							display(item6);

							demo = new Image(game.shopAtlas.findRegion("cap6"));
							demo.setX(0.7890625f*screenWidth);

							// demo.setScale(0.6F);
							demo.setY(0.48611f*screenHeight);
							group.addActor(demo);
							costText.setText(item6.getName());
						}
					}
				});
				item7.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item7.getX() == shirt7x) {
							group.removeActor(demo);
							display(item7);

							demo = new Image(game.shopAtlas.findRegion("cap7"));
							demo.setX(0.7890625f*screenWidth);

							// demo.setScale(0.6F);
							demo.setY(0.48611f*screenHeight);
							group.addActor(demo);
							costText.setText(item7.getName());
						}
					}
				});
				item8.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						if (item8.getX() == shirt8x) {
							group.removeActor(demo);
							display(item8);

							demo = new Image(game.shopAtlas.findRegion("cap8"));
							demo.setX(0.7890625f*screenWidth);

							// demo.setScale(0.6F);
							demo.setY(0.48611f*screenHeight);
							group.addActor(demo);
							costText.setText(item8.getName());
						}
					}
				});
			}

			item1.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item1.getY() == shirtTempY) {
						float dx = x - item1.getWidth() * 0.5f;
						if (dx > 0) {
							item1.setX(item1.getX() + dx);
							if (item1.getX() > 0.3984f * screenWidth) {
								item1.setX(shirtShopX);
								if (hatsFlag[0] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item1
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (hatsFlag[0] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item1
															.getName()));
									pref.putInteger("hatStyle", 1);
									pref.flush();
									cashRegister.play();
									hatsFlag[0] = true;
								}
							}
						}
					}
				}
			});

			item2.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item2.getY() == shirtTempY) {
						float dx = x - item2.getWidth() * 0.5f;
						if (dx > 0) {
							item2.setX(item2.getX() + dx);
							if (item2.getX() > 0.3984f * screenWidth) {
								item2.setX(shirtShopX);
								if (hatsFlag[1] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item2
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (hatsFlag[1] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item2
															.getName()));
									pref.putInteger("hatStyle", 2);
									cashRegister.play();
									pref.flush();
									hatsFlag[1] = true;
								}
							}
						}
					}
				}
			});

			item3.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item3.getY() == shirtTempY) {
						float dx = x - item3.getWidth() * 0.5f;
						if (dx > 0) {
							item3.setX(item3.getX() + dx);
							if (item3.getX() > 0.3984f * screenWidth) {
								item3.setX(shirtShopX);
								if (hatsFlag[2] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item3
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (hatsFlag[2] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item3
															.getName()));
									pref.putInteger("hatStyle", 3);
									cashRegister.play();
									pref.flush();
									hatsFlag[2] = true;
								}
							}
						}
					}
				}
			});

			item4.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item4.getY() == shirtTempY) {
						float dx = x - item4.getWidth() * 0.5f;
						if (dx > 0) {
							item4.setX(item4.getX() + dx);
							if (item4.getX() > 0.3984f * screenWidth) {
								item4.setX(shirtShopX);
								if (hatsFlag[3] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item4
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (hatsFlag[3] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item4
															.getName()));
									pref.putInteger("hatStyle", 4);
									cashRegister.play();
									pref.flush();
									hatsFlag[3] = true;
								}
							}
						}
					}
				}
			});

			item5.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item5.getY() == shirtTempY) {
						float dx = x - item5.getWidth() * 0.5f;
						if (dx > 0) {
							item5.setX(item5.getX() + dx);
							if (item5.getX() > 0.3984f * screenWidth) {
								item5.setX(shirtShopX);
								if (hatsFlag[4] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item5
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (hatsFlag[4] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item5
															.getName()));
									pref.putInteger("hatStyle", 5);
									cashRegister.play();
									pref.flush();
									hatsFlag[4] = true;
								}
							}
						}
					}
				}
			});

			item6.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item6.getY() == shirtTempY) {
						float dx = x - item6.getWidth() * 0.5f;
						if (dx > 0) {
							item6.setX(item6.getX() + dx);
							if (item6.getX() > 0.3984f * screenWidth) {
								item6.setX(shirtShopX);
								if (hatsFlag[5] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item6
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (hatsFlag[5] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item6
															.getName()));
									pref.putInteger("hatStyle", 6);
									cashRegister.play();
									pref.flush();
									hatsFlag[5] = true;
								}
							}
						}
					}
				}
			});

			item7.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item7.getY() == shirtTempY) {
						float dx = x - item7.getWidth() * 0.5f;
						if (dx > 0) {
							item7.setX(item7.getX() + dx);
							if (item7.getX() > 0.3984f * screenWidth) {
								item7.setX(shirtShopX);
								if (hatsFlag[6] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item7
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (hatsFlag[6] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item7
															.getName()));
									pref.putInteger("hatStyle", 7);
									cashRegister.play();
									pref.flush();
									hatsFlag[6] = true;
								}
							}
						}
					}
				}
			});

			item8.addListener(new DragListener() {
				public void touchDragged(InputEvent event, float x, float y,
						int pointer) {
					if (item8.getY() == shirtTempY) {
						float dx = x - item8.getWidth() * 0.5f;
						if (dx > 0) {
							item8.setX(item8.getX() + dx);
							if (item8.getX() > 0.3984f * screenWidth) {
								item8.setX(shirtShopX);
								if (hatsFlag[7] == false
										&& pref.getInteger("Savings_new",
												pref.getInteger("Savings", 0))
												- Integer.parseInt(item8
														.getName()) < 0) {
									popupGroup.addActor(popupImage);
									popupGroup.addActor(finishedMessage1);
									popupGroup.addActor(finishedMessage2);
									popupGroup.addActor(nextButtonImage);
								} else if (hatsFlag[7] == false) {
									pref.putInteger(
											"Savings_new",
											pref.getInteger("Savings_new", 0)
													- Integer.parseInt(item8
															.getName()));
									pref.putInteger("hatStyle", 8);
									cashRegister.play();
									pref.flush();
									hatsFlag[7] = true;
								}
							}
						}
					}
				}
			});

		}

		group.addActor(item1);
		group.addActor(item2);
		group.addActor(item3);
		group.addActor(item4);
		group.addActor(item5);
		group.addActor(item6);
		group.addActor(item7);
		group.addActor(item8);

		spent = 0;

	}

	public void display(Image image) {

		float Xcord, Ycord;
		if (shades) {
			Xcord = shirtTempX - 0.0390625f*screenWidth;
			Ycord = (shirtTempY - 0.048611f*screenHeight);
		} else {
			Xcord = shirtTempX;
			Ycord = shirtTempY;
		}
		if (!setblank) {
			System.out.println("trouble");
			setblank = true;
			setblankI = image;
			image.setX(Xcord);
			image.setY(Ycord);

		} else {
			System.out.println("trouble 2");
			if (setblankI.getName().equalsIgnoreCase("5")) {
				setblankI.setX(shirt7x);
				setblankI.setY(shirt1y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("10")) {
				setblankI.setX(shirt2x);
				setblankI.setY(shirt2y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("15")) {
				setblankI.setX(shirt3x);
				setblankI.setY(shirt3y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("20")) {
				setblankI.setX(shirt4x);
				setblankI.setY(shirt4y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("25")) {
				setblankI.setX(shirt5x);
				setblankI.setY(shirt3y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("30")) {
				setblankI.setX(shirt6x);
				setblankI.setY(shirt6y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("35")) {
				setblankI.setX(shirt7x);
				setblankI.setY(shirt7y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("40")) {
				setblankI.setX(shirt8x);
				setblankI.setY(shirt2y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("6")) {
				setblankI.setX(shirt7x);
				setblankI.setY(shirt1y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("12")) {
				setblankI.setX(shirt2x);
				setblankI.setY(shirt2y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("18")) {
				setblankI.setX(shirt3x);
				setblankI.setY(shirt3y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("24")) {
				setblankI.setX(shirt4x);
				setblankI.setY(shirt4y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("29")) {
				setblankI.setX(shirt5x);
				setblankI.setY(shirt3y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("32")) {
				setblankI.setX(shirt6x);
				setblankI.setY(shirt6y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("36")) {
				setblankI.setX(shirt7x);
				setblankI.setY(shirt7y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("39")) {
				setblankI.setX(shirt8x);
				setblankI.setY(shirt2y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("7")) {
				setblankI.setX(shirt7x - 0.0390625f*screenWidth);
				setblankI.setY(shirt1y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("14")) {
				setblankI.setX(shirt2x - 0.0390625f*screenWidth);
				setblankI.setY(shirt2y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("21")) {
				setblankI.setX(shirt3x - 0.0390625f*screenWidth);
				setblankI.setY(shirt3y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("28")) {
				setblankI.setX(shirt4x - 0.0390625f*screenWidth);
				setblankI.setY(shirt4y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("34")) {
				setblankI.setX(shirt5x - 0.0390625f*screenWidth);
				setblankI.setY(shirt3y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("42")) {
				setblankI.setX(shirt6x - 0.0390625f*screenWidth);
				setblankI.setY(shirt6y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("49")) {
				setblankI.setX(shirt7x - 0.0390625f*screenWidth);
				setblankI.setY(shirt7y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("55")) {
				setblankI.setX(shirt8x - 0.0390625f*screenWidth);
				setblankI.setY(shirt2y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("2")) {
				setblankI.setX(shirt7x - 25);
				setblankI.setY(shirt1y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("3")) {
				setblankI.setX(shirt2x);
				setblankI.setY(shirt2y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("4")) {
				setblankI.setX(shirt3x);
				setblankI.setY(shirt3y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("8")) {
				setblankI.setX(shirt4x);
				setblankI.setY(shirt4y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("9")) {
				setblankI.setX(shirt5x - 0.0390625f*screenWidth);
				setblankI.setY(shirt3y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("11")) {
				setblankI.setX(shirt6x);
				setblankI.setY(shirt6y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("16")) {
				setblankI.setX(shirt7x);
				setblankI.setY(shirt7y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}
			if (setblankI.getName().equalsIgnoreCase("19")) {

				setblankI.setX(shirt8x);
				setblankI.setY(shirt2y);
				image.setX(Xcord);
				image.setY(Ycord);
				setblankI = image;

			}

		}
	}

	@Override
	public void dispose() {
		shoppingTheme.dispose();
		font.dispose();
		batch.dispose();
		stage.dispose();

	}

	@Override
	public void resize(int width, int height) {
	}
}