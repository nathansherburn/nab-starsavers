package com.eduspire.starsavers;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.TimeUtils;
import com.eduspire.starsavers.StarSavers;

public class BankScreen extends AbstractScreen {
	String Prefs = "StarCharacters";
	Preferences pref;

	private Stage stage;
	private Image backgroundImage;
	private SpriteBatch batch;
	int screenWidth, screenHeight;
	int savings;
	float timediff;
	int timeCount;
		
	Image moneyTopImage, moneyMiddleImage, moneyBottomImage, measurementsImage,
			savingsCounterBoxImage, educationCounterBoxImage,
			healthCounterBoxImage, blackImage, adventureImagePressed,
			adventureImage, popupImage, nextButtonImage, plant1, plant2,
			plant3, plant4, plant5, plant6, plant7, book1, book2, book3, book4,
			book5, book6, book7;
	TextField moneyEarned, communityEarned, educationEarned, rewardMessage1,
			rewardMessage2, interestMessage1, interestMessage2;
	boolean[] unlocks = new boolean[14];
	boolean makePopup = false;
	boolean buttonVisible = false;
	boolean updateGraphsIncrease = false;
	boolean updateGraphsDecrease = false;
	boolean makePiggyPopup = false;
	boolean bedUnlocked = false, closetUnlocked = false, carpetUnlocked = false, sideUnlocked = false,
			bbagUnlocked = false, deskUnlocked = false, chairUnlocked = false, lampUnlocked = false,
			footyUnlocked = false, guitarUnlocked = false, plantUnlocked = false, piggyUnlocked = false,
			laptopUnlocked = false, skatebUnlocked = false;

	int moneyDifference, educationDifference, communityDifference;
	Group mainGroup = new Group();
	Group awardGroup = new Group();
	Group treeGroup = new Group();
	Music relaxTheme;

	public BankScreen(StarSavers game) {
		super(game);
	}

	void placeImage(Image imageName, Float scale, Float x, Float y) {
		imageName.setScale(scale);
		imageName.setX(x);
		imageName.setY(y);
	}

	@Override
	public void show() {
		Texture.setEnforcePotImages(false);

		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		batch = new SpriteBatch();
		boolean maintainAspectRatio = true;
		stage = new Stage(screenWidth, screenHeight, maintainAspectRatio);
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);
		pref = Gdx.app.getPreferences(Prefs);

		backgroundImage = new Image(new Texture("data/Backgrounds/bankScreenBackground.png"));
		backgroundImage.setFillParent(true);
		stage.addActor(backgroundImage);

		relaxTheme = Gdx.audio.newMusic(Gdx.files
				.internal("data/Sound/relaxTheme.mp3"));
		relaxTheme.setLooping(true);
		
//		// FOR TESING	
//		makePopup = true;
//		skatebUnlocked = true;
//		pref.putInteger("fromAdventureScreen", 0);
//		
		// FOR TESTING PURPOSES
		// pref.putInteger("Savings", 50);
		// pref.putInteger("Savings_new", 50);
		// pref.flush();
		// pref.putInteger("Education", 50);
		// pref.putInteger("Education_new", 50);
		// pref.flush();
		// pref.putInteger("Community", 50);
		// pref.putInteger("Community_new", 50);
		// pref.flush();

		// System.out.println(pref.getInteger("Savings_new", 0));
		// System.out.println(pref.getInteger("Savings", 0));

		popupImage = new Image(game.generalTextureAtlas.findRegion("popup"));
		popupImage.setX(10f * screenWidth / 100f);
		popupImage.setY(10f * screenHeight / 100f);
		popupImage.setSize(80f * screenWidth / 100f, 80f * screenHeight / 100f);

		BitmapFont arial60 = new BitmapFont(
				Gdx.files.internal("data/Fonts/arial64.fnt"),
				Gdx.files.internal("data/Fonts/arial64.png"), false);
		TextFieldStyle counterStyle = new TextFieldStyle();
		counterStyle.fontColor = Color.BLACK;
		counterStyle.font = arial60;
		counterStyle.font.setScale(screenWidth / 1200f);

		communityEarned = new TextField("" + pref.getInteger("Community", 0),
				counterStyle);
		communityEarned.setWidth(screenWidth);
		communityEarned.setX(11f * screenWidth / 100f);
		communityEarned.setY(86f * screenHeight / 100f);
		communityEarned.setDisabled(true);

		moneyEarned = new TextField("" + pref.getInteger("Savings", 0),
				counterStyle);
		moneyEarned.setWidth(screenWidth);
		moneyEarned.setX(43f * screenWidth / 100f);
		moneyEarned.setY(86f * screenHeight / 100f);
		moneyEarned.setDisabled(true);

		educationEarned = new TextField("" + pref.getInteger("Education", 0),
				counterStyle);
		educationEarned.setWidth(screenWidth);
		educationEarned.setX(76f * screenWidth / 100f);
		educationEarned.setY(86f * screenHeight / 100f);
		educationEarned.setDisabled(true);

		// savings = pref.getInteger("Savings_new", 0);

	
		TextFieldStyle awardMessageStyle = new TextFieldStyle();
		awardMessageStyle.fontColor = Color.BLACK;
		awardMessageStyle.font = game.font;
		awardMessageStyle.font.setScale(screenWidth / 1200f);
		rewardMessage1 = new TextField("You earned a", awardMessageStyle);
		rewardMessage2 = new TextField("reward", awardMessageStyle);
		rewardMessage1.setWidth(screenWidth);
		rewardMessage1.setX(19.5f * screenWidth / 100f);
		rewardMessage1.setY(60f * screenHeight / 100f);
		rewardMessage1.setDisabled(true);
		rewardMessage2.setWidth(screenWidth);
		rewardMessage2.setX(45f * screenWidth / 100f);
		rewardMessage2.setY(45f * screenHeight / 100f);
		rewardMessage2.setDisabled(true);

		interestMessage1 = new TextField("You unlocked", awardMessageStyle);
		interestMessage1.setWidth(screenWidth);
		interestMessage1.setX(19.5f * screenWidth / 100f);
		interestMessage1.setY(50f * screenHeight / 100f);
		interestMessage1.setDisabled(true);
		interestMessage2 = new TextField("the bonus game", awardMessageStyle);
		interestMessage2.setWidth(screenWidth);
		interestMessage2.setX(13f * screenWidth / 100f);
		interestMessage2.setY(35f * screenHeight / 100f);
		interestMessage2.setDisabled(true);

		moneyTopImage = new Image(game.bank0Atlas.findRegion("moneyTop"));
		moneyTopImage.setScale(screenWidth / 2280f);
		moneyTopImage.setX(39.5f * screenWidth / 100f);
		moneyTopImage.setY(pref.getInteger("Savings", 0) * 0.353f + 126);

		moneyMiddleImage = new Image(game.bank0Atlas.findRegion("moneyMiddle"));
		moneyMiddleImage.setScale(screenWidth / 2280f);
		moneyMiddleImage.setHeight(0);
		moneyMiddleImage.setX(39.5f * screenWidth / 100f);
		moneyMiddleImage.setY(19f * screenHeight / 100f);
		moneyMiddleImage.setHeight(0.662f * pref.getInteger("Savings", 0));
		// moneyMiddleImage.setY(19f * screenHeight / 100f);

		moneyBottomImage = new Image(game.bank0Atlas.findRegion("moneyBottom"));
		moneyBottomImage.setScale(screenWidth / 2280f);
		moneyBottomImage.setX(39.5f * screenWidth / 100f);
		moneyBottomImage.setY(14f * screenHeight / 100f);

		measurementsImage = new Image(game.bank0Atlas.findRegion("measurements"));
		measurementsImage.setScale(screenWidth / 2280f);
		measurementsImage.setX(47f * screenWidth / 100f);
		measurementsImage.setY(14f * screenHeight / 100f);

		// RESET BUTTON
		Image resetButton = new Image(game.bank0Atlas.findRegion("reset"));
		resetButton.setScale(screenWidth / 2280f);
		resetButton.setX(1f * screenWidth / 100f);
		resetButton.setY(1f * screenHeight / 100f);
		resetButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				pref.putInteger("Savings", 0);
				pref.putInteger("Savings_new", 0);
				pref.putInteger("Community", 0);
				pref.putInteger("Community_new", 0);
				pref.putInteger("Education", 0);
				pref.putInteger("Education_new", 0);

				pref.putBoolean("bedUnlocked", false);
				pref.putBoolean("closetUnlocked", false);
				pref.putBoolean("carpetUnlocked", false);
				pref.putBoolean("laptopUnlocked", false);
				pref.putBoolean("deskUnlocked", false);
				pref.putBoolean("chairUnlocked", false);
				pref.putBoolean("lampUnlocked", false);
				pref.putBoolean("piggyUnlocked", false);
				pref.putBoolean("plantUnlocked", false);
				pref.putBoolean("guitarUnlocked", false);
				pref.putBoolean("bbagUnlocked", false);
				pref.putBoolean("footyUnlocked", false);
				pref.putBoolean("skatebUnlocked", false);
				pref.putBoolean("sideUnlocked", false);
				pref.putInteger("prevHappy", 50);
				pref.putInteger("prevEdu", 50);

				pref.flush();

				moneyEarned.setText("0");
				communityEarned.setText("0");
				educationEarned.setText("0");
			}
		});

		if (pref.getInteger("Education", 0) > 349) {
			book7 = new Image(game.bank0Atlas.findRegion("book7"));
			book7.setScale(screenWidth / 2280f);
			book7.setX(71f * screenWidth / 100f);
			book7.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book7);
		} else if (pref.getInteger("Education", 0) > 299) {
			book6 = new Image(game.bank0Atlas.findRegion("book6"));
			book6.setScale(screenWidth / 2280f);
			book6.setX(71f * screenWidth / 100f);
			book6.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book6);
		} else if (pref.getInteger("Education", 0) > 249) {
			book5 = new Image(game.bank0Atlas.findRegion("book5"));
			book5.setScale(screenWidth / 2280f);
			book5.setX(71f * screenWidth / 100f);
			book5.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book5);
		} else if (pref.getInteger("Education", 0) > 199) {
			book4 = new Image(game.bank1Atlas.findRegion("book4"));
			book4.setScale(screenWidth / 2280f);
			book4.setX(71f * screenWidth / 100f);
			book4.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book4);
		} else if (pref.getInteger("Education", 0) > 149) {
			book3 = new Image(game.bank1Atlas.findRegion("book3"));
			book3.setScale(screenWidth / 2280f);
			book3.setX(71f * screenWidth / 100f);
			book3.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book3);
		} else if (pref.getInteger("Education", 0) > 99) {
			book2 = new Image(game.bank1Atlas.findRegion("book2"));
			book2.setScale(screenWidth / 2280f);
			book2.setX(71f * screenWidth / 100f);
			book2.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book2);
		} else if (pref.getInteger("Education", 0) > 49) {
			book1 = new Image(game.bank1Atlas.findRegion("book1"));
			book1.setScale(screenWidth / 2280f);
			book1.setX(71f * screenWidth / 100f);
			book1.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book1);
		}

		if (pref.getInteger("Community", 0) > 349) {
			plant7 = new Image(game.bank0Atlas.findRegion("plant7"));
			plant7.setScale(screenWidth / 2280f);
			plant7.setX(7f * screenWidth / 100f);
			plant7.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant7);
		} else if (pref.getInteger("Community", 0) > 299) {
			plant6 = new Image(game.bank0Atlas.findRegion("plant6"));
			plant6.setScale(screenWidth / 2280f);
			plant6.setX(7f * screenWidth / 100f);
			plant6.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant6);
		} else if (pref.getInteger("Community", 0) > 249) {
			plant5 = new Image(game.bank0Atlas.findRegion("plant5"));
			plant5.setScale(screenWidth / 2280f);
			plant5.setX(7f * screenWidth / 100f);
			plant5.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant5);
		} else if (pref.getInteger("Community", 0) > 199) {
			plant4 = new Image(game.bank0Atlas.findRegion("plant4"));
			plant4.setScale(screenWidth / 2280f);
			plant4.setX(7f * screenWidth / 100f);
			plant4.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant4);
		} else if (pref.getInteger("Community", 0) > 149) {
			plant3 = new Image(game.bank1Atlas.findRegion("plant3"));
			plant3.setScale(screenWidth / 2280f);
			plant3.setX(7f * screenWidth / 100f);
			plant3.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant3);
		} else if (pref.getInteger("Community", 0) > 99) {
			plant2 = new Image(game.bank1Atlas.findRegion("plant2"));
			plant2.setScale(screenWidth / 2280f);
			plant2.setX(7f * screenWidth / 100f);
			plant2.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant2);
		} else if (pref.getInteger("Community", 0) > 49) {
			plant1 = new Image(game.bank1Atlas.findRegion("plant1"));
			plant1.setScale(screenWidth / 2280f);
			plant1.setX(7f * screenWidth / 100f);
			plant1.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant1);
		}

		mainGroup.addActor(moneyMiddleImage);
		mainGroup.addActor(moneyBottomImage);
		mainGroup.addActor(moneyTopImage);
		mainGroup.addActor(measurementsImage);
		mainGroup.addActor(moneyEarned);
		mainGroup.addActor(communityEarned);
		mainGroup.addActor(educationEarned);
		mainGroup.addActor(resetButton);

		nextButtonImage = new Image(game.generalTextureAtlas.findRegion("nextButton"));
		nextButtonImage.setScale(screenWidth / 1280f);
		nextButtonImage.setX(74f * screenWidth / 100f);
		nextButtonImage.setY(10f * screenHeight / 100f);
		nextButtonImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				stage.addActor(blackImage);
				blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
					@Override
					public boolean act(float delta) {
						dispose();
						game.setScreen(new RoomScreen(game));
						return false;
					}
				}));
			}
		});

		adventureImage = new Image(game.generalTextureAtlas.findRegion("nextButtonAdventure"));
		adventureImage.setScale(screenWidth / 2010f);
		adventureImage.setX(86.5f * screenWidth / 100f);
		adventureImage.setY(5f * screenHeight / 100f);
		adventureImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				stage.addActor(blackImage);
				blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
					@Override
					public boolean act(float delta) {
						dispose();
						game.setScreen(new AdventureScreen(game));
						return false;
					}
				}));
			}
		});

		if (pref.getInteger("fromAdventureScreen", 1) == 1) {
			mainGroup.addActor(adventureImage);
		}

		// System.out.println(pref.getBoolean("bedUnlocked", false));
		//
		// if(pref.getBoolean("bedUnlocked", false) ==false)
		// {
		// System.out.println("bed unlocked = false");
		// if((pref.getInteger("Community", 0)>50))
		// {
		// System.out.println( "Happiness");
		//
		// }
		// }
		// if(pref.getBoolean("carpetUnlocked", false) ==false)
		// {
		// System.out.println("carpet unlocked = false");
		// if((pref.getInteger("Community", 0)>100))
		// {
		// System.out.println( "Happiness C");
		// pref.putBoolean("carpetUnlocked", true);
		// pref.flush();
		// }
		// }

		stage.addActor(mainGroup);
		stage.addActor(treeGroup);
		stage.addActor(awardGroup);

		blackImage = new Image(game.generalTextureAtlas.findRegion("black"));
		blackImage.setFillParent(true);
		stage.addActor(blackImage);
		blackImage.addAction(sequence(delay(0.1f), fadeOut(0.1f),
				Actions.removeActor()));

		relaxTheme.play();
	}

	@Override
	public void render(float delta) {
		// System.out.println(pref.getInteger("Savings_new", 0));
		// System.out.println(pref.getInteger("Savings", 0));

		// Check differences for life graph values
		moneyDifference = pref.getInteger("Savings_new", 0)
				- pref.getInteger("Savings", 0);
		educationDifference = pref.getInteger("Education_new", 0)
				- pref.getInteger("Education", 0);
		communityDifference = pref.getInteger("Community_new", 0)
				- pref.getInteger("Community", 0);

		System.out.println(educationDifference);

		if (pref.getInteger("fromAdventureScreen", 1) == 0) {

			if (TimeUtils.nanoTime() - timediff > 150000) {
				timediff = TimeUtils.nanoTime();
				timeCount++;
				if (timeCount > 30) {
					if (moneyDifference > 0) {
						System.out.println("increaseMoney");
						increaseMoney();
					} else if (moneyDifference < 0) {
						// System.out.println("decreaseMoney");
						decreaseMoney();
					} else if (communityDifference > 0) {
						// System.out.println("increaseCommunity");
						increaseCommunity();
					} else if (communityDifference < 0) {
						// System.out.println("decreaseCommunity");
						decreaseCommunity();
					} else if (educationDifference > 0) {
						// System.out.println("increaseEducation");
						increaseEducation();
					} else if (educationDifference < 0) {
						// System.out.println("decreaseEducation");
						decreaseEducation();
					} else {
						// System.out.println("Equalize");
						pref.putInteger("Savings",
								pref.getInteger("Savings_new", 0));
						pref.putInteger("Community",
								pref.getInteger("Community_new", 0));
						pref.putInteger("Education",
								pref.getInteger("Education_new", 0));
						pref.flush();
						communityEarned.setText(""
								+ pref.getInteger("Savings", 0));
						communityEarned.setText(""
								+ pref.getInteger("Education", 0));
						communityEarned.setText(""
								+ pref.getInteger("Community", 0));
						checkHappiness();
						checkEducation();

						if (makePiggyPopup == true) {
							awardGroup.addActor(popupImage);
							awardGroup.addActor(interestMessage1);
							awardGroup.addActor(interestMessage2);
							adventureImage.setScale(screenWidth / 1700f);
							adventureImage.setX(75f * screenWidth / 100f);
							adventureImage.setY(5f * screenHeight / 100f);
							makePiggyPopup = false;
							buttonVisible = true;
							pref.putInteger("collectInterest", 1);
							pref.flush();
							awardGroup.addActor(adventureImage);
						}

						if (makePopup == true) {
							awardGroup.addActor(popupImage);
							awardGroup.addActor(rewardMessage1);
							awardGroup.addActor(rewardMessage2);
							awardGroup.addActor(nextButtonImage);

							if (plantUnlocked) {
								Image plant = new Image(game.roomAtlas.findRegion(
										"plant"));
								plant.setX(0.2156f * screenWidth);
								plant.setY(0.15f * screenHeight);
								plant.setScale(0.66f);
								stage.addActor(plant);
							} else

							if (piggyUnlocked) {
								Image piggy = new Image(game.roomAtlas.findRegion(
										"piggy"));
								piggy.setX(0.2156f * screenWidth);
								piggy.setY(0.15f * screenHeight);
								piggy.setScale(0.66f);
								stage.addActor(piggy);
							} else

							if (closetUnlocked) {
								Image closetImage = new Image(game.roomAtlas.findRegion(
										"wardboardc"));
								closetImage.setScale(0.35f);
								closetImage.setX(0.2156f * screenWidth);
								closetImage.setY(0.15f * screenHeight);
								stage.addActor(closetImage);
							} else

							if (bedUnlocked) {
								Image bedImage = new Image(game.roomAtlas.findRegion(
										"bed"));
								bedImage.setScale(0.66f);
								bedImage.setX(0.2156f * screenWidth);
								bedImage.setY(0.15f * screenHeight);
								stage.addActor(bedImage);
							} else

							if (carpetUnlocked) {
								Image carpetImage = new Image(game.roomAtlas.findRegion(
										"carpet"));
								carpetImage.setScale(0.7f);
								carpetImage.setX(0.2156f * screenWidth);
								carpetImage.setY(0.15f * screenHeight);
								stage.addActor(carpetImage);

							} else if (skatebUnlocked) {
								Image skateb = new Image(game.roomAtlas.findRegion(
										"skateb"));
								skateb.setX(0.2156f * screenWidth);
								skateb.setY(0.15f * screenHeight);
								skateb.setScale(0.66f);
								stage.addActor(skateb);
							} else if (guitarUnlocked) {
								Image guitarImage = new Image(game.roomAtlas.findRegion(
										"guitar"));
								guitarImage.setScale(0.66f);
								guitarImage.setX(0.2156f * screenWidth);
								guitarImage.setY(0.15f * screenHeight);
								stage.addActor(guitarImage);
							} else if (footyUnlocked) {
								Image footballImage = new Image(game.roomAtlas.findRegion(
										"football"));
								footballImage.setScale(0.66f);
								footballImage.setX(0.2156f * screenWidth);
								footballImage.setY(0.15f * screenHeight);
								stage.addActor(footballImage);
							} else

							if (bbagUnlocked) {
								Image beanbagImage = new Image(game.roomAtlas.findRegion(
										"beanbag"));
								beanbagImage.setScale(0.66f);
								beanbagImage.setX(0.2156f * screenWidth);
								beanbagImage.setY(0.15f * screenHeight);
								stage.addActor(beanbagImage);
							} else

							if (deskUnlocked) {
								Image deskImage = new Image(game.roomAtlas.findRegion(
										"desk"));
								deskImage.setScale(0.670f);
								deskImage.setX(0.2156f * screenWidth);
								deskImage.setY(0.15f * screenHeight);
								stage.addActor(deskImage);
							} else

							if (chairUnlocked) {
								Image chairImage = new Image(game.roomAtlas.findRegion(
										"chair"));
								chairImage.setScale(0.670f);
								chairImage.setX(0.2156f * screenWidth);
								chairImage.setY(0.15f * screenHeight);
								stage.addActor(chairImage);
							} else

							if (laptopUnlocked) {
								Image laptopImage = new Image(game.roomAtlas.findRegion(
										"laptop1"));
								laptopImage.setScale(0.670f);
								laptopImage.setX(0.2156f * screenWidth);
								laptopImage.setY(0.15f * screenHeight);
								stage.addActor(laptopImage);
							} else

							if (lampUnlocked) {
								Image lampImage = new Image(game.roomAtlas.findRegion(
										"lamp"));
								lampImage.setScale(0.65f);
								lampImage.setX(0.2156f * screenWidth);
								lampImage.setY(0.15f * screenHeight);
								stage.addActor(lampImage);
							} else

							if (sideUnlocked) {
								Image sideImage = new Image(game.roomAtlas.findRegion(
										"side"));
								sideImage.setScale(0.7f);
								sideImage.setX(0.2156f * screenWidth);
								sideImage.setY(0.15f * screenHeight);
								stage.addActor(sideImage);
							}

							makePopup = false;
							buttonVisible = true;
						} else if (buttonVisible == false) {
							mainGroup.addActor(adventureImage);
							buttonVisible = true;
						}

					}

				}
			}
		}

		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void dispose() {
		batch.dispose();
		stage.dispose();
		relaxTheme.dispose();
	}

	public String nextUnlock() {
		System.out.print("entered unlocks");

		if (pref.getBoolean("bedUnlocked", false) == false) {

			pref.putBoolean("bedUnlocked", true);
			bedUnlocked = true;

		} else if (pref.getBoolean("closetUnlocked", false) == false) {

			pref.putBoolean("closetUnlocked", true);
			closetUnlocked = true;

		} else if (pref.getBoolean("carpetUnlocked", false) == false) {

			pref.putBoolean("carpetUnlocked", true);
			carpetUnlocked = true;

		} else if (pref.getBoolean("sideUnlocked", false) == false) {

			pref.putBoolean("sideUnlocked", true);
			sideUnlocked = true;

		} else if (pref.getBoolean("bbagUnlocked", false) == false) {

			pref.putBoolean("bbagUnlocked", true);
			bbagUnlocked = true;

		} else if (pref.getBoolean("piggyUnlocked", false) == false) {

			pref.putBoolean("piggyUnlocked", true);
			piggyUnlocked = true;

		} else if (pref.getBoolean("lampUnlocked", false) == false) {

			pref.putBoolean("lampUnlocked", true);
			lampUnlocked = true;

		} else if (pref.getBoolean("deskUnlocked", false) == false) {

			pref.putBoolean("deskUnlocked", true);
			deskUnlocked = true;

		} else if (pref.getBoolean("chairUnlocked", false) == false) {

			pref.putBoolean("chairUnlocked", true);
			chairUnlocked = true;

		} else if (pref.getBoolean("guitarUnlocked", false) == false) {

			pref.putBoolean("guitarUnlocked", true);
			guitarUnlocked = true;

		} else if (pref.getBoolean("plantUnlocked", false) == false) {

			pref.putBoolean("plantUnlocked", true);
			plantUnlocked = true;

		} else if (pref.getBoolean("footyUnlocked", false) == false) {

			pref.putBoolean("footyUnlocked", true);
			footyUnlocked = true;

		} else if (pref.getBoolean("skatebUnlocked", false) == false) {

			pref.putBoolean("skatebUnlocked", true);
			skatebUnlocked = true;

		} else if (pref.getBoolean("laptopUnlocked", false) == false) {

			pref.putBoolean("laptopUnlocked", true);
			laptopUnlocked = true;
		}

		makePopup = true;

		pref.flush();

		return ""; // should we make this void?

	}

	public void checkEducation() {

		int previousMax = pref.getInteger("prevEdu", 50);

		int education = pref.getInteger("Education", 0);

		if (education > previousMax) {
			nextUnlock();
			pref.putInteger("prevEdu", previousMax + 50);
			pref.flush();
		}
	}

	public void checkHappiness() {

		int previousMax = pref.getInteger("prevHappy", 50);

		int happy = pref.getInteger("Community", 0);

		if (happy > previousMax) {
			nextUnlock();
			pref.putInteger("prevHappy", previousMax + 50);
			pref.flush();
		}
	}

	@Override
	public void resize(int width, int height) {
	}

	public void increaseMoney() {
		if (pref.getInteger("Savings", 0) < 890) {
			moneyMiddleImage.setHeight(moneyMiddleImage.getHeight() + 0.662f);
			moneyTopImage.setY(moneyTopImage.getY() + 0.662f / 1.8f);

		}
		pref.putInteger("Savings", pref.getInteger("Savings", 0) + 1);
		pref.flush();
		moneyEarned.setText("" + pref.getInteger("Savings", 0));

		if (pref.getInteger("Savings", 0) % 100 == 0)
			makePiggyPopup = true;
	}

	public void decreaseMoney() {
		if (pref.getInteger("Savings", 0) > 0) {
			moneyMiddleImage.setHeight(moneyMiddleImage.getHeight() - 0.662f);
			moneyTopImage.setY(moneyTopImage.getY() - 0.662f / 1.8f);

		}
		pref.putInteger("Savings", pref.getInteger("Savings", 0) - 1);
		pref.flush();
		moneyEarned.setText("" + pref.getInteger("Savings", 0));
	}

	public void increaseCommunity() {
		pref.putInteger("Community", pref.getInteger("Community", 0) + 1);
		pref.flush();
		communityEarned.setText("" + pref.getInteger("Community", 0));

		if (pref.getInteger("Community", 0) > 349) {
			plant7 = new Image(game.bank0Atlas.findRegion("plant7"));
			plant7.setScale(screenWidth / 2280f);
			plant7.setX(7f * screenWidth / 100f);
			plant7.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant7);
		} else if (pref.getInteger("Community", 0) > 299) {
			plant6 = new Image(game.bank0Atlas.findRegion("plant6"));
			plant6.setScale(screenWidth / 2280f);
			plant6.setX(7f * screenWidth / 100f);
			plant6.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant6);
		} else if (pref.getInteger("Community", 0) > 249) {
			plant5 = new Image(game.bank0Atlas.findRegion("plant5"));
			plant5.setScale(screenWidth / 2280f);
			plant5.setX(7f * screenWidth / 100f);
			plant5.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant5);
		} else if (pref.getInteger("Community", 0) > 199) {
			plant4 = new Image(game.bank0Atlas.findRegion("plant4"));
			plant4.setScale(screenWidth / 2280f);
			plant4.setX(7f * screenWidth / 100f);
			plant4.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant4);
		} else if (pref.getInteger("Community", 0) > 149) {
			plant3 = new Image(game.bank1Atlas.findRegion("plant3"));
			plant3.setScale(screenWidth / 2280f);
			plant3.setX(7f * screenWidth / 100f);
			plant3.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant3);
		} else if (pref.getInteger("Community", 0) > 99) {
			plant2 = new Image(game.bank1Atlas.findRegion("plant2"));
			plant2.setScale(screenWidth / 2280f);
			plant2.setX(7f * screenWidth / 100f);
			plant2.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant2);
		} else if (pref.getInteger("Community", 0) > 49) {
			plant1 = new Image(game.bank1Atlas.findRegion("plant1"));
			plant1.setScale(screenWidth / 2280f);
			plant1.setX(7f * screenWidth / 100f);
			plant1.setY(11.5f * screenHeight / 100f);
			treeGroup.clear();
			treeGroup.addActor(plant1);
		}

	}

	public void decreaseCommunity() {
		pref.putInteger("Community", pref.getInteger("Community", 0) - 1);
		pref.flush();
		communityEarned.setText("" + pref.getInteger("Community", 0));
	}

	public void increaseEducation() {
		System.out.println("WE entered the function yo");

		pref.putInteger("Education", pref.getInteger("Education", 0) + 1);
		pref.flush();
		educationEarned.setText("" + pref.getInteger("Education", 0));

		if (pref.getInteger("Education", 0) > 349) {
			book7 = new Image(game.bank0Atlas.findRegion("book7"));
			book7.setScale(screenWidth / 2280f);
			book7.setX(71f * screenWidth / 100f);
			book7.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book7);
		} else if (pref.getInteger("Education", 0) > 299) {
			book6 = new Image(game.bank0Atlas.findRegion("book6"));
			book6.setScale(screenWidth / 2280f);
			book6.setX(71f * screenWidth / 100f);
			book6.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book6);
		} else if (pref.getInteger("Education", 0) > 249) {
			book5 = new Image(game.bank0Atlas.findRegion("book5"));
			book5.setScale(screenWidth / 2280f);
			book5.setX(71f * screenWidth / 100f);
			book5.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book5);
		} else if (pref.getInteger("Education", 0) > 199) {
			book4 = new Image(game.bank1Atlas.findRegion("book4"));
			book4.setScale(screenWidth / 2280f);
			book4.setX(71f * screenWidth / 100f);
			book4.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book4);
		} else if (pref.getInteger("Education", 0) > 149) {
			book3 = new Image(game.bank1Atlas.findRegion("book3"));
			book3.setScale(screenWidth / 2280f);
			book3.setX(71f * screenWidth / 100f);
			book3.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book3);
		} else if (pref.getInteger("Education", 0) > 99) {
			book2 = new Image(game.bank1Atlas.findRegion("book2"));
			book2.setScale(screenWidth / 2280f);
			book2.setX(71f * screenWidth / 100f);
			book2.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book2);
		} else if (pref.getInteger("Education", 0) > 49) {
			book1 = new Image(game.bank1Atlas.findRegion("book1"));
			book1.setScale(screenWidth / 2280f);
			book1.setX(71f * screenWidth / 100f);
			book1.setY(10f * screenHeight / 100f);
			mainGroup.addActor(book1);
		}

	}

	public void decreaseEducation() {
		pref.putInteger("Education", pref.getInteger("Education", 0) - 1);
		pref.flush();
		educationEarned.setText("" + pref.getInteger("Education", 0));
	}

}
