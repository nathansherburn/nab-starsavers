package com.eduspire.starsavers;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class AdventureScreen extends AbstractScreen {

	String Prefs = "StarCharacters";
	Preferences pref;

	TextField name, keepSaving;
	// public Texture navigator;
	// public Texture background;
	private Stage stage;
	private Skin skin;
	private Image backgroundImage, blackImage, savingsFaceImage,
	educationFaceImage, sharingFaceImage, tickImage, crossImage;
	private SpriteBatch batch;
	public BitmapFont sketchFont;
	static int location = 1;
	boolean pizzaquest = false, schoolquest = false;

	Image headImage, bodyImage, eyesImage, hairImage, shirtImage, shadeImage,
	pantsImage, lipsImage, hatsImage, savingsFaceImage_large,
	educationFaceImage_large, sharingFaceImage_large,
	groceriesNotification, animationFace, eduQuestNotification,
	pizzaQuestNotification, popupImage, nextButton, collectInterest;
	int iX = 0, iY = 0;
	// public Sprite bgSprite;
	int screenWidth, screenHeight;
	Texture backgroundTexture;
	Label questinfo;
	long Time = 0;
	int statex = 0, statey = 0;

	Group group, buttongroup;
	Group popupGroup = new Group();

	Music mainTheme;

	public AdventureScreen(StarSavers game) {
		super(game);
	}

	@Override
	public void show() {
		Texture.setEnforcePotImages(false);

		group = new Group();
		buttongroup = new Group();
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		batch = new SpriteBatch();
		boolean maintainAspectRatio = false;
		stage = new Stage(screenWidth, screenHeight, maintainAspectRatio);
		Gdx.input.setInputProcessor(stage);

		pref = Gdx.app.getPreferences(Prefs);

		pref.putInteger("Savings", pref.getInteger("Savings_new", 0));
		pref.putInteger("Education", pref.getInteger("Education_new", 0));
		pref.putInteger("Community", pref.getInteger("Community_new", 0));

		mainTheme = Gdx.audio.newMusic(Gdx.files
				.internal("data/Sound/mainTheme.mp3"));
		mainTheme.setLooping(true);

		sketchFont = new BitmapFont(Gdx.files.internal("data/Fonts/sketch120.fnt"),
				Gdx.files.internal("data/Fonts/sketch120.png"), false);
		sketchFont.getRegion().getTexture()
		.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		LabelStyle style2 = new LabelStyle();
		style2.fontColor = Color.BLUE;
		style2.font = sketchFont;
		style2.font.setScale(0.35f);
		questinfo = new Label("", style2);
		// jumble1.setScale(0.25f);
		questinfo.setWidth(30f * screenWidth / 100f);
		questinfo.setX(38f * screenWidth / 100f);
		questinfo.setY(75f * screenHeight / 100f);

		popupImage = new Image(
				game.generalTextureAtlas.findRegion("popup"));

		tickImage = new Image(
				game.generalTextureAtlas.findRegion("tickButton"));
		tickImage.setX(45f * screenWidth / 100f);
		tickImage.setY(48f * screenHeight / 100f);
		tickImage.setScale(0.45f);

		crossImage = new Image(
				game.generalTextureAtlas.findRegion("crossButton"));
		crossImage.setX(57f * screenWidth / 100f);
		crossImage.setY(48f * screenHeight / 100f);
		crossImage.setScale(0.45f);

		tickImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				stage.addActor(blackImage);
				blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
					@Override
					public boolean act(float delta) {
						// the last action will move to the next screen
						if (pizzaquest) {
							mainTheme.stop();
							dispose();
							pref.putInteger("PizzaQuest", 0);
							pref.flush();
							game.setScreen(new PizzaScreen(game, true));
						} else if (schoolquest) {
							mainTheme.stop();
							dispose();
							pref.putInteger("SchoolQuest", 0);
							pref.flush();
							game.setScreen(new SchoolScreen(game, true));
						}
						return false;
					}
				}));
			}
		});

		crossImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				// the last action will move to the next screen
				System.out.println("Hit");
				buttongroup.removeActor(popupImage);
				buttongroup.removeActor(questinfo);
				buttongroup.removeActor(tickImage);
				buttongroup.removeActor(crossImage);

				if (pizzaquest) {
					if (pref.getInteger("Community", 0) - 50 > 0) {
						pref.putInteger("Community_new",
								pref.getInteger("Community", 0) - 50);
						pref.flush();
					}
					pref.putInteger("PizzaQuest", 0);
					pref.flush();
					mainTheme.stop();
					dispose();
					game.setScreen(new BankScreen(game));

				} else if (schoolquest) {
					if ((pref.getInteger("Education", 0) - 50) > 0) {
						pref.putInteger("Education_new",
								pref.getInteger("Education", 0) - 50);
						pref.flush();
					}
					pref.putInteger("SchoolQuest", 0);
					pref.flush();
					mainTheme.stop();
					dispose();
					game.setScreen(new BankScreen(game));
				}
			}
		});
		backgroundImage = new Image(new Texture(
				"data/Backgrounds/AdventureScreenBackground.png"));
		backgroundImage.setWidth(screenWidth);
		backgroundImage.setHeight(screenHeight);
		backgroundImage.setFillParent(true);
		stage.addActor(backgroundImage);

		System.out.println(pref.getInteger("Savings", 0));

		// Make quests

		// //////////////////////////////
		// pref.putInteger("GroceryItem", 1);
		// pref.putInteger("Savings", 180);
		// //////////////////////////////

		if (pref.getInteger("GroceryItem", 0) == 0
				&& pref.getInteger("Savings", 0) > 100) {
			if (Math.random() * 10 < 3) {
				int itemPrice = (int) (100 * Math.random());
				pref.putInteger("GroceryItem", itemPrice);
				pref.flush();
				groceriesNotification = new Image(
						game.generalTextureAtlas.findRegion("groceries"));
				groceriesNotification.setX(91f * screenWidth / 100f);
				groceriesNotification.setY(62f * screenHeight / 100f);
				stage.addActor(groceriesNotification);
			}
		} else if (pref.getInteger("GroceryItem", 0) != 0
				&& pref.getInteger("Savings", 0) > 100) {
			groceriesNotification = new Image(
					game.generalTextureAtlas.findRegion("groceries"));
			groceriesNotification.setX(91f * screenWidth / 100f);
			groceriesNotification.setY(62f * screenHeight / 100f);
			stage.addActor(groceriesNotification);
		}

		if (pref.getInteger("PizzaQuest", 0) == 0
				&& pref.getInteger("Savings", 0) > 70) {
			if (Math.random() * 10 < 3) {
				pref.putInteger("PizzaQuest", 1);
				pref.flush();
				pizzaQuestNotification = new Image(
						game.generalTextureAtlas.findRegion("pizzaQuest"));
				pizzaQuestNotification.setX(0.33f * screenWidth);
				pizzaQuestNotification.setY(0.83f * screenHeight);
				pizzaQuestNotification.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						// the last action will move to the next screen
						pizzaquest = true;
						schoolquest = false;
						questinfo
						.setText("\t\t\tQuest : \n\nEarn 40 dollars before\nthe timer runs out ");
						popupImage.setX(35f * screenWidth / 100f);
						popupImage.setY(45f * screenHeight / 100f);
						popupImage.setSize(45f * screenWidth / 100f,
								45f * screenHeight / 100f);
						buttongroup.addActor(popupImage);
						buttongroup.addActor(questinfo);
						buttongroup.addActor(tickImage);
						buttongroup.addActor(crossImage);
						// game.setScreen(new PizzaScreen(game,true));

					}
				});

				buttongroup.addActor(pizzaQuestNotification);
			}
		} else if (pref.getInteger("PizzaQuest", 0) != 0) {
			pizzaQuestNotification = new Image(
					game.generalTextureAtlas.findRegion("pizzaQuest"));
			pizzaQuestNotification.setX(0.33f * screenWidth);
			pizzaQuestNotification.setY(0.83f * screenHeight);
			pizzaQuestNotification.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					// the last action will move to the next screen
					pizzaquest = true;
					schoolquest = false;
					questinfo
					.setText("\t\t\tQuest : \n\nEarn 40 dollars before\nthe timer runs out ");
					popupImage.setX(35f * screenWidth / 100f);
					popupImage.setY(45f * screenHeight / 100f);
					popupImage.setSize(45f * screenWidth / 100f,
							45f * screenHeight / 100f);
					buttongroup.addActor(popupImage);
					buttongroup.addActor(questinfo);
					buttongroup.addActor(tickImage);
					buttongroup.addActor(crossImage);
					// game.setScreen(new PizzaScreen(game,true));

				}
			});

			buttongroup.addActor(pizzaQuestNotification);
		}

		if (pref.getInteger("SchoolQuest", 0) == 0
				&& pref.getInteger("Savings", 0) > 80) {
			if (Math.random() * 10 < 3) {
				pref.putInteger("SchoolQuest", 1);
				pref.flush();
				eduQuestNotification = new Image(
						game.generalTextureAtlas.findRegion("eduQuest"));
				eduQuestNotification.setX(0.23f * screenWidth);
				eduQuestNotification.setY(0.59f * screenHeight);

				eduQuestNotification.addListener(new ClickListener() {
					public void clicked(InputEvent event, float x, float y) {
						// the last action will move to the next screen
						pizzaquest = false;
						schoolquest = true;
						questinfo
						.setText("\t\t\tQuest : \n\nEarn 60 education points\nbefore the timer\nruns out ");
						popupImage.setX(35f * screenWidth / 100f);
						popupImage.setY(45f * screenHeight / 100f);
						popupImage.setSize(45f * screenWidth / 100f,
								45f * screenHeight / 100f);
						buttongroup.addActor(popupImage);
						buttongroup.addActor(questinfo);
						buttongroup.addActor(tickImage);
						buttongroup.addActor(crossImage);
						// dispose();
						// game.setScreen(new SchoolScreen(game, true));
					}
				});

				buttongroup.addActor(eduQuestNotification);
			}
		} else if (pref.getInteger("SchoolQuest", 0) != 0) {
			eduQuestNotification = new Image(
					game.generalTextureAtlas.findRegion("eduQuest"));
			eduQuestNotification.setX(0.23f * screenWidth);
			eduQuestNotification.setY(0.59f * screenHeight);
			eduQuestNotification.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					// the last action will move to the next screen
					pizzaquest = false;
					schoolquest = true;
					questinfo
					.setText("\t\t\tQuest : \n\nEarn 60 education points\nbefore the timer\nruns out ");
					popupImage.setX(35f * screenWidth / 100f);
					popupImage.setY(45f * screenHeight / 100f);
					popupImage.setSize(45f * screenWidth / 100f,
							45f * screenHeight / 100f);
					buttongroup.addActor(popupImage);
					buttongroup.addActor(questinfo);
					buttongroup.addActor(tickImage);
					buttongroup.addActor(crossImage);
				}
			});
			buttongroup.addActor(eduQuestNotification);
		}

		// Update lifegraph
		if (pref.getInteger("Education", 9) < 50) {
			// small face (lifegraph)
			educationFaceImage = new Image(
					game.generalTextureAtlas.findRegion("sadFace"));
			// large face (on icon)
			educationFaceImage_large = new Image(
					game.generalTextureAtlas.findRegion("sadLarge"));
			educationFaceImage_large.setX(8f * screenWidth / 100f);
			educationFaceImage_large.setY(59f * screenHeight / 100f);
			// educationFaceImage_large.setScale(screenWidth / 1280f);
			stage.addActor(educationFaceImage_large);
		} else {
			// small face (lifegraph)
			educationFaceImage = new Image(
					game.generalTextureAtlas.findRegion("happyFace"));
		}
		educationFaceImage.setX(6.6f * screenWidth / 100f);
		educationFaceImage.setY(29.3f * screenHeight / 100f);
		// educationFaceImage.setScale(screenWidth / (2.5f * 1280f));
		stage.addActor(educationFaceImage);

		if (pref.getInteger("Savings", 0) < 50) {
			// small face (lifegraph)
			savingsFaceImage = new Image(
					game.generalTextureAtlas.findRegion("sadFace")); 
			// large face (on icon)
			savingsFaceImage_large = new Image(
					game.generalTextureAtlas.findRegion("sadLarge"));
			savingsFaceImage_large.setX(15f * screenWidth / 100f);
			savingsFaceImage_large.setY(83f * screenHeight / 100f);
			// savingsFaceImage_large.setScale(screenWidth / 1280f);
			stage.addActor(savingsFaceImage_large);
		} else {
			// small face (lifegraph)
			savingsFaceImage = new Image(
					game.generalTextureAtlas.findRegion("happyFace"));
		}
		savingsFaceImage.setX(6.6f * screenWidth / 100f);
		savingsFaceImage.setY(18.5f * screenHeight / 100f);
		// savingsFaceImage.setScale(screenWidth / (2.5f * 1280f));
		stage.addActor(savingsFaceImage);

		if (pref.getInteger("Community", 0) < 50) {
			// small face (lifegraph)
			sharingFaceImage = new Image(
					game.generalTextureAtlas.findRegion("sadFace"));
			// large face (on icon)
	sharingFaceImage_large = new Image(game.generalTextureAtlas.findRegion("sadLarge"));

			sharingFaceImage_large.setX(65f * screenWidth / 100f);
			sharingFaceImage_large.setY(22f * screenHeight / 100f);
			// sharingFaceImage_large.setScale(screenWidth / 1280f);
			stage.addActor(sharingFaceImage_large);
		} else {
			// small face (lifegraph)
			sharingFaceImage = new Image(
					game.generalTextureAtlas.findRegion("happyFace"));
		}
		sharingFaceImage.setX(6.6f * screenWidth / 100f);
		sharingFaceImage.setY(7f * screenHeight / 100f);
		// sharingFaceImage.setScale(screenWidth / (2.5f * 1280f));
		stage.addActor(sharingFaceImage);

		// for degug

		pref.putInteger("collectInterest", 1);

		if (pref.getInteger("collectInterest", 0) == 1) {
			Image collectInterest = new Image(
					game.generalTextureAtlas.findRegion("collectInterest"));
			collectInterest.setX(0.389f * screenWidth);
			collectInterest.setY(0.29f * screenHeight);
			stage.addActor(collectInterest);

			collectInterest.addListener(new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					stage.addActor(blackImage);
					blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
						@Override
						public boolean act(float delta) {
							location = 7;
							pref.putInteger("collectInterest", 0);
							pref.flush();
							mainTheme.stop();
							dispose();
							game.setScreen(new BonusScreen(game));
							return false;
						}
					}));
				}
			});
		}


		int skinTone = pref.getInteger("skinTone", 1);
		headImage = new Image(game.avatarTextureAtlas0.findRegion("head" + skinTone));
		bodyImage = new Image(game.avatarTextureAtlas0.findRegion("body" + skinTone));
		placeImage(headImage, screenWidth / 1280f, 66f * screenWidth / 100f,
				35f * screenHeight / 100f);
		placeImage(bodyImage, screenWidth / 1280f,
				70.61f * screenWidth / 100f, 12.48f * screenHeight / 100f);
		group.addActor(bodyImage);
		group.addActor(headImage);
		
		int eyeColour = pref.getInteger("eyeColour", 1);
		eyesImage = new Image(game.avatarTextureAtlas0.findRegion("eyes" + eyeColour));
		placeImage(eyesImage, screenWidth / 1280f, 63.7f * screenWidth / 100f,
				52.3f * screenHeight / 100f);
		group.addActor(eyesImage);
		
		int lipsStyle = pref.getInteger("lipsStyle", 1);
		lipsImage = new Image(game.avatarTextureAtlas0.findRegion("lips" + lipsStyle));
		placeImage(lipsImage, screenWidth / 1280f,
				69.61f * screenWidth / 100f, 37.48f * screenHeight / 100f);
		group.addActor(lipsImage);
		
		int hairStyle = pref.getInteger("hairStyle", 1);
		if (hairStyle == 1 || (hairStyle > 9 && hairStyle < 19)) {
			hairImage = new Image(game.avatarTextureAtlas2.findRegion("hair" + hairStyle));
		} else if (hairStyle == 2 || hairStyle == 3 || (hairStyle > 18 && hairStyle < 32)) {
			hairImage = new Image(game.avatarTextureAtlas1.findRegion("hair" + hairStyle));
		} else {
			hairImage = new Image(game.avatarTextureAtlas0.findRegion("hair" + hairStyle));
		}
		placeImage(hairImage, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);
		group.addActor(hairImage);
		
		int pantsStyle = pref.getInteger("pantsStyle", 1);
		pantsImage = new Image(game.shopAtlas.findRegion("pants" + pantsStyle));
		placeImage(pantsImage, screenWidth / 1100f,
				71.61f * screenWidth / 100f, 14.48f * screenHeight / 100f);
		group.addActor(pantsImage);

		int shirtStyle = pref.getInteger("shirtStyle", 1);
		shirtImage = new Image(game.shopAtlas.findRegion("tshirt" + shirtStyle));
		placeImage(shirtImage, screenWidth / 1280f,
				71.91f * screenWidth / 100f, 22.1f * screenHeight / 100f);
		group.addActor(shirtImage);
		
		int shadesStyle = pref.getInteger("shadesStyle", 1);
		shadeImage = new Image(game.shopAtlas.findRegion("shades" + shadesStyle));
		placeImage(shadeImage, screenWidth / 1100f,
				60f * screenWidth / 100f, 45.48f * screenHeight / 100f);
		group.addActor(shadeImage);

		int hatStyle = pref.getInteger("hatStyle", 1);
		hatsImage = new Image(game.shopAtlas.findRegion("cap" + hatStyle));
		placeImage(hatsImage, screenWidth / 1280f,
				65.91f * screenWidth / 100f, 66.1f * screenHeight / 100f);
		group.addActor(hatsImage);
		
		stage.addActor(group);

		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("x " + x + "y " + y + "" + screenHeight);

				if ((x / screenWidth) > 0.219 && (x / screenWidth) < 0.322) {
					if ((y / screenHeight) > 0.117
							&& (y / screenHeight) < 0.342) {
						stage.addActor(blackImage);
						blackImage.addAction(sequence(fadeIn(0.1f),
								new Action() {
							@Override
							public boolean act(float delta) {
								location = 1;
								mainTheme.stop();
								dispose();
								game.setScreen(new RoomScreen(game));

								return false;
							}
						}));
					}
				}

				if ((x / screenWidth) > 0.419 && (x / screenWidth) < 0.572) {
					if ((y / screenHeight) > 0.342
							&& (y / screenHeight) < 0.661) {

						// ////////////////////////////////////////////
						// pref.putInteger("collectInterest", 1);
						// ////////////////////////////////////////////
						if (pref.getInteger("collectInterest", 0) == 1) {
							stage.addActor(blackImage);
							blackImage.addAction(sequence(fadeIn(0.1f),
									new Action() {
								@Override
								public boolean act(float delta) {
									location = 7;
									pref.putInteger("collectInterest",
											0);
									pref.flush();
									mainTheme.stop();
									dispose();
									game.setScreen(new BonusScreen(game));
									return false;
								}
							}));
						} else {
							popupImage.setWidth(80f * screenWidth / 100f);
							popupImage.setHeight(80f * screenHeight / 100f);
							popupImage.setX(10f * screenWidth / 100f);
							popupImage.setY(10f * screenHeight / 100f);
							popupGroup.addActor(popupImage);

							BitmapFont sketch120 = new BitmapFont(Gdx.files
									.internal("data/Fonts/sketch120.fnt"), Gdx.files
									.internal("data/Fonts/sketch120.png"), false);
							TextFieldStyle popupStyle = new TextFieldStyle();
							popupStyle.fontColor = Color.BLACK;
							popupStyle.font = sketch120;
							popupStyle.font.setScale(screenWidth / 1280f);

							keepSaving = new TextField("Keep saving",
									popupStyle);
							keepSaving.setWidth(screenWidth);
							keepSaving.setX(20f * screenWidth / 100f);
							keepSaving.setY(40f * screenHeight / 100f);
							keepSaving.setDisabled(true);
							popupGroup.addActor(keepSaving);

							nextButton = new Image(
									game.generalTextureAtlas.findRegion("nextButton"));
							nextButton.setScale(screenWidth / 1280f);
							nextButton.setX(74f * screenWidth / 100f);
							nextButton.setY(10f * screenHeight / 100f);
							nextButton.addListener(new ClickListener() {
								public void clicked(InputEvent event, float x,
										float y) {
									popupGroup.removeActor(popupImage);
									popupGroup.removeActor(nextButton);
									popupGroup.removeActor(keepSaving);
								}
							});
							popupGroup.addActor(nextButton);
							stage.addActor(popupGroup);
						}
					}
				}

				if ((x / screenWidth) > 0.649 && (x / screenWidth) < 0.831) {
					if ((y / screenHeight) > 0.8 && (y / screenHeight) < 0.956) {
						pref.putInteger("fromAdventureScreen", 1);
						pref.flush();

						stage.addActor(blackImage);
						blackImage.addAction(sequence(fadeIn(0.1f),
								new Action() {
							@Override
							public boolean act(float delta) {
								location = 4;
								mainTheme.stop();
								dispose();
								game.setScreen(new BankScreen(game));

								return false;
							}
						}));
					}
				}

				if ((x / screenWidth) > 0.799 && (x / screenWidth) < 0.965) {
					if ((y / screenHeight) > 0.458
							&& (y / screenHeight) < 0.618) {
						stage.addActor(blackImage);
						blackImage.addAction(sequence(fadeIn(0.1f),
								new Action() {
							@Override
							public boolean act(float delta) {
								location = 5;
								mainTheme.stop();
								dispose();
								game.setScreen(new ShoppingMallScreen(
										game));

								return false;
							}
						}));
						System.out.println("Shop");
					}
				}
				if ((x / screenWidth) > 0.667 && (x / screenWidth) < 0.809) {
					if ((y / screenHeight) > 0.023
							&& (y / screenHeight) < 0.304) {

						stage.addActor(blackImage);
						blackImage.addAction(sequence(fadeIn(0.1f),
								new Action() {
							@Override
							public boolean act(float delta) {
								location = 6;
								mainTheme.stop();
								dispose();
								game.setScreen(new RunGame(game));

								return false;
							}
						}));
						System.out.println("Play Ground");
					}
				}
				if ((x / screenWidth > 0.195 && (x / screenWidth) < 0.35)) {
					if ((y / screenHeight) > 0.805
							&& (y / screenHeight) < 0.958) {

						location = 3;
						// dispose();
						if (pref.getInteger("PizzaQuest", 0) != 0) {

							// thelast action will move to the next screen
							pizzaquest = true;
							schoolquest = false;
							questinfo
							.setText("\t\t\tQuest : \n\nEarn 40 dollars before\nthe timer runs out");
							popupImage.setX(35f * screenWidth / 100f);
							popupImage.setY(45f * screenHeight / 100f);
							popupImage.setSize(45f * screenWidth / 100f,
									45f * screenHeight / 100f);
							buttongroup.addActor(popupImage);
							buttongroup.addActor(questinfo);
							buttongroup.addActor(tickImage);
							buttongroup.addActor(crossImage);
							stage.addActor(buttongroup);
							// game.setScreen(new PizzaScreen(game,true));

						} else {
							stage.addActor(blackImage);
							blackImage.addAction(sequence(fadeIn(0.1f),
									new Action() {
								@Override
								public boolean act(float delta) {
									mainTheme.stop();
									dispose();
									game.setScreen(new PizzaScreen(game));
									return false;
								}
							}));
						}

						System.out.println("Pizza!");
					}
				}
				if ((x / screenWidth > 0.110 && (x / screenWidth) < 0.263)) {
					if ((y / screenHeight) > 0.533
							&& (y / screenHeight) < 0.672) {
						// dispose();
						location = 2;
						if (pref.getInteger("SchoolQuest", 0) != 0) {

							// thelast action will move to the next screen
							pizzaquest = false;
							schoolquest = true;
							questinfo
							.setText("\t\t\tQuest : \n\nEarn 60 education points\nbefore the timer\nruns out ");
							popupImage.setX(35f * screenWidth / 100f);
							popupImage.setY(45f * screenHeight / 100f);
							popupImage.setSize(45f * screenWidth / 100f,
									45f * screenHeight / 100f);
							buttongroup.addActor(popupImage);
							buttongroup.addActor(questinfo);
							buttongroup.addActor(tickImage);
							buttongroup.addActor(crossImage);
							stage.addActor(buttongroup);
							// game.setScreen(new PizzaScreen(game,true));

						} else {
							stage.addActor(blackImage);
							blackImage.addAction(sequence(fadeIn(0.1f),
									new Action() {
								@Override
								public boolean act(float delta) {
									
									mainTheme.stop();
									dispose();
									game.setScreen(new SchoolScreen(
											game));
									return false;
								}
							}));
						}

						System.out.println("School!");
					}
				}

			}
		});

		// stage.addActor(backgroundImage);


		stage.addActor(buttongroup);

		blackImage = new Image(
				game.generalTextureAtlas.findRegion("black"));
		blackImage.setFillParent(true);
		stage.addActor(blackImage);
		blackImage.addAction(sequence(delay(0.75f), fadeOut(0.1f),
				Actions.removeActor()));

		mainTheme.play();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
		batch.begin();

		// ...
		// batch.draw(backgroundTexture, iX, iY);//,2048,512,0,0,0.8f,1);

		/*
		 * if(System.currentTimeMillis()-Time >5) { Time =
		 * System.currentTimeMillis(); statex++;
		 * 
		 * if(statex<200) { statey++; batch.draw(navigator,statex,statey); }
		 * else if(statex<400) { batch.draw(navigator, 200, 200,statex-200 ,
		 * statey-200); statey++; } else { statey++; }
		 */

		// }

		batch.end();
	}

	@Override
	public void resize(int width, int height) {
	}
	
	@Override
	public void dispose() {
		Gdx.input.setInputProcessor(null);
		font.dispose();
		batch.dispose();
		stage.dispose();
		mainTheme.dispose();
		super.dispose();
	}

	void placeImage(Image imageName, Float scale, Float x, Float y) {
		System.gc();
		// 195,66|150,330|280,530|570,550|730,340|600,66|500,310
		if (location == 1) {
			group.setX(0.1523f * screenWidth);
			group.setY(-0.00017f * screenHeight);
			group.setScale(0.3f);
		} else if (location == 2) {
			group.setX(-0.15045f * screenWidth);
			group.setY(0.4383f * screenHeight);
			group.setScale(0.3f);
		} else if (location == 3) {
			group.setX(-0.09675f * screenWidth);
			group.setY(0.736f * screenHeight);
			group.setScale(0.3f);
		} else if (location == 4) {
			group.setX(0.6253f * screenWidth);
			group.setY(0.7239f * screenHeight);
			group.setScale(0.3f);
		} else if (location == 5) {
			group.setX(0.6803f * screenWidth);
			group.setY(0.27222f * screenHeight);
			group.setScale(0.3f);
		} else if (location == 6) {
			group.setX(0.38875f * screenWidth);
			group.setY(.07167f * screenHeight);
			group.setScale(0.3f);
		} else if (location == 7) {
			group.setX(0.3096f * screenWidth);
			group.setY(0.43056f * screenHeight);
			group.setScale(0.3f);
		}

		imageName.setScale(scale);
		imageName.setX(x);
		imageName.setY(y);

	}

}