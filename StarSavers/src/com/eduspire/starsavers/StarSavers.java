package com.eduspire.starsavers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.eduspire.starsavers.SplashScreen;

// The main game class. This class is called when Application events are fired.

public class StarSavers extends Game {

	private static final int VIRTUAL_WIDTH = 1280;
	private static final int VIRTUAL_HEIGHT = 720;
	private static final float ASPECT_RATIO = (float) VIRTUAL_WIDTH
			/ (float) VIRTUAL_HEIGHT;

	OrthographicCamera camera;
	private Rectangle viewport;
	SpriteBatch batch;
	BitmapFont font;
	TextureAtlas generalTextureAtlas, pizzaAtlas, bank0Atlas, bank1Atlas,
			roomAtlas, shopAtlas, avatarTextureAtlas0, avatarTextureAtlas1,
			avatarTextureAtlas2, bonusAtlas, groceryAtlas, runAtlas, schoolAtlas;

	String Prefs = "StarSavers";

	// constant useful for logging
	public static final String LOG = StarSavers.class.getSimpleName();

	// a libgdx helper class that logs the current FPS each second
	private FPSLogger fpsLogger;

	// Screen methods

	public SplashScreen getSplashScreen() {
		return new SplashScreen(this);
	}

	public SetupScreen2 getSetupScreen2() {
		return new SetupScreen2(this);
	}

	public ShopScreen getShopScreen() {
		return new ShopScreen(this);
	}

	public BonusScreen getBonusScreen() {
		return new BonusScreen(this);
	}

	public PizzaScreen getPizzaScreen() {
		return new PizzaScreen(this);
	}

	public BankScreen getBankScreen() {
		return new BankScreen(this);
	}

	public AdventureScreen getAdventureScreen() {
		return new AdventureScreen(this);
	}

	public SchoolScreen getSchoolScreen() {
		return new SchoolScreen(this);
	}

	public RoomScreen getRoomScreen() {
		return new RoomScreen(this);
	}

	protected Preferences getPrefs()

	{
		return Gdx.app.getPreferences(Prefs);
	}

	@Override
	public void create() {
		Texture.setEnforcePotImages(false);
		avatarTextureAtlas0 = new TextureAtlas("data/avatarLabels0.txt");
		avatarTextureAtlas1 = new TextureAtlas("data/avatarLabels1.txt");
		avatarTextureAtlas2 = new TextureAtlas("data/avatarLabels2.txt");
		generalTextureAtlas = new TextureAtlas("data/general.txt");
		pizzaAtlas = new TextureAtlas("data/pizzaLabels.txt");
		bank0Atlas = new TextureAtlas("data/bank0.txt");
		bank1Atlas = new TextureAtlas("data/bank1.txt");
		shopAtlas = new TextureAtlas("data/shop.txt");
		roomAtlas = new TextureAtlas("data/room.txt");
		bonusAtlas = new TextureAtlas("data/piggy.txt");
		groceryAtlas = new TextureAtlas("data/groceries.txt");
		runAtlas = new TextureAtlas("data/run.txt");
		schoolAtlas = new TextureAtlas("data/school.txt");
		
		batch = new SpriteBatch();
		font = new BitmapFont(Gdx.files.internal("data/Fonts/sketch120.fnt"),
				Gdx.files.internal("data/Fonts/sketch120.png"), false);
		font.getRegion().getTexture()
				.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		camera = new OrthographicCamera(VIRTUAL_WIDTH, VIRTUAL_HEIGHT);
		Gdx.app.log(StarSavers.LOG, "Creating game");
		fpsLogger = new FPSLogger();
		setScreen(getRoomScreen());
	}

	@Override
	public void resize(int width, int height) {
		// calculate new viewport
		float aspectRatio = (float) width / (float) height;
		float scale = 1f;
		Vector2 crop = new Vector2(0f, 0f);

		if (aspectRatio > ASPECT_RATIO) {
			scale = (float) height / (float) VIRTUAL_HEIGHT;
			crop.x = (width - VIRTUAL_WIDTH * scale) / 2f;
		} else if (aspectRatio < ASPECT_RATIO) {
			scale = (float) width / (float) VIRTUAL_WIDTH;
			crop.y = (height - VIRTUAL_HEIGHT * scale) / 2f;
		} else {
			scale = (float) width / (float) VIRTUAL_WIDTH;
		}

		float w = (float) VIRTUAL_WIDTH * scale;
		float h = (float) VIRTUAL_HEIGHT * scale;
		viewport = new Rectangle(crop.x, crop.y, w, h);

		super.resize(width, height);
		Gdx.app.log(StarSavers.LOG, "Resizing game to: " + width + " x "
				+ height);
	}

	@Override
	public void render() {
		// update camera
		camera.update();
		camera.apply(Gdx.gl10);

		// set viewport
		Gdx.gl.glViewport((int) viewport.x, (int) viewport.y,
				(int) viewport.width, (int) viewport.height);

		// clear previous frame
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		super.render();

		// output the current FPS
		fpsLogger.log();
	}

	@Override
	public void pause() {
		super.pause();
		Gdx.app.log(StarSavers.LOG, "Pausing game");
	}

	@Override
	public void resume() {
		super.resume();
		Gdx.app.log(StarSavers.LOG, "Resuming game");
	}

	@Override
	public void setScreen(Screen screen) {
		System.gc();
		super.setScreen(screen);
		Gdx.app.log(StarSavers.LOG, "Setting screen: "
				+ screen.getClass().getSimpleName());
	}

	@Override
	public void dispose() {
		super.dispose();
		System.gc();
		Gdx.app.log(StarSavers.LOG, "Disposing game");
	}
}
