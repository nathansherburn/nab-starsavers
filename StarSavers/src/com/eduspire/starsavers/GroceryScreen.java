package com.eduspire.starsavers;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class GroceryScreen extends AbstractScreen {

	public Texture background, avatarTexture, bedTexture;
	private Stage stage;
	private Image backgroundImage, groceryItem, buy, blackImage;
	private SpriteBatch batch;
	int screenWidth, screenHeight;
	String Prefs = "StarCharacters";
	Preferences pref;
	Music shoppingTheme;
	Sound cashRegister;
	TextField cashregister;
	String amount = "";
	
	public GroceryScreen(StarSavers game) {
		super(game);
	}

	void placeImage(Image imageName, Float scale, Float x, Float y) {
		imageName.setScale(scale);
		imageName.setX(x);
		imageName.setY(y);
	}

	@Override
	public void show() {
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		batch = new SpriteBatch();
		boolean maintainAspectRatio = true;
		stage = new Stage(screenWidth, screenHeight, maintainAspectRatio);
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);
		pref = Gdx.app.getPreferences(Prefs);
		
		shoppingTheme = Gdx.audio.newMusic(Gdx.files
				.internal("data/Sound/shoppingTheme.mp3"));
		shoppingTheme.setLooping(true);
		cashRegister = Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/cashRegister.mp3"));

		backgroundImage = new Image(new Texture(
				"data/Backgrounds/groceryScreenBackground.png"));
		backgroundImage.setFillParent(true);
		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("x " + x + " y " + y + " " + screenHeight);
			}
		});
		stage.addActor(backgroundImage);

		
		/// MAKE PAY BUTTON HERE
		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				
				if ((x / screenWidth) > 0.389
						&& (x / screenWidth) < 0.625f) {
					if ((y / screenHeight) > 0.0972f
							&& (y / screenHeight) < 0.250f) {

						double price;

						try {
							price = Double.parseDouble(amount);
						} catch (Exception e) {
							price = 0;
						}
						if (price == pref.getInteger("GroceryItem", 0)) {
							pref.putInteger(
									"Savings_new",
									pref.getInteger("Savings", 0)
											- pref.getInteger("GroceryItem", 0));
							pref.flush();
							System.out.println("PAID "
									+ pref.getInteger("GroceryItem", 0)
									+ " for the groceries");
							System.out.println("Savinsgs new is now "
									+ pref.getInteger("Savings_new", 0));
							pref.putInteger("fromAdventureScreen", 0);
							Image tick = new Image(game.generalTextureAtlas.findRegion("tickButton"));
							tick.setX(0.7f*screenWidth);
							tick.setY(0.50f*screenHeight);
							stage.addActor(tick);
							cashRegister.play();
						} else {
							System.out.println("Invalid amount");
						}
					}
				}
			}
		});

		BitmapFont arial64 = new BitmapFont(
				Gdx.files.internal("data/Fonts/arial64.fnt"),
				Gdx.files.internal("data/Fonts/arial64.png"), false);
		TextFieldStyle groceryPriceStyle = new TextFieldStyle();
		groceryPriceStyle.fontColor = Color.BLACK;
		groceryPriceStyle.font = arial64;
		groceryPriceStyle.font.setScale(screenWidth / 1200f);
		TextField groceryPrice = new TextField("Total: $"
				+ pref.getInteger("GroceryItem", 0), groceryPriceStyle);
		groceryPrice.setWidth(screenWidth / 4);
		groceryPrice.setX(40f * screenWidth / 100f);
		groceryPrice.setY(45f * screenHeight / 100f);
		groceryPrice.setDisabled(true);

		cashregister = new TextField(amount, groceryPriceStyle);
		cashregister.setX(0.7031f * screenWidth);
		cashregister.setY(0.8375f * screenHeight);

		stage.addActor(groceryPrice);
		stage.addActor(cashregister);

		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("x " + x + "y " + y + "" + screenHeight);

				if ((x / screenWidth) > 0.682031f
						&& (x / screenWidth) < 0.763281f) {
					if ((y / screenHeight) > 0.401389f
							&& (y / screenHeight) < 0.498611f) {

						amount = amount + "7";

					}
				}

				if ((x / screenWidth) > 0.775f && (x / screenWidth) < 0.849219f) {
					if ((y / screenHeight) > 0.4041f
							&& (y / screenHeight) < 0.5f) {

						amount = amount + "8";

					}
				}

				if ((x / screenWidth) > 0.866406f
						&& (x / screenWidth) < 0.944531f) {
					if ((y / screenHeight) > 0.418056f
							&& (y / screenHeight) < 0.5125f) {

						amount = amount + "9";

					}
				}

				if ((x / screenWidth) > 0.6875f
						&& (x / screenWidth) < 0.767188f) {
					if ((y / screenHeight) > 0.25833f
							&& (y / screenHeight) < 0.37222f) {

						amount = amount + "4";

					}
				}

				if ((x / screenWidth) > 0.778125f
						&& (x / screenWidth) < 0.857031f) {
					if ((y / screenHeight) > 0.275f
							&& (y / screenHeight) < 0.376389f) {

						amount = amount + "5";

					}
				}
				if ((x / screenWidth) > 0.871875f && (x / screenWidth) < 0.95f) {
					if ((y / screenHeight) > 0.284722f
							&& (y / screenHeight) < 0.390278f) {

						amount = amount + "6";

					}
				}
				if ((x / screenWidth) > 0.69375f
						&& (x / screenWidth) < 0.771875f) {
					if ((y / screenHeight) > 0.140278f
							&& (y / screenHeight) < 0.238889f) {

						amount = amount + "1";

					}
				}

				if ((x / screenWidth) > 0.786719f
						&& (x / screenWidth) < 0.864063f) {
					if ((y / screenHeight) > 0.141667f
							&& (y / screenHeight) < 0.25f) {

						amount = amount + "2";

					}
				}
				if ((x / screenWidth) > 0.875781
						&& (x / screenWidth) < 0.953125f) {
					if ((y / screenHeight) > 0.151389f
							&& (y / screenHeight) < 0.251389f) {

						amount = amount + "3";

					}
				}
				if ((x / screenWidth) > 0.709375f
						&& (x / screenWidth) < 0.796094f) {
					if ((y / screenHeight) > 0.05f
							&& (y / screenHeight) < 0.11111f) {

						amount = amount + "0";

					}
				}
				if ((x / screenWidth) > 0.828906f
						&& (x / screenWidth) < 0.952344f) {
					if ((y / screenHeight) > 0.05694f
							&& (y / screenHeight) < 0.115278f) {

						amount = "";

					}
				}

			}
		});

		double itemNumber = Math.random() * 4;

		if (itemNumber < 1) {
			groceryItem = new Image(game.groceryAtlas.findRegion("item1"));
		} else if (itemNumber < 2) {
			groceryItem = new Image(game.groceryAtlas.findRegion("item2"));
		} else if (itemNumber < 3) {
			groceryItem = new Image(game.groceryAtlas.findRegion("item3"));
		} else {
			groceryItem = new Image(game.groceryAtlas.findRegion("item4"));
		}
		groceryItem.setScale(screenWidth / 2280f);
		groceryItem.setX(41f * screenWidth / 100f);
		groceryItem.setY(54f * screenHeight / 100f);
		stage.addActor(groceryItem);

		Image nextButton = new Image(game.generalTextureAtlas.findRegion("nextButton"));
		nextButton.setScale(screenWidth / 2010f);
		nextButton.setX(3f * screenWidth / 100f);
		nextButton.setY(5f * screenHeight / 100f);
		nextButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				stage.addActor(blackImage);
				blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
					@Override
					public boolean act(float delta) {
						pref.putInteger("GroceryItem", 0);
						pref.flush();
						shoppingTheme.stop();
						dispose();
						game.setScreen(new BankScreen(game));
						return false;
					}
				}));
			}
		});
		stage.addActor(nextButton);

		blackImage = new Image(game.generalTextureAtlas.findRegion("black"));
		blackImage.setFillParent(true);
		stage.addActor(blackImage);
		blackImage.addAction(sequence(delay(0.75f), fadeOut(0.1f),
				Actions.removeActor()));

		shoppingTheme.play();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		cashregister.setText(amount);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void dispose() {
		shoppingTheme.dispose();
	}
}
