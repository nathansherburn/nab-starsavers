package com.eduspire.starsavers;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import com.eduspire.starsavers.StarSavers;

// Shows a splash image and moves on to the next screen.
public class SetupScreen2 extends AbstractScreen {

	private Stage stage;
	int screenWidth = Gdx.graphics.getWidth();
	int screenHeight = Gdx.graphics.getHeight();
	private Skin skin;
	int clickedHead = 0;
	int clickedHair = 0;
	int clickedEyes = 0;
	int clickedLips = 0;

	TextField nameField;

	Group headGroup = new Group();
	Group hairGroup = new Group();
	Group eyesGroup = new Group();
	Group lipsGroup = new Group();

	String Prefs = "StarCharacters";
	Preferences pref;
	Image blackImage;

	public Image eyesImage1, eyesImage2, eyesImage3, eyesImage4, eyesImage5,
			eyesImage6, eyesImage7, eyesImage8;
	public Image hairImage1, hairImage2, hairImage3, hairImage4, hairImage5,
			hairImage6, hairImage7, hairImage8, hairImage9, hairImage10,
			hairImage11, hairImage12, hairImage13, hairImage14, hairImage15,
			hairImage16, hairImage17, hairImage18, hairImage19, hairImage20,
			hairImage21, hairImage22, hairImage23, hairImage24, hairImage25,
			hairImage26, hairImage27, hairImage28, hairImage29, hairImage30,
			hairImage31, hairImage32, hairImage33, hairImage34, hairImage35,
			hairImage36, hairImage37, hairImage38, hairImage39, hairImage40;
	public Image bodyImage1, bodyImage2, bodyImage3, bodyImage4;
	public Image headImage1, headImage2, headImage3, headImage4;
	public Image lipsImage1, lipsImage2, lipsImage3, lipsImage4;

	boolean right;

	public SetupScreen2(StarSavers game) {
		super(game);
	}

	@Override
	public void show() {
		super.show();
		boolean maintainAspectRatio = true;
		pref = Gdx.app.getPreferences(Prefs);

		stage = new Stage(screenWidth, screenHeight, maintainAspectRatio);
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);

		Image backgroundImage = new Image(new Texture(
				"data/Backgrounds/setupScreenBackground.png"));
		backgroundImage.setFillParent(true);
		stage.addActor(backgroundImage);

		pref.putInteger("skinTone", pref.getInteger("skinTone", 1));
		pref.putInteger("lipsStyle", pref.getInteger("lipsStyle", 1));
		pref.putInteger("eyeColour", pref.getInteger("eyeColour", 1));
		pref.putInteger("hairStyle", pref.getInteger("hairStyle", 1));

		// Customizable head
		headImage1 = new Image(game.avatarTextureAtlas0.findRegion("head1"));
		placeImage(headImage1, screenWidth / 1280f, 66f * screenWidth / 100f,
				35f * screenHeight / 100f);

		headImage2 = new Image(game.avatarTextureAtlas0.findRegion("head2"));
		placeImage(headImage2, screenWidth / 1280f, 66f * screenWidth / 100f,
				35f * screenHeight / 100f);

		headImage3 = new Image(game.avatarTextureAtlas0.findRegion("head3"));
		placeImage(headImage3, screenWidth / 1280f, 66f * screenWidth / 100f,
				35f * screenHeight / 100f);

		headImage4 = new Image(game.avatarTextureAtlas0.findRegion("head4"));
		placeImage(headImage4, screenWidth / 1280f, 66f * screenWidth / 100f,
				35f * screenHeight / 100f);

		// Customizable body
		bodyImage1 = new Image(game.avatarTextureAtlas0.findRegion("body1"));
		placeImage(bodyImage1, screenWidth / 1280f,
				70.61f * screenWidth / 100f, 12.48f * screenHeight / 100f);

		bodyImage2 = new Image(game.avatarTextureAtlas0.findRegion("body2"));
		placeImage(bodyImage2, screenWidth / 1280f,
				70.61f * screenWidth / 100f, 12.48f * screenHeight / 100f);

		bodyImage3 = new Image(game.avatarTextureAtlas0.findRegion("body3"));
		placeImage(bodyImage3, screenWidth / 1280f,
				70.61f * screenWidth / 100f, 12.48f * screenHeight / 100f);

		bodyImage4 = new Image(game.avatarTextureAtlas0.findRegion("body4"));
		placeImage(bodyImage4, screenWidth / 1280f,
				70.61f * screenWidth / 100f, 12.48f * screenHeight / 100f);

		// Customizable mouth
		lipsImage1 = new Image(game.avatarTextureAtlas0.findRegion("lips1"));
		placeImage(lipsImage1, screenWidth / 1280f,
				69.61f * screenWidth / 100f, 37.48f * screenHeight / 100f);

		lipsImage2 = new Image(game.avatarTextureAtlas0.findRegion("lips2"));
		placeImage(lipsImage2, screenWidth / 1280f,
				69.61f * screenWidth / 100f, 37.48f * screenHeight / 100f);

		lipsImage3 = new Image(game.avatarTextureAtlas0.findRegion("lips3"));
		placeImage(lipsImage3, screenWidth / 1280f,
				69.61f * screenWidth / 100f, 37.48f * screenHeight / 100f);

		lipsImage4 = new Image(game.avatarTextureAtlas0.findRegion("lips4"));
		placeImage(lipsImage4, screenWidth / 1280f,
				69.61f * screenWidth / 100f, 37.48f * screenHeight / 100f);

		// Customizable eyes
		eyesImage1 = new Image(game.avatarTextureAtlas0.findRegion("eyes1"));
		placeImage(eyesImage1, screenWidth / 1280f, 63.7f * screenWidth / 100f,
				52.3f * screenHeight / 100f);

		eyesImage2 = new Image(game.avatarTextureAtlas0.findRegion("eyes2"));
		placeImage(eyesImage2, screenWidth / 1280f, 63.7f * screenWidth / 100f,
				52.3f * screenHeight / 100f);

		eyesImage3 = new Image(game.avatarTextureAtlas0.findRegion("eyes3"));
		placeImage(eyesImage3, screenWidth / 1280f, 63.7f * screenWidth / 100f,
				52.3f * screenHeight / 100f);

		eyesImage4 = new Image(game.avatarTextureAtlas0.findRegion("eyes4"));
		placeImage(eyesImage4, screenWidth / 1280f, 63.7f * screenWidth / 100f,
				52.3f * screenHeight / 100f);

		eyesImage5 = new Image(game.avatarTextureAtlas0.findRegion("eyes5"));
		placeImage(eyesImage5, screenWidth / 1280f, 63.7f * screenWidth / 100f,
				52.3f * screenHeight / 100f);

		eyesImage6 = new Image(game.avatarTextureAtlas0.findRegion("eyes6"));
		placeImage(eyesImage6, screenWidth / 1280f, 63.7f * screenWidth / 100f,
				52.3f * screenHeight / 100f);

		eyesImage7 = new Image(game.avatarTextureAtlas0.findRegion("eyes7"));
		placeImage(eyesImage7, screenWidth / 1280f, 63.7f * screenWidth / 100f,
				52.3f * screenHeight / 100f);

		eyesImage8 = new Image(game.avatarTextureAtlas0.findRegion("eyes8"));
		placeImage(eyesImage8, screenWidth / 1280f, 63.7f * screenWidth / 100f,
				52.3f * screenHeight / 100f);

		// Customizable hair
		hairImage1 = new Image(game.avatarTextureAtlas2.findRegion("hair1"));
		placeImage(hairImage1, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage2 = new Image(game.avatarTextureAtlas1.findRegion("hair2"));
		placeImage(hairImage2, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage3 = new Image(game.avatarTextureAtlas1.findRegion("hair3"));
		placeImage(hairImage3, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage4 = new Image(game.avatarTextureAtlas0.findRegion("hair4"));
		placeImage(hairImage4, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage5 = new Image(game.avatarTextureAtlas0.findRegion("hair5"));
		placeImage(hairImage5, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage6 = new Image(game.avatarTextureAtlas0.findRegion("hair6"));
		placeImage(hairImage6, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage7 = new Image(game.avatarTextureAtlas0.findRegion("hair7"));
		placeImage(hairImage7, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage8 = new Image(game.avatarTextureAtlas0.findRegion("hair8"));
		placeImage(hairImage8, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage9 = new Image(game.avatarTextureAtlas0.findRegion("hair9"));
		placeImage(hairImage9, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage10 = new Image(game.avatarTextureAtlas2.findRegion("hair10"));
		placeImage(hairImage10, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage11 = new Image(game.avatarTextureAtlas2.findRegion("hair11"));
		placeImage(hairImage11, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage12 = new Image(game.avatarTextureAtlas2.findRegion("hair12"));
		placeImage(hairImage12, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage13 = new Image(game.avatarTextureAtlas2.findRegion("hair13"));
		placeImage(hairImage13, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage14 = new Image(game.avatarTextureAtlas2.findRegion("hair14"));
		placeImage(hairImage14, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage15 = new Image(game.avatarTextureAtlas2.findRegion("hair15"));
		placeImage(hairImage15, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage16 = new Image(game.avatarTextureAtlas2.findRegion("hair16"));
		placeImage(hairImage16, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage17 = new Image(game.avatarTextureAtlas2.findRegion("hair17"));
		placeImage(hairImage17, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage18 = new Image(game.avatarTextureAtlas2.findRegion("hair18"));
		placeImage(hairImage18, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage19 = new Image(game.avatarTextureAtlas1.findRegion("hair19"));
		placeImage(hairImage19, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage20 = new Image(game.avatarTextureAtlas1.findRegion("hair20"));
		placeImage(hairImage20, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage21 = new Image(game.avatarTextureAtlas1.findRegion("hair21"));
		placeImage(hairImage21, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage22 = new Image(game.avatarTextureAtlas1.findRegion("hair22"));
		placeImage(hairImage22, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage23 = new Image(game.avatarTextureAtlas1.findRegion("hair23"));
		placeImage(hairImage23, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage24 = new Image(game.avatarTextureAtlas1.findRegion("hair24"));
		placeImage(hairImage24, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage25 = new Image(game.avatarTextureAtlas1.findRegion("hair25"));
		placeImage(hairImage25, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage26 = new Image(game.avatarTextureAtlas1.findRegion("hair26"));
		placeImage(hairImage26, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage27 = new Image(game.avatarTextureAtlas1.findRegion("hair27"));
		placeImage(hairImage27, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage28 = new Image(game.avatarTextureAtlas1.findRegion("hair28"));
		placeImage(hairImage28, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage29 = new Image(game.avatarTextureAtlas1.findRegion("hair29"));
		placeImage(hairImage29, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage30 = new Image(game.avatarTextureAtlas1.findRegion("hair30"));
		placeImage(hairImage30, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage31 = new Image(game.avatarTextureAtlas1.findRegion("hair31"));
		placeImage(hairImage31, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage32 = new Image(game.avatarTextureAtlas0.findRegion("hair32"));
		placeImage(hairImage32, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage33 = new Image(game.avatarTextureAtlas0.findRegion("hair33"));
		placeImage(hairImage33, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage34 = new Image(game.avatarTextureAtlas0.findRegion("hair34"));
		placeImage(hairImage34, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage35 = new Image(game.avatarTextureAtlas0.findRegion("hair35"));
		placeImage(hairImage35, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage36 = new Image(game.avatarTextureAtlas0.findRegion("hair36"));
		placeImage(hairImage36, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage37 = new Image(game.avatarTextureAtlas0.findRegion("hair37"));
		placeImage(hairImage37, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage38 = new Image(game.avatarTextureAtlas0.findRegion("hair38"));
		placeImage(hairImage38, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage39 = new Image(game.avatarTextureAtlas0.findRegion("hair39"));
		placeImage(hairImage39, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		hairImage40 = new Image(game.avatarTextureAtlas0.findRegion("hair40"));
		placeImage(hairImage40, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);

		// //////////////////////////////
		int skinTone = pref.getInteger("skinTone", 1);
		if (skinTone == 1) {
			headGroup.addActor(bodyImage1);
			headGroup.addActor(headImage1);
		} else if (skinTone == 2) {
			headGroup.addActor(bodyImage2);
			headGroup.addActor(headImage2);
			pref.putInteger("skinTone", 1);
		} else if (skinTone == 3) {
			headGroup.addActor(bodyImage3);
			headGroup.addActor(headImage3);
			pref.putInteger("skinTone", 1);
		} else {
			headGroup.addActor(bodyImage4);
			headGroup.addActor(headImage4);
			pref.putInteger("skinTone", 1);
		}

		int eyeColour = pref.getInteger("eyeColour", 1);
		if (eyeColour == 1) {
			eyesGroup.addActor(eyesImage1);
		} else if (eyeColour == 2) {
			eyesGroup.addActor(eyesImage2);
		} else if (eyeColour == 3) {
			eyesGroup.addActor(eyesImage3);
		} else if (eyeColour == 4) {
			eyesGroup.addActor(eyesImage4);
		} else if (eyeColour == 5) {
			eyesGroup.addActor(eyesImage5);
		} else if (eyeColour == 6) {
			eyesGroup.addActor(eyesImage6);
		} else if (eyeColour == 7) {
			eyesGroup.addActor(eyesImage7);
		} else {
			eyesGroup.addActor(eyesImage8);
		}

		int lipsStyle = pref.getInteger("lipsStyle", 1);
		if (lipsStyle == 1) {
			lipsGroup.addActor(lipsImage1);
		} else if (lipsStyle == 2) {
			lipsGroup.addActor(lipsImage2);
		} else if (lipsStyle == 3) {
			lipsGroup.addActor(lipsImage3);
		} else {
			lipsGroup.addActor(lipsImage4);
		}

		int hairStyle = pref.getInteger("hairStyle", 1);
		if (hairStyle == 1) {
			hairGroup.addActor(hairImage1);
		} else if (hairStyle == 2) {
			hairGroup.addActor(hairImage2);
		} else if (hairStyle == 3) {
			hairGroup.addActor(hairImage3);
		} else if (hairStyle == 4) {
			hairGroup.addActor(hairImage4);
		} else if (hairStyle == 5) {
			hairGroup.addActor(hairImage5);
		} else if (hairStyle == 6) {
			hairGroup.addActor(hairImage6);
		} else if (hairStyle == 7) {
			hairGroup.addActor(hairImage7);
		} else if (hairStyle == 8) {
			hairGroup.addActor(hairImage8);
		} else if (hairStyle == 9) {
			hairGroup.addActor(hairImage9);
		} else if (hairStyle == 10) {
			hairGroup.addActor(hairImage10);
		} else if (hairStyle == 11) {
			hairGroup.addActor(hairImage11);
		} else if (hairStyle == 12) {
			hairGroup.addActor(hairImage12);
		} else if (hairStyle == 13) {
			hairGroup.addActor(hairImage13);
		} else if (hairStyle == 14) {
			hairGroup.addActor(hairImage14);
		} else if (hairStyle == 15) {
			hairGroup.addActor(hairImage15);
		} else if (hairStyle == 16) {
			hairGroup.addActor(hairImage16);
		} else if (hairStyle == 17) {
			hairGroup.addActor(hairImage17);
		} else if (hairStyle == 18) {
			hairGroup.addActor(hairImage18);
		} else if (hairStyle == 19) {
			hairGroup.addActor(hairImage19);
		} else if (hairStyle == 20) {
			hairGroup.addActor(hairImage20);
		} else if (hairStyle == 21) {
			hairGroup.addActor(hairImage21);
		} else if (hairStyle == 22) {
			hairGroup.addActor(hairImage22);
		} else if (hairStyle == 23) {
			hairGroup.addActor(hairImage23);
		} else if (hairStyle == 24) {
			hairGroup.addActor(hairImage24);
		} else if (hairStyle == 25) {
			hairGroup.addActor(hairImage25);
		} else if (hairStyle == 26) {
			hairGroup.addActor(hairImage26);
		} else if (hairStyle == 27) {
			hairGroup.addActor(hairImage27);
		} else if (hairStyle == 28) {
			hairGroup.addActor(hairImage28);
		} else if (hairStyle == 29) {
			hairGroup.addActor(hairImage29);
		} else if (hairStyle == 30) {
			hairGroup.addActor(hairImage30);
		} else if (hairStyle == 31) {
			hairGroup.addActor(hairImage31);
		} else if (hairStyle == 32) {
			hairGroup.addActor(hairImage32);
		} else if (hairStyle == 33) {
			hairGroup.addActor(hairImage33);
		} else if (hairStyle == 34) {
			hairGroup.addActor(hairImage34);
		} else if (hairStyle == 35) {
			hairGroup.addActor(hairImage35);
		} else if (hairStyle == 36) {
			hairGroup.addActor(hairImage36);
		} else if (hairStyle == 37) {
			hairGroup.addActor(hairImage37);
		} else if (hairStyle == 38) {
			hairGroup.addActor(hairImage38);
		} else if (hairStyle == 39) {
			hairGroup.addActor(hairImage39);
		} else {
			hairGroup.addActor(hairImage40);
		}

		stage.addActor(headGroup);
		stage.addActor(lipsGroup);
		stage.addActor(eyesGroup);
		stage.addActor(hairGroup);
		// //////////////////////////////////////

		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("x " + x + " y " + y + " " + screenHeight);
			}
		});

		// /////// LEFT BUTTONS
		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				if (x / screenWidth > 0.055 && x / screenWidth < 0.127) {
					if (y / screenHeight > 0.645 && y / screenHeight < 0.793) {
						right = false;
						getEyesImage();
					}
				}
			}
		});
		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				if (x / screenWidth > 0.055 && x / screenWidth < 0.127) {
					if (y / screenHeight > 0.494 && y / screenHeight < 0.631) {
						right = false;
						getLipsImage();
					}
				}
			}
		});
		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				if (x / screenWidth > 0.055 && x / screenWidth < 0.127) {
					if (y / screenHeight > 0.332 && y / screenHeight < 0.469) {
						right = false;
						getHairImage();
					}
				}
			}
		});
		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				if (x / screenWidth > 0.055 && x / screenWidth < 0.127) {
					if (y / screenHeight > 0.169 && y / screenHeight < 0.305) {
						right = false;
						getHeadImage();
					}
				}
			}
		});
		// //////////////// RIGHT BUTTONS
		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				if (x / screenWidth > 0.384 && x / screenWidth < 0.454) {
					if (y / screenHeight > 0.645 && y / screenHeight < 0.793) {
						right = true;
						getEyesImage();
					}
				}
			}
		});
		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				if (x / screenWidth > 0.384 && x / screenWidth < 0.454) {
					if (y / screenHeight > 0.494 && y / screenHeight < 0.631) {
						right = true;
						getLipsImage();
					}
				}
			}
		});
		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				if (x / screenWidth > 0.384 && x / screenWidth < 0.454) {
					if (y / screenHeight > 0.332 && y / screenHeight < 0.469) {
						right = true;
						getHairImage();
					}
				}
			}
		});
		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				if (x / screenWidth > 0.384 && x / screenWidth < 0.454) {
					if (y / screenHeight > 0.169 && y / screenHeight < 0.305) {
						right = true;
						getHeadImage();
					}
				}
			}
		});

		// Next arrow
		Image nextButton = new Image(
				game.generalTextureAtlas.findRegion("nextButton"));
		nextButton.setScale(screenWidth / 2010f);
		nextButton.setX(86.5f * screenWidth / 100f);
		nextButton.setY(5f * screenHeight / 100f);
		nextButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				stage.addActor(blackImage);
				blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
					@Override
					public boolean act(float delta) {
						pref.putBoolean("data/setupdone", true);
						pref.flush();
						dispose();
						game.setScreen(new RoomScreen(game));
						return false;
					}
				}));
			}
		});
		stage.addActor(nextButton);

		// Fade in code (should always be the final actor)
		blackImage = new Image(game.generalTextureAtlas.findRegion("black"));
		blackImage.setFillParent(true);
		stage.addActor(blackImage);
		blackImage.addAction(sequence(delay(0.1f), fadeOut(0.1f),
				Actions.removeActor()));

		// Display current time (does not update)
		// TextField time = new TextField(
		// Integer.toString(Calendar.getInstance().get(
		// Calendar.HOUR_OF_DAY))
		// + ":"
		// + Integer.toString(Calendar.getInstance().get(
		// Calendar.MINUTE)), skin);
		//
		// time.setDisabled(true);
		// stage.addActor(time);
	}

	void placeImage(Image imageName, Float scale, Float x, Float y) {
		imageName.setScale(scale);
		imageName.setX(x);
		imageName.setY(y);
	}

	public void getHeadImage() {

		if (right == true) {
			clickedHead++;
			if (clickedHead == 4)
				clickedHead = 0;
		} else {
			clickedHead--;
			if (clickedHead == -1)
				clickedHead = 3;
		}

		if (clickedHead == 0) {
			headGroup.clear();
			headGroup.addActor(bodyImage1);
			headGroup.addActor(headImage1);
		} else if (clickedHead == 1) {
			headGroup.clear();
			headGroup.addActor(bodyImage2);
			headGroup.addActor(headImage2);
		} else if (clickedHead == 2) {
			headGroup.clear();
			headGroup.addActor(bodyImage3);
			headGroup.addActor(headImage3);
		} else {
			headGroup.clear();
			headGroup.addActor(bodyImage4);
			headGroup.addActor(headImage4);
		}
		
		pref.putInteger("skinTone", clickedHead + 1);
		pref.flush();
	}

	public void getLipsImage() {
		if (right == true) {
			clickedLips++;
			if (clickedLips == 4)
				clickedLips = 0;
		} else {
			clickedLips--;
			if (clickedLips == -1)
				clickedLips = 3;
		}
		
		lipsGroup.clear();

		if (clickedLips == 0) {
			lipsGroup.addActor(lipsImage1);
		} else if (clickedLips == 1) {
			lipsGroup.addActor(lipsImage2);
		} else if (clickedLips == 2) {
			lipsGroup.addActor(lipsImage3);
		} else {
			lipsGroup.addActor(lipsImage4);
		}
		pref.putInteger("lipsStyle", clickedLips+1);
		pref.flush();
	}

	public void getEyesImage() {

		if (right == true) {
			clickedEyes++;
			if (clickedEyes == 8)
				clickedEyes = 0;
		} else {
			clickedEyes--;
			if (clickedEyes == -1)
				clickedEyes = 7;
		}

		if (clickedEyes == 0) {
			eyesGroup.clear();
			eyesGroup.addActor(eyesImage1);
		} else if (clickedEyes == 1) {
			eyesGroup.clear();
			eyesGroup.addActor(eyesImage2);
		} else if (clickedEyes == 2) {
			eyesGroup.clear();
			eyesGroup.addActor(eyesImage3);
		} else if (clickedEyes == 3) {
			eyesGroup.clear();
			eyesGroup.addActor(eyesImage4);
		} else if (clickedEyes == 4) {
			eyesGroup.clear();
			eyesGroup.addActor(eyesImage5);
		} else if (clickedEyes == 5) {
			eyesGroup.clear();
			eyesGroup.addActor(eyesImage6);
		} else if (clickedEyes == 6) {
			eyesGroup.clear();
			eyesGroup.addActor(eyesImage7);
		} else {
			eyesGroup.clear();
			eyesGroup.addActor(eyesImage8);
		}
		
		pref.putInteger("eyeColour", clickedEyes + 1);
		pref.flush();

	}

	public void getHairImage() {

		if (right == true) {
			clickedHair++;
			if (clickedHair == 40)
				clickedHair = 0;
		} else {
			clickedHair--;
			if (clickedHair == -1)
				clickedHair = 39;
		}

		hairGroup.clear();

		if (clickedHair == 0) {
			hairGroup.addActor(hairImage1);
		} else if (clickedHair == 1) {
			hairGroup.addActor(hairImage2);
		} else if (clickedHair == 2) {
			hairGroup.addActor(hairImage3);
		} else if (clickedHair == 3) {
			hairGroup.addActor(hairImage4);
		} else if (clickedHair == 4) {
			hairGroup.addActor(hairImage5);
		} else if (clickedHair == 5) {
			hairGroup.addActor(hairImage6);
		} else if (clickedHair == 6) {
			hairGroup.addActor(hairImage7);
		} else if (clickedHair == 7) {
			hairGroup.addActor(hairImage8);
		} else if (clickedHair == 8) {
			hairGroup.addActor(hairImage9);
		} else if (clickedHair == 9) {
			hairGroup.addActor(hairImage10);
		} else if (clickedHair == 10) {
			hairGroup.addActor(hairImage11);
		} else if (clickedHair == 11) {
			hairGroup.addActor(hairImage12);
		} else if (clickedHair == 12) {
			hairGroup.addActor(hairImage13);
		} else if (clickedHair == 13) {
			hairGroup.addActor(hairImage14);
		} else if (clickedHair == 14) {
			hairGroup.addActor(hairImage15);
		} else if (clickedHair == 15) {
			hairGroup.addActor(hairImage16);
		} else if (clickedHair == 16) {
			hairGroup.addActor(hairImage17);
		} else if (clickedHair == 17) {
			hairGroup.addActor(hairImage18);
		} else if (clickedHair == 18) {
			hairGroup.addActor(hairImage19);
		} else if (clickedHair == 19) {
			hairGroup.addActor(hairImage20);
		} else if (clickedHair == 20) {
			hairGroup.addActor(hairImage21);
		} else if (clickedHair == 21) {
			hairGroup.addActor(hairImage22);
		} else if (clickedHair == 22) {
			hairGroup.addActor(hairImage23);
		} else if (clickedHair == 23) {
			hairGroup.addActor(hairImage24);
		} else if (clickedHair == 24) {
			hairGroup.addActor(hairImage25);
		} else if (clickedHair == 25) {
			hairGroup.addActor(hairImage26);
		} else if (clickedHair == 26) {
			hairGroup.addActor(hairImage27);
		} else if (clickedHair == 27) {
			hairGroup.addActor(hairImage28);
		} else if (clickedHair == 28) {
			hairGroup.addActor(hairImage29);
		} else if (clickedHair == 29) {
			hairGroup.addActor(hairImage30);
		} else if (clickedHair == 30) {
			hairGroup.addActor(hairImage31);
		} else if (clickedHair == 31) {
			hairGroup.addActor(hairImage32);
		} else if (clickedHair == 32) {
			hairGroup.addActor(hairImage33);
		} else if (clickedHair == 33) {
			hairGroup.addActor(hairImage34);
		} else if (clickedHair == 34) {
			hairGroup.addActor(hairImage35);
		} else if (clickedHair == 35) {
			hairGroup.addActor(hairImage36);
		} else if (clickedHair == 36) {
			hairGroup.addActor(hairImage37);
		} else if (clickedHair == 37) {
			hairGroup.addActor(hairImage38);
		} else if (clickedHair == 38) {
			hairGroup.addActor(hairImage39);
		} else {
			hairGroup.addActor(hairImage40);
		}
		pref.putInteger("hairStyle", clickedHair + 1);
		pref.flush();

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void dispose() {
		super.dispose();
	}
}