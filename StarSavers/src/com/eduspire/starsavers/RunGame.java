package com.eduspire.starsavers;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.utils.TimeUtils;

public class RunGame extends AbstractScreen {

	Stage stage;

	String Prefs = "StarCharacters";
	Preferences pref;
	boolean animals = false, plants = false, Auskick = false;
	Skin skin;

	Image backgroundImage, blackImage, counterCommunity, imageredbar,
			imagegraybar, popupImage, nextButtonImage, warningImage, runner,
			escapeImage, runInst1, skip, causeImage;

	int screenWidth, screenHeight;
	int[] checkpoint = new int[4];
	int checkpointNumber;
	int arrayIndex = 0;
	int laps = 0;
	int timeCount = 0;
	int count = 0;
	int finishedOn = 0;
	int returned;
	long timex;
	long timediff = 0;
	boolean stop = false;
	TextField lapCount;
	Group warningGroup = new Group();
	Group introGroup = new Group();
	Music gameTheme;
	Sound whistle;
	boolean causeChosen = false;
	
	public RunGame(StarSavers game) {
		super(game);
	}

	void hotSpot(float x, float y, float x1, float x2, float y1, float y2,
			int arrayIndex, int checkpointNumber) {
		if (x > x1) {
			if (x < x2) {
				if (y > y1) {
					if (y < y2) {
						checkpoint[arrayIndex] = checkpointNumber;
						returned = checkpointNumber;

						if (checkpoint[0] == 1 && checkpoint[1] == 2
								&& checkpoint[2] == 3 && checkpoint[3] == 4) { // +Arrays.equals(ary,
																				// ary1));
							if (returned == 1) {
								laps++;
								finishedOn = checkpointNumber;
								for (int i = 0; i < 4; i++)
									checkpoint[i] = 0;
							}
						}

						if (checkpointNumber == finishedOn)
							checkpoint[arrayIndex] = 0;

						if ((checkpoint[0] == 1 && checkpoint[1] == 0
								&& checkpoint[2] == 3 && checkpoint[3] == 0)
								|| (checkpoint[0] == 0 && checkpoint[1] == 2
										&& checkpoint[2] == 0 && checkpoint[3] == 4)) { // +Arrays.equals(ary,
							// ary1));
							for (int i = 0; i < 4; i++)
								checkpoint[i] = 0;
						}

						// print for bebugging
						// ///////////////////////////////////////////////
						System.out.println("checkpoint:");
						for (int i = 0; i < 4; i++)
							System.out.println(checkpoint[i]);
						// print for bebugging
						// ///////////////////////////////////////////////

					}
				}
			}
		}
	}

	@Override
	public void show() {
		Texture.setEnforcePotImages(false);

		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		boolean maintainAspectRatio = true;
		stage = new Stage(screenWidth, screenHeight, maintainAspectRatio);
		Gdx.input.setInputProcessor(stage);
	
		pref = Gdx.app.getPreferences(Prefs);

		gameTheme = Gdx.audio.newMusic(Gdx.files
				.internal("data/Sound/gameTheme.mp3"));
		gameTheme.setLooping(true);

		whistle = Gdx.audio.newSound(Gdx.files
				.internal("data/Sound/whistle.mp3"));

		backgroundImage = new Image(new Texture("data/Backgrounds/RunGameBackground.png"));
		backgroundImage.setFillParent(true);
		stage.addActor(backgroundImage);

		counterCommunity = new Image(game.generalTextureAtlas.findRegion("counterCommunity"));
		counterCommunity.setScale(screenWidth / 1400f);
		counterCommunity.setX(2f * screenWidth / 100f);
		counterCommunity.setY(77f * screenHeight / 100f);
		stage.addActor(counterCommunity);

		warningImage = new Image(game.runAtlas.findRegion("noShortcuts"));
		warningImage.setScale(screenWidth / 2000f);
		warningImage.setX(30f * screenWidth / 100f);
		warningImage.setY(40f * screenHeight / 100f);

		BitmapFont arial64 = new BitmapFont(
				Gdx.files.internal("data/Fonts/arial64.fnt"),
				Gdx.files.internal("data/Fonts/arial64.png"), false);

		TextFieldStyle lapCountStyle = new TextFieldStyle();
		lapCountStyle.fontColor = Color.BLACK;
		lapCountStyle.font = arial64;
		lapCountStyle.font.setScale(screenWidth / 1280f);
		lapCount = new TextField("0" + laps, lapCountStyle);
		lapCount.setWidth(10f * screenWidth / 100f);
		lapCount.setX(14.5f * screenWidth / 100f);
		lapCount.setY(79f * screenHeight / 100f);
		lapCount.setDisabled(true);
		stage.addActor(lapCount);

		imageredbar = new Image(game.generalTextureAtlas.findRegion("bar"));
		imagegraybar = new Image(game.generalTextureAtlas.findRegion("grayBar"));
		imageredbar.setWidth(0);
		imagegraybar.setWidth(screenWidth);
		imageredbar.setX(0);
		// imageredbar.setScaleY(screenWidth / 69000f);
		imagegraybar.setX(0);
		// imagegraybar.setScaleY(screenWidth / 69000f);
		imageredbar.setY(screenHeight - screenHeight / 21f);
		imagegraybar.setY(screenHeight - screenHeight / 21f);
		stage.addActor(imagegraybar);
		stage.addActor(imageredbar);

		stage.addActor(warningGroup);
		System.out.println("Done 0");
		popupImage = new Image(game.generalTextureAtlas.findRegion("popup"));
		popupImage.setX(10f * screenWidth / 100f);
		popupImage.setY(10f * screenHeight / 100f);
		popupImage.setSize(80f * screenWidth / 100f, 80f * screenHeight / 100f);

		escapeImage = new Image(game.generalTextureAtlas.findRegion("HomeButton"));
		escapeImage.setX(screenWidth - screenWidth / 10f);
		escapeImage.setY(60f * screenHeight / 100f);
		escapeImage.setScale(0.70f);
		escapeImage.addListener(new ClickListener() {

			public void clicked(InputEvent event, float x, float y) {
				// the last action will move to the next screen
				gameTheme.stop();
				dispose();
				game.setScreen(new BankScreen(game));
			}
		});
		stage.addActor(escapeImage);
		System.out.println("Done 1");
		nextButtonImage = new Image(game.generalTextureAtlas.findRegion("nextButton"));
		nextButtonImage.setScale(screenWidth / 1280f);
		nextButtonImage.setX(74f * screenWidth / 100f);
		nextButtonImage.setY(10f * screenHeight / 100f);
		nextButtonImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				stage.addActor(blackImage);
				blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
					@Override
					public boolean act(float delta) {
						gameTheme.stop();
						dispose();
						game.setScreen(new BankScreen(game));
						return false;
					}
				}));
			}
		});

		runner = new Image(game.runAtlas.findRegion("runningCharacter"));
		runner.setScale(screenWidth / 1280f);
		runner.setX(20f * screenWidth / 100f);
		runner.setY(70f * screenHeight / 100f);

		stage.addActor(runner);
		runner.addListener(new DragListener() {
			public void touchDragged(InputEvent event, float x, float y,
					int pointer) {
				float dx = x - runner.getWidth() * 0.5f;
				float dy = y - runner.getHeight() * 0.5f;
				runner.setPosition(runner.getX() + dx, runner.getY() + dy);

				hotSpot(runner.getX() + dx, runner.getY() + dy, 0,
						25f * screenWidth / 100f, 0, screenHeight, 0, 1);
				hotSpot(runner.getX() + dx, runner.getY() + dy, 0, screenWidth,
						0, 35f * screenHeight / 100f, 1, 2);
				hotSpot(runner.getX() + dx, runner.getY() + dy,
						65f * screenWidth / 100f, screenWidth, 0, screenHeight,
						2, 3);
				hotSpot(runner.getX() + dx, runner.getY() + dy, 0, screenWidth,
						65f * screenHeight / 100f, screenHeight, 3, 4);

				if (runner.getX() + dx > 25f * screenWidth / 100f) {
					if (runner.getX() + dx < 65f * screenWidth / 100f) {
						if (runner.getY() + dy > 35f * screenHeight / 100f) {
							if (runner.getY() + dy < 65f * screenHeight / 100f) {
								warningGroup.addActor(warningImage);
								whistle.play(0.2f);
								for (int i = 0; i < 4; i++)
									checkpoint[i] = 0;
								timex = TimeUtils.nanoTime();
							}
						}
					}
				}

			}
		});

		causeImage = new Image(new Texture("data/Backgrounds/Stadiumchoose.png"));
		stage.addActor(causeImage);

		introGroup.addActor(causeImage);
		causeImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("x " + x + "y " + y + "" + screenHeight);
				if ((x / screenWidth) > 0.3726 && (x / screenWidth) < 0.5726) {
					System.out.println("animals");
					animals = true;
				} else if ((x / screenWidth) < 0.3726) {
					System.out.println("plants");
					plants = true;

				} else if ((x / screenWidth) > 0.5726) {
					System.out.println("Auskick");
					Auskick = true;
				}
				causeChosen = true;
				introGroup.removeActor(causeImage);
				runInst1 = new Image(new Texture(Gdx.files
						.internal("data/Instructions/runInst1.png")));
				runInst1.setWidth(screenWidth);
				runInst1.setHeight(screenHeight);
				introGroup.clear();
				introGroup.addActor(runInst1);
			}
		});

		skip = new Image(game.generalTextureAtlas.findRegion("skip"));
		skip.setX(80f * screenWidth / 100f);
		skip.setY(5f * screenHeight / 100f);
		skip.setScale(screenWidth / 1280f);
		skip.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				timeCount = 418;
			}
		});

		stage.addActor(introGroup);

		blackImage = new Image(game.generalTextureAtlas.findRegion("black"));
		blackImage.setFillParent(true);
		stage.addActor(blackImage);
		blackImage.addAction(sequence(delay(0.75f), fadeOut(0.1f),
				Actions.removeActor()));
		System.out.println("Done 3");

		gameTheme.setVolume(0.4f);
		gameTheme.play();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		if (stop == false) {
			lapCount.setText("" + laps);
			if (laps > 9) {
				lapCount.setX(10.5f * screenWidth / 100f);
			} else {
				lapCount.setX(10.5f * screenWidth / 100f);
			}
		}

		stage.act(delta);
		stage.draw();
		batch.begin();
		batch.end();

		if (causeChosen == true) {
			if (TimeUtils.nanoTime() - timex > 600000000) {
				timex = TimeUtils.nanoTime();
				warningGroup.removeActor(warningImage);
			}

			if (TimeUtils.nanoTime() - timediff > 15000000) {
				timediff = TimeUtils.nanoTime();
				timeCount++;
				if (timeCount == 120) {
					Image runInst2 = new Image(new Texture(
							Gdx.files.internal("data/Instructions/runInst2.png")));
					runInst2.setWidth(screenWidth);
					runInst2.setHeight(screenHeight);
					introGroup.clear();
					introGroup.addActor(runInst2);
					introGroup.addActor(skip);
					introGroup.addActor(skip);
				} else if (timeCount == 180) {
					Image runInst3 = new Image(new Texture(
							Gdx.files.internal("data/Instructions/runInst3.png")));
					runInst3.setWidth(screenWidth);
					runInst3.setHeight(screenHeight);
					introGroup.clear();
					introGroup.addActor(runInst3);
					introGroup.addActor(skip);
				} else if (timeCount == 240) {
					Image runInst4 = new Image(new Texture(
							Gdx.files.internal("data/Instructions/runInst4.png")));
					runInst4.setWidth(screenWidth);
					runInst4.setHeight(screenHeight);
					introGroup.clear();
					introGroup.addActor(runInst4);
					introGroup.addActor(skip);
				} else if (timeCount == 300) {
					Image runInst5 = new Image(new Texture(
							Gdx.files.internal("data/Instructions/runInst5.png")));
					runInst5.setWidth(screenWidth);
					runInst5.setHeight(screenHeight);
					introGroup.clear();
					introGroup.addActor(runInst5);
					introGroup.addActor(skip);
				} else if (timeCount == 360) {
					Image runInst6 = new Image(new Texture(
							Gdx.files.internal("data/Instructions/runInst6.png")));
					runInst6.setWidth(screenWidth);
					runInst6.setHeight(screenHeight);
					introGroup.clear();
					introGroup.addActor(runInst6);
					introGroup.addActor(skip);
				} else if (timeCount == 420) {
					introGroup.clear();
					stop = false;
				}

				if (timeCount > 420) {
					imageredbar.setWidth(screenWidth / 500 * count++);

					if (imageredbar.getWidth() >= screenWidth && !stop) {
						stop = true;

						pref.putInteger("Community_new",
								pref.getInteger("Community", 0) + laps);
						pref.flush();
						System.out.println("COMMUNITY POINTS:"
								+ pref.getInteger("Community", 0));
						System.out.println("COMMUNITY_NEW POINTS:"
								+ pref.getInteger("Community_new", 0));

						// Finished message and popup box
						stage.addActor(popupImage);

						BitmapFont sketchFont = new BitmapFont(
								Gdx.files.internal("data/Fonts/sketch120.fnt"),
								Gdx.files.internal("data/Fonts/sketch120.png"), false);
						TextFieldStyle finishedMessageStyle = new TextFieldStyle();
						finishedMessageStyle.fontColor = Color.BLACK;
						finishedMessageStyle.font = sketchFont;
						finishedMessageStyle.font.setScale(screenWidth / 1200f);

						TextField finishedMessage1 = new TextField("You ran "
								+ laps + " laps", finishedMessageStyle);
						finishedMessage1.setWidth(screenWidth);
						finishedMessage1.setX(15.5f * screenWidth / 100f);
						finishedMessage1.setY(50f * screenHeight / 100f);
						finishedMessage1.setDisabled(true);
						stage.addActor(finishedMessage1);

						TextField finishedMessage2 = null;
						if (Auskick == true) {
							finishedMessage2 = new TextField("for AusKick",
									finishedMessageStyle);
						} else if (animals == true) {
							finishedMessage2 = new TextField("for Animals",
									finishedMessageStyle);
						} else if (plants == true) {
							finishedMessage2 = new TextField("for nature",
									finishedMessageStyle);
						}
						finishedMessage2.setWidth(screenWidth);
						finishedMessage2.setX(15.5f * screenWidth / 100f);
						finishedMessage2.setY(35f * screenHeight / 100f);
						stage.addActor(finishedMessage2);
						finishedMessage2.setDisabled(true);

						stage.addActor(nextButtonImage);

						pref.putInteger("fromAdventureScreen", 0);
						pref.flush();
					}
				}
			}
		}
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void dispose() {
		Gdx.input.setInputProcessor(null);
		gameTheme.dispose();
		font.dispose();
		batch.dispose();
		stage.dispose();
	}
}