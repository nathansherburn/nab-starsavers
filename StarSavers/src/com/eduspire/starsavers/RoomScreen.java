package com.eduspire.starsavers;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class RoomScreen extends AbstractScreen {
	public Texture background, avatarTexture, bedTexture, adventureTexture;
	private Stage stage;
	private Image backgroundImage;
	private SpriteBatch batch;
	int screenWidth, screenHeight;
	Image headImage, bodyImage, eyesImage, hairImage, shirtImage, lipsImage, shadeImage,
			pantsImage, hatsImage;
	private String Prefs = "StarCharacters";
	private Preferences pref;
	private Image blackImage;
	Group group;
	boolean bedUnlocked, closetUnlocked, carpetUnlocked, sideUnlocked,
			bbagUnlocked, deskUnlocked, chairUnlocked, lampUnlocked,
			footyUnlocked, guitarUnlocked, plantUnlocked, piggyUnlocked,
			laptopUnlocked, skatebUnlocked;
	Music relaxTheme;	

	public RoomScreen(StarSavers game) {
		super(game);
	}

	void placeImage(Image imageName, Float scale, Float x, Float y) {
		int location = 1;
		if (location == 1) {
			group.setX(165);
			group.setY(66);
			group.setScale(0.9f);
		} else if (location == 2) {
			group.setX(150);
			group.setY(330);
			group.setScale(0.3f);
		} else if (location == 3) {
			group.setX(280);
			group.setY(530);
			group.setScale(0.3f);
		} else if (location == 4) {
			group.setX(570);
			group.setY(550);
			group.setScale(0.3f);
		} else if (location == 5) {
			group.setX(730);
			group.setY(340);
			group.setScale(0.3f);
		} else if (location == 6) {
			group.setX(600);
			group.setY(66);
			group.setScale(0.3f);
		} else if (location == 7) {
			group.setX(500);
			group.setY(310);
			group.setScale(0.3f);
		}

		imageName.setScale(scale);
		imageName.setX(x);
		imageName.setY(y);
	}

	@Override
	public void show() {
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		batch = new SpriteBatch();
		boolean maintainAspectRatio = true;
		stage = new Stage(screenWidth, screenHeight, maintainAspectRatio);
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);
		pref = Gdx.app.getPreferences(Prefs);
		group = new Group();

		relaxTheme = Gdx.audio.newMusic(Gdx.files
				.internal("data/Sound/relaxTheme.mp3"));
		relaxTheme.setLooping(true);
		
		backgroundImage = new Image(new Texture("data/Backgrounds/roomScreenBackground.png"));
		backgroundImage.setSize(screenWidth, screenHeight);
		stage.addActor(backgroundImage);

		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("x " + x + "y " + y + "" + screenHeight);

				if ((x / screenWidth) > 0.189 && (x / screenWidth) < 0.44297) {
					if ((y / screenHeight) > 0.7056
							&& (y / screenHeight) < 0.97222) {
						stage.addActor(blackImage);
						blackImage.addAction(sequence(fadeIn(0.1f),
								new Action() {
									@Override
									public boolean act(float delta) {
										relaxTheme.stop();
										dispose();
										game.setScreen(new AdventureScreen(game));
										return false;
									}
								}));
					}
				}

			}
		});

		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("x " + x + "y " + y + "" + screenHeight);

				if ((x / screenWidth) < 0.11) {
					if ((y / screenHeight) > 0.53 && (y / screenHeight) < 0.83) {
						stage.addActor(blackImage);
						blackImage.addAction(sequence(fadeIn(0.1f),
								new Action() {
									@Override
									public boolean act(float delta) {
										relaxTheme.stop();
										dispose();
										game.setScreen(new SetupScreen2(game));
										return false;
									}
								}));
					}
				}

			}
		});
		
		bedUnlocked = pref.getBoolean("bedUnlocked", false);
		System.out.println("bed" + bedUnlocked);
		closetUnlocked = pref.getBoolean("closetUnlocked", false);
		deskUnlocked = pref.getBoolean("deskUnlocked", false);
		chairUnlocked = pref.getBoolean("chairUnlocked", false);
		sideUnlocked = pref.getBoolean("sideUnlocked", false);
		bbagUnlocked = pref.getBoolean("bbagUnlocked", false);
		carpetUnlocked = pref.getBoolean("carpetUnlocked", false);

		guitarUnlocked = pref.getBoolean("guitarUnlocked", false);
		lampUnlocked = pref.getBoolean("lampUnlocked", false);
		plantUnlocked = pref.getBoolean("plantUnlocked", false);
		piggyUnlocked = pref.getBoolean("piggyUnlocked", false);
		footyUnlocked = pref.getBoolean("footyUnlocked", false);
		skatebUnlocked = pref.getBoolean("skatebUnlocked", false);
		laptopUnlocked = pref.getBoolean("laptopUnlocked", false);

		if (plantUnlocked) {
			Image plant = new Image(
					game.roomAtlas.findRegion("plant"));
			plant.setX(0.847f * screenWidth);
			plant.setY(0.6322f * screenHeight);
			plant.setScale(0.66f);
			stage.addActor(plant);
		}

		if (piggyUnlocked) {
			Image piggy = new Image(
					game.roomAtlas.findRegion("piggy"));
			piggy.setX(0.414f * screenWidth);
			piggy.setY(0.5419f * screenHeight);
			piggy.setScale(0.66f);
			stage.addActor(piggy);
		}

		if (closetUnlocked) {
			Image closetImage = new Image(
					game.roomAtlas.findRegion("wardboardc"));
			closetImage.setScale(0.65f);
			closetImage.setX(0.5227f * screenWidth);
			closetImage.setY(0.3056f * screenHeight);
			stage.addActor(closetImage);
		}

		if (bedUnlocked) {
			Image bedImage = new Image(
					game.roomAtlas.findRegion("bed"));
			bedImage.setScale(0.66f);
			bedImage.setX(0.2156f * screenWidth);
			bedImage.setY(0.20833f * screenHeight);
			stage.addActor(bedImage);
		}

		if (carpetUnlocked) {
			Image carpetImage = new Image(
					game.roomAtlas.findRegion("carpet"));
			carpetImage.setScale(0.7f);
			carpetImage.setX(110);
			carpetImage.setY(00);
			stage.addActor(carpetImage);

		}
		if (skatebUnlocked) {
			Image skateb = new Image(
					game.roomAtlas.findRegion("skateb"));
			skateb.setX(420);
			skateb.setY(10.2f);
			skateb.setScale(0.66f);
			stage.addActor(skateb);
		}
		if (guitarUnlocked) {
			Image guitarImage = new Image(
					game.roomAtlas.findRegion("guitar"));
			guitarImage.setScale(0.66f);
			guitarImage.setX(650);
			guitarImage.setY(120);
			stage.addActor(guitarImage);
		}
		if (footyUnlocked) {
			Image footballImage = new Image(
					game.roomAtlas.findRegion("football"));
			footballImage.setScale(0.66f);
			footballImage.setX(483);
			footballImage.setY(100);
			stage.addActor(footballImage);
		}

		if (bbagUnlocked) {
			Image beanbagImage = new Image(
					game.roomAtlas.findRegion("beanbag"));
			beanbagImage.setScale(0.66f);
			beanbagImage.setX(923);
			beanbagImage.setY(160);
			stage.addActor(beanbagImage);
		}

		if (deskUnlocked) {
			Image deskImage = new Image(
					game.roomAtlas.findRegion("desk"));
			deskImage.setScale(0.670f);
			deskImage.setX(1065);
			deskImage.setY(00);
			stage.addActor(deskImage);
		}

		if (chairUnlocked) {
			Image chairImage = new Image(
					game.roomAtlas.findRegion("chair"));
			chairImage.setScale(0.670f);
			chairImage.setX(965);
			chairImage.setY(00);
			stage.addActor(chairImage);
		}

		if (laptopUnlocked) {
			Image laptopImage = new Image(
					game.roomAtlas.findRegion("laptop1"));
			laptopImage.setScale(0.670f);
			laptopImage.setX(1125);
			laptopImage.setY(169);
			stage.addActor(laptopImage);
		}

		if (lampUnlocked) {
			Image lampImage = new Image(
					game.roomAtlas.findRegion("lamp"));
			lampImage.setScale(0.65f);
			lampImage.setX(155);
			lampImage.setY(370);
			stage.addActor(lampImage);
		}

		if (sideUnlocked) {
			Image sideImage = new Image(
					game.roomAtlas.findRegion("side"));
			sideImage.setScale(0.7f);
			sideImage.setX(140);
			sideImage.setY(230);
			stage.addActor(sideImage);
		}

		int skinTone = pref.getInteger("skinTone", 1);
		headImage = new Image(game.avatarTextureAtlas0.findRegion("head" + skinTone));
		bodyImage = new Image(game.avatarTextureAtlas0.findRegion("body" + skinTone));
		placeImage(headImage, screenWidth / 1280f, 66f * screenWidth / 100f,
				35f * screenHeight / 100f);
		placeImage(bodyImage, screenWidth / 1280f,
				70.61f * screenWidth / 100f, 12.48f * screenHeight / 100f);
		group.addActor(bodyImage);
		group.addActor(headImage);
		
		int eyeColour = pref.getInteger("eyeColour", 1);
		eyesImage = new Image(game.avatarTextureAtlas0.findRegion("eyes" + eyeColour));
		placeImage(eyesImage, screenWidth / 1280f, 63.7f * screenWidth / 100f,
				52.3f * screenHeight / 100f);
		group.addActor(eyesImage);

		int lipsStyle = pref.getInteger("lipsStyle", 1);
		lipsImage = new Image(game.avatarTextureAtlas0.findRegion("lips" + lipsStyle));
		placeImage(lipsImage, screenWidth / 1280f,
				69.61f * screenWidth / 100f, 37.48f * screenHeight / 100f);
		group.addActor(lipsImage);

		int hairStyle = pref.getInteger("hairStyle", 1);
		if (hairStyle == 1 || (hairStyle > 9 && hairStyle < 19)) {
			hairImage = new Image(game.avatarTextureAtlas2.findRegion("hair" + hairStyle));
		} else if (hairStyle == 2 || hairStyle == 3 || (hairStyle > 18 && hairStyle < 32)) {
			hairImage = new Image(game.avatarTextureAtlas1.findRegion("hair" + hairStyle));
		} else {
			hairImage = new Image(game.avatarTextureAtlas0.findRegion("hair" + hairStyle));
		}
		placeImage(hairImage, screenWidth / 1280f,
				63.33f * screenWidth / 100f, 18.3f * screenHeight / 100f);
		group.addActor(hairImage);
		
		int pantsStyle = pref.getInteger("pantsStyle", 1);
		pantsImage = new Image(game.shopAtlas.findRegion("pants" + pantsStyle));
		placeImage(pantsImage, screenWidth / 1100f,
				71.61f * screenWidth / 100f, 14.48f * screenHeight / 100f);
		group.addActor(pantsImage);

		int shirtStyle = pref.getInteger("shirtStyle", 1);
		shirtImage = new Image(game.shopAtlas.findRegion("tshirt" + shirtStyle));
		placeImage(shirtImage, screenWidth / 1280f,
				71.91f * screenWidth / 100f, 22.1f * screenHeight / 100f);
		group.addActor(shirtImage);

		int shadesStyle = pref.getInteger("shadesStyle", 1);
		shadeImage = new Image(game.shopAtlas.findRegion("shades" + shadesStyle));
		placeImage(shadeImage, screenWidth / 1100f,
				60f * screenWidth / 100f, 45.48f * screenHeight / 100f);
		group.addActor(shadeImage);

		int hatStyle = pref.getInteger("hatStyle", 1);
		hatsImage = new Image(game.shopAtlas.findRegion("cap" + hatStyle));
		placeImage(hatsImage, screenWidth / 1280f,
				65.91f * screenWidth / 100f, 66.1f * screenHeight / 100f);
		group.addActor(hatsImage);
		
		group.setX(-3f * screenWidth / 100f);
		group.setY(-1f * screenHeight / 100f);

		stage.addActor(group);

		blackImage = new Image(
				game.generalTextureAtlas.findRegion("black"));
		blackImage.setFillParent(true);
		stage.addActor(blackImage);

		blackImage.addAction(sequence(delay(0.1f), fadeOut(0.1f),
				Actions.removeActor()));
		
		relaxTheme.play();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void dispose() {
		relaxTheme.dispose();
		stage.dispose();
	}
}
