package com.eduspire.starsavers;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class BonusScreen implements Screen {
	String Prefs = "StarCharacters";
	Preferences pref;

	final StarSavers game;
	long prev_coin, timediff = 0, timeCount = 0, lastDropTime;
	int screenWidth, screenHeight, dropsGathered, coinsCollected, count = 0;
	float startTime;
	boolean stop = false;
	double accelY = 0;
	Skin skin;
	Stage stage, forgroundStage;
	OrthographicCamera camera;
	Rectangle bucket;
	Array<Rectangle> coinarray;
	TextField coinCount;
	Group dropGroup = new Group();
	Group introGroup = new Group();
	Texture backgroundTexture,  instruction1,
			instruction2, goTexture, transSky, pigSilTexture, adventureTexture;
	
	AtlasRegion dropTexture,bucketImage;
	
	Image imageredbar, imagegraybar, backgroundImage, piggy, blackImage, skip,
			popupImage, nextButtonImage, instructionImage1, instructionImage2,
			instructionImage3, goImage, transSkyImage, dropImage, pigSilImage,
			adventureImage;
	Music gameTheme;
		
	public BonusScreen(final StarSavers gam) {
		this.game = gam;

		Texture.setEnforcePotImages(false);
		pref = Gdx.app.getPreferences(Prefs);
		gameTheme = Gdx.audio.newMusic(Gdx.files.internal("data/Sound/gameTheme.mp3"));
		gameTheme.setLooping(true);
				
		dropTexture = game.bonusAtlas.findRegion("coin");
		dropImage = new Image(dropTexture);
		bucketImage = game.bonusAtlas.findRegion("piggybank");
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		imageredbar = new Image(game.generalTextureAtlas.findRegion("bar"));
		imagegraybar = new Image(game.generalTextureAtlas.findRegion("grayBar"));
		imageredbar.setWidth(0);
		imagegraybar.setWidth(screenWidth);
		backgroundTexture = new Texture("data/Backgrounds/BonusScreenBackground.png");
		backgroundTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		backgroundImage = new Image(backgroundTexture);
		backgroundImage.setFillParent(true);
		stage = new Stage(screenWidth, screenHeight, true);
		forgroundStage = new Stage(screenWidth, screenHeight, true);
		Gdx.input.setInputProcessor(forgroundStage);
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 1280, 720);
		stage.addActor(backgroundImage);

		imageredbar.setX(0);
		imagegraybar.setX(0);
		imageredbar.setY(screenHeight - screenHeight / 21f);
		imagegraybar.setY(screenHeight - screenHeight / 21f);
		stage.addActor(imagegraybar);
		stage.addActor(imageredbar);

		bucket = new Rectangle();
		bucket.x = screenWidth / 2 - 128; // center
		bucket.width = 50;
		bucket.height = 160;

		instructionImage1 = new Image(new Texture("data/Instructions/coinGameInstructions1.png"));
		instructionImage1.setWidth(screenWidth);
		instructionImage1.setHeight(screenHeight);
		introGroup.addActor(instructionImage1);

		instructionImage2 = new Image(new Texture("data/Instructions/coinGameInstructions2.png"));
		instructionImage2.setWidth(screenWidth);
		instructionImage2.setHeight(screenHeight);

		instructionImage3 = new Image(new Texture("data/Instructions/coinGameInstructions3.png"));
		instructionImage3.setWidth(screenWidth);
		instructionImage3.setHeight(screenHeight);

		skip = new Image(game.generalTextureAtlas.findRegion("skip"));
		skip.setX(80f * screenWidth / 100f);
		skip.setY(5f * screenHeight / 100f);
		skip.setScale(screenWidth / 1280f);
		skip.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				timeCount = 298;
			}
		});

	
		goImage = new Image(game.generalTextureAtlas.findRegion("goText"));
		goImage.setX(30f * screenWidth / 100f);
		goImage.setY(30f * screenHeight / 100f);

		popupImage = new Image(game.generalTextureAtlas.findRegion("popup"));
		popupImage.setX(10f * screenWidth / 100f);
		popupImage.setY(10f * screenHeight / 100f);
		popupImage.setSize(80f * screenWidth / 100f, 80f * screenHeight / 100f);

		
		pigSilImage = new Image(game.bonusAtlas.findRegion("pigSilhouette"));
		pigSilImage.setScale(screenWidth / 1280f);
		pigSilImage.setX(1f * screenWidth / 100f);
		pigSilImage.setY(72f * screenHeight / 100f);

		nextButtonImage = new Image(game.generalTextureAtlas.findRegion("nextButton"));
		nextButtonImage.setScale(screenWidth / 1280f);
		nextButtonImage.setX(74f * screenWidth / 100f);
		nextButtonImage.setY(10f * screenHeight / 100f);
		nextButtonImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				forgroundStage.addActor(blackImage);
				blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
					@Override
					public boolean act(float delta) {
						dispose();
						gameTheme.stop();
						dispose();
						game.setScreen(new BankScreen(game));
						return false;
					}
				}));
			}
		});

		// create the coinarray array and spawn the first coinitem
		coinarray = new Array<Rectangle>();
		spawnRaindrop();

		
		TextFieldStyle coinCountStyle = new TextFieldStyle();
		coinCountStyle.fontColor = Color.WHITE;
		coinCountStyle.font = game.font;
		coinCountStyle.font.setScale(screenWidth / 1280f);
		coinCount = new TextField("" + dropsGathered, coinCountStyle);
		coinCount.setWidth(screenWidth);
		coinCount.setX(5.5f * screenWidth / 100f);
		coinCount.setY(74f * screenHeight / 100f);

		// stage.addActor(dropGroup);
		// forgroundStage.addActor(piggy);
		forgroundStage.addActor(pigSilImage);
		forgroundStage.addActor(coinCount);
		forgroundStage.addActor(imagegraybar);
		forgroundStage.addActor(imageredbar);
		forgroundStage.addActor(introGroup);

		stop = true;

		blackImage = new Image(game.generalTextureAtlas.findRegion("black"));
		blackImage.setFillParent(true);
		forgroundStage.addActor(blackImage);
		blackImage.addAction(sequence(delay(0.75f), fadeOut(0.1f),
				Actions.removeActor()));
		
		gameTheme.setVolume(0.4f);
		gameTheme.play();
	}

	private void spawnRaindrop() {
		Rectangle coinitem = new Rectangle();
		coinitem.x = MathUtils.random(1f * screenWidth / 100f, screenWidth - 1f
				* screenWidth / 100f - 64);
		coinitem.y = 720;
		coinitem.width = 200;
		coinitem.height = 200;
		coinarray.add(coinitem);
		prev_coin = TimeUtils.nanoTime();
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		forgroundStage.act(delta);
		stage.draw();
		camera.update();
		game.batch.setProjectionMatrix(camera.combined);

		// process user input
		// if (Gdx.input.isTouched()) {
		// Vector3 touchPos = new Vector3();
		// touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
		// camera.unproject(touchPos);
		// bucket.x = touchPos.x - 64 / 2;
		// }

		float accelPos = (float) filterY() * screenWidth / 10f + 1.1f
				* screenWidth / 2f;
		// game.camera.unproject(touchPos);
		bucket.x = accelPos;
		// piggy.setX(accelPos - 170);

		// if (Gdx.input.isKeyPressed(Keys.LEFT))
		// bucket.x -= 200 * Gdx.graphics.getDeltaTime();
		// if (Gdx.input.isKeyPressed(Keys.RIGHT))
		// bucket.x += 200 * Gdx.graphics.getDeltaTime();

		// make sure the bucket stays within the screen bounds
		if (bucket.x < 0)
			bucket.x = 0;
		else if (bucket.x > screenWidth - 256f)
			bucket.x = screenWidth - 256f;

		// piggy.setY(1f * screenHeight / 100f);
		// piggyGroup.removeActor(piggy);

		// begin a new batch and draw the bucket and
		// all drops
		game.batch.begin();
		game.batch.draw(bucketImage, bucket.x - 175, bucket.y);

		for (Rectangle coinitem : coinarray) {
			game.batch.draw(dropTexture, coinitem.x, coinitem.y);
		}
		game.batch.end();

		forgroundStage.draw();

		if (dropsGathered > 9) {
			coinCount.setX(3.5f * screenWidth / 100f);
		}
		coinCount.setText("" + dropsGathered);

		// move the coinarray, remove any that are beneath the bottom edge of
		// the screen or that hit the bucket. In the later case we play back
		// a sound effect as well.
		Iterator<Rectangle> iter = coinarray.iterator();
		while (iter.hasNext() && !stop) {
			Rectangle coinitem = iter.next();
			coinitem.y -= 200 * Gdx.graphics.getDeltaTime();
			if (coinitem.y + 64 < 0)
				iter.remove();
			if (coinitem.overlaps(bucket)) {
				dropsGathered++;
				// dropSound.play();
				iter.remove();
			}
		}

		if (TimeUtils.nanoTime() - timediff > 15000000) {
			timediff = TimeUtils.nanoTime();
			timeCount++;
			if (timeCount == 60) {
				introGroup.clear();
				introGroup.addActor(instructionImage2);
				introGroup.addActor(skip);
			} else if (timeCount == 120) {
				introGroup.clear();
				introGroup.addActor(instructionImage3);
				introGroup.addActor(skip);
			} else if (timeCount == 180) {
				introGroup.clear();
				introGroup.addActor(instructionImage2);
				introGroup.addActor(skip);
			} else if (timeCount == 240) {
				introGroup.clear();
				introGroup.addActor(goImage);
			} else if (timeCount == 300) {
				introGroup.clear();
				stop = false;
			}

			if (timeCount > 300) {
				imageredbar.setWidth(screenWidth / 500 * count++);
				// System.out.println(count + "," + imageredbar.getWidth());

				// check if we need to create a new coinitem
				if (TimeUtils.nanoTime() - prev_coin > 300000000) {
					spawnRaindrop();
				}

				if (imageredbar.getWidth() >= screenWidth && !stop) {
					stop = true;

					// Finished message and popup box
					forgroundStage.addActor(popupImage);

					TextFieldStyle finishedMessageStyle = new TextFieldStyle();
					finishedMessageStyle.fontColor = Color.BLACK;
					finishedMessageStyle.font = game.font;
					finishedMessageStyle.font.setScale(screenWidth / 1200f);
					TextField finishedMessage1 = new TextField("You collected",
							finishedMessageStyle);
					TextField finishedMessage2 = new TextField(""
							+ dropsGathered + " coins", finishedMessageStyle);
					finishedMessage1.setWidth(screenWidth);
					finishedMessage1.setX(19.5f * screenWidth / 100f);
					finishedMessage1.setY(50f * screenHeight / 100f);
					forgroundStage.addActor(finishedMessage1);
					finishedMessage1.setDisabled(true);
					finishedMessage2.setWidth(screenWidth);
					if (dropsGathered > 9) {
						finishedMessage2.setX(33f * screenWidth / 100f);
					} else {
						finishedMessage2.setX(35f * screenWidth / 100f);
					}
					finishedMessage2.setY(35f * screenHeight / 100f);
					forgroundStage.addActor(finishedMessage2);
					finishedMessage2.setDisabled(true);

					pref.putInteger("Savings_new",
							pref.getInteger("Savings", 0) + dropsGathered);

					pref.putInteger("fromAdventureScreen", 0);
					pref.flush();

					forgroundStage.addActor(nextButtonImage);

				}
			}
		}

	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
		// start the playback of the background music
		// when the screen is shown
		// rainMusic.play();
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		//dropTexture.dispose();
	//	bucketImage.dispose();
		gameTheme.dispose();
		// dropSound.dispose();
		// rainMusic.dispose();
	}

	public double filterY() {
		float kFilteringFactor = 0.06f;
		accelY = (Gdx.input.getAccelerometerY() * kFilteringFactor)
				+ (accelY * (1.0 - kFilteringFactor));
		return accelY;
	}

}