package com.eduspire.starsavers;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class ShoppingMallScreen extends AbstractScreen {

	public Texture background, avatarTexture, bedTexture, adventureTexture;
	private Stage stage;
	private Image backgroundImage, blackImage, adventureImage;
	private SpriteBatch batch;
	int screenWidth, screenHeight;
	String Prefs = "StarCharacters";
	Preferences pref;
	Music shoppingTheme;

	public ShoppingMallScreen(StarSavers game) {
		super(game);
	}

	void placeImage(Image imageName, Float scale, Float x, Float y) {
		imageName.setScale(scale);
		imageName.setX(x);
		imageName.setY(y);
	}

	@Override
	public void show() {
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		batch = new SpriteBatch();
		boolean maintainAspectRatio = true;
		stage = new Stage(screenWidth, screenHeight, maintainAspectRatio);
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);
		pref = Gdx.app.getPreferences(Prefs);

		backgroundImage = new Image(new Texture(
				"data/Backgrounds/ShoppingMallScreenBackground.png"));
		backgroundImage.setFillParent(true);
		stage.addActor(backgroundImage);

		shoppingTheme = Gdx.audio.newMusic(Gdx.files
				.internal("data/Sound/shoppingTheme.mp3"));
		shoppingTheme.setLooping(true);
		
		// bedTexture = new Texture("bed.png");
		// Image bedImage = new Image(bedTexture);
		// bedImage.setHeight(screenHeight / 1f);
		// bedImage.setWidth(screenWidth / 2.3f);
		// bedImage.setX(4f * screenWidth / 7f);
		// bedImage.setY(-screenHeight / 1.8f);
		// stage.addActor(bedImage);

		
		adventureImage = new Image(game.generalTextureAtlas.findRegion("nextButtonAdventure"));
		adventureImage.setScale(screenWidth / 2010f);
		adventureImage.setX(86.5f * screenWidth / 100f);
		adventureImage.setY(5f * screenHeight / 100f);
		adventureImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				stage.addActor(blackImage);
				blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
					@Override
					public boolean act(float delta) {
						shoppingTheme.stop();
						dispose();
						game.setScreen(new AdventureScreen(game));
						return false;
					};
				}));
			};
		});

		stage.addActor(adventureImage);

		backgroundImage.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("x " + x + "y " + y + "" + screenHeight);

				if (x / screenWidth < 0.5) {
					stage.addActor(blackImage);
					blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
						@Override
						public boolean act(float delta) {
							shoppingTheme.stop();
							dispose();
							game.setScreen(new GroceryScreen(game));
							return false;
						}
					}));
				}

				if ((x / screenWidth) > 0.5) {
					stage.addActor(blackImage);
					blackImage.addAction(sequence(fadeIn(0.1f), new Action() {
						@Override
						public boolean act(float delta) {
							shoppingTheme.stop();
							dispose();
							game.setScreen(new ShopScreen(game));
							return false;
						}
					}));
				}
			}
		});

		blackImage = new Image(
				game.generalTextureAtlas.findRegion("black"));
		blackImage.setFillParent(true);
		stage.addActor(blackImage);
		blackImage.addAction(sequence(delay(0.1f), fadeOut(0.1f),
				Actions.removeActor()));

		shoppingTheme.play();

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void dispose() {
		shoppingTheme.dispose();
	}
}
