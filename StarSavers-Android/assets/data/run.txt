run.png
format: RGBA8888
filter: Linear,Linear
repeat: none
causeafl
  rotate: false
  xy: 598, 2
  size: 300, 284
  orig: 300, 300
  offset: 0, 9
  index: -1
causeanimal
  rotate: false
  xy: 296, 2
  size: 300, 292
  orig: 300, 300
  offset: 0, 8
  index: -1
causeplant
  rotate: false
  xy: 2, 2
  size: 292, 300
  orig: 300, 300
  offset: 7, 0
  index: -1
noShortcuts
  rotate: false
  xy: 900, 2
  size: 756, 273
  orig: 756, 273
  offset: 0, 0
  index: -1
runningCharacter
  rotate: false
  xy: 1658, 2
  size: 68, 131
  orig: 70, 135
  offset: 0, 4
  index: -1
