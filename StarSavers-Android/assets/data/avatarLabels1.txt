avatarTexture1.png
format: RGBA8888
filter: Linear,Linear
repeat: none
hair19
  rotate: false
  xy: 2, 2
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair2
  rotate: false
  xy: 2, 516
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair20
  rotate: false
  xy: 2, 1030
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair21
  rotate: false
  xy: 354, 2
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair22
  rotate: false
  xy: 354, 516
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair23
  rotate: false
  xy: 354, 1030
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair24
  rotate: false
  xy: 706, 2
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair25
  rotate: false
  xy: 1058, 2
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair26
  rotate: false
  xy: 1410, 2
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair27
  rotate: false
  xy: 706, 516
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair28
  rotate: false
  xy: 706, 1030
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair29
  rotate: false
  xy: 1058, 516
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair3
  rotate: false
  xy: 1058, 1030
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair30
  rotate: false
  xy: 1410, 516
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair31
  rotate: false
  xy: 1410, 1030
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
