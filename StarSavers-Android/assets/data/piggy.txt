piggy.png
format: RGBA8888
filter: Linear,Linear
repeat: none
coin
  rotate: false
  xy: 254, 137
  size: 64, 64
  orig: 64, 64
  offset: 0, 0
  index: -1
pigSilhouette
  rotate: false
  xy: 254, 2
  size: 180, 133
  orig: 180, 133
  offset: 0, 0
  index: -1
piggybank
  rotate: false
  xy: 2, 2
  size: 250, 182
  orig: 256, 184
  offset: 0, 2
  index: -1
