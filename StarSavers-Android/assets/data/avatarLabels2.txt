avatarTexture2.png
format: RGBA8888
filter: Linear,Linear
repeat: none
hair1
  rotate: false
  xy: 2, 2
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair10
  rotate: false
  xy: 2, 516
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair11
  rotate: false
  xy: 354, 2
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair12
  rotate: false
  xy: 354, 516
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair13
  rotate: false
  xy: 706, 2
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair14
  rotate: false
  xy: 706, 516
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair15
  rotate: false
  xy: 1058, 2
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair16
  rotate: false
  xy: 1058, 516
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair17
  rotate: false
  xy: 1410, 2
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
hair18
  rotate: false
  xy: 1410, 516
  size: 350, 512
  orig: 350, 512
  offset: 0, 0
  index: -1
