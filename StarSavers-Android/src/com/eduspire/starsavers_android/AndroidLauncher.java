package com.eduspire.starsavers_android;

import android.os.Bundle;
import com.eduspire.starsavers.StarSavers;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// whether to use OpenGL ES 2.0

		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
		cfg.useGL20 = false;
		cfg.useAccelerometer = true;
		cfg.useCompass = false;
		// create the game
		initialize(new StarSavers(), cfg);
	}

}