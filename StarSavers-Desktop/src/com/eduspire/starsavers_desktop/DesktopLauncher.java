package com.eduspire.starsavers_desktop;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.eduspire.starsavers.StarSavers;

public class DesktopLauncher {
	public static void main(String[] args) {
		// create the listener that will receive the application events
		ApplicationListener listener = new StarSavers();
		// LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		
		// define the window's title
		String title = "StarSavers";

		// define the window's size
		int width = 1280, height = 720;

		// whether to use OpenGL ES 2.0
		boolean useOpenGLES2 = false;

		// create the game
		new LwjglApplication(listener, title, width, height, useOpenGLES2);
	}
}